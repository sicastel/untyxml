module C = struct
  let string_of_sandbox_token = function
    | `Allow_forms -> "allow-forms"
    | `Allow_pointer_lock -> "allow-pointer-lock"
    | `Allow_popups -> "allow-popups"
    | `Allow_top_navigation -> "allow-top-navigation"
    | `Allow_same_origin -> "allow-same-origin"
    | `Allow_script -> "allow-script"

  let string_of_linktype = function
    | `Alternate -> "alternate"
    | `Archives -> "archives"
    | `Author -> "author"
    | `Bookmark -> "bookmark"
    | `Canonical -> "canonical"
    | `External -> "external"
    | `First -> "first"
    | `Help -> "help"
    | `Icon -> "icon"
    | `Index -> "index"
    | `Last -> "last"
    | `License -> "license"
    | `Next -> "next"
    | `Nofollow -> "nofollow"
    | `Noreferrer -> "noreferrer"
    | `Noopener -> "noopener"
    | `Pingback -> "pingback"
    | `Prefetch -> "prefetch"
    | `Prev -> "prev"
    | `Search -> "search"
    | `Stylesheet -> "stylesheet"
    | `Sidebar -> "sidebar"
    | `Tag -> "tag"
    | `Up -> "up"
    | `Other s -> s

  let string_of_mediadesc_token = function
    | `All -> "all"
    | `Aural -> "aural"
    | `Braille -> "braille"
    | `Embossed -> "embossed"
    | `Handheld -> "handheld"
    | `Print -> "print"
    | `Projection -> "projection"
    | `Screen -> "screen"
    | `Speech -> "speech"
    | `Tty -> "tty"
    | `Tv -> "tv"
    | `Raw_mediadesc s -> s

  let string_of_referrerpolicy = function
    | `Empty -> ""
    | `No_referrer -> "no-referrer"
    | `No_referrer_when_downgrade -> "no-referrer-when-downgrade"
    | `Origin -> "origin"
    | `Origin_when_cross_origin -> "origin-when-cross-origin"
    | `Same_origin -> "same-origin"
    | `Strict_origin -> "strict-origin"
    | `Strict_origin_when_cross_origin -> "strict-origin-when-cross-origin"
    | `Unsafe_url -> "unsafe-url"

  let string_of_big_variant = function
    | `Anonymous -> "anonymous"
    | `Async -> "async"
    | `Autofocus -> "autofocus"
    | `Autoplay -> "autoplay"
    | `Checked -> "checked"
    | `Defer -> "defer"
    | `Disabled -> "disabled"
    | `Muted -> "muted"
    | `ReadOnly -> "readonly"
    | `Rect -> "rect"
    | `Selected -> "selected"
    | `Use_credentials -> "use-credentials"
    | `W3_org_1999_xhtml -> "http://www.w3.org/1999/xhtml"
    | `All -> "all"
    | `Preserve -> "preserve"
    | `Default -> "default"
    | `Controls -> "controls"
    | `Ltr -> "ltr"
    | `Rtl -> "rtl"
    | `Get -> "GET"
    | `Post -> "POST"
    | `Formnovalidate -> "formnovalidate"
    | `Hidden -> "hidden"
    | `Ismap -> "ismap"
    | `Loop -> "loop"
    | `Novalidate -> "novalidate"
    | `Open -> "open"
    | `None -> "none"
    | `Metadata -> "metadata"
    | `Audio -> "audio"
    | `Pubdate -> "pubdate"
    | `Required -> "required"
    | `Reversed -> "reserved"
    | `Scoped -> "scoped"
    | `Seamless -> "seamless"
    | `Any -> "any"
    | `Soft -> "soft"
    | `Hard -> "hard"
    | `Context -> "context"
    | `Toolbar -> "toolbar"
    | `Command -> "command"
    | `Checkbox -> "checkbox"
    | `Radio -> "radio"
    | `Multiple -> "multiple"
    | `Left -> "left"
    | `Right -> "right"
    | `Justify -> "justify"
    | `Char -> "char"
    | `Row -> "row"
    | `Col -> "col"
    | `Rowgroup -> "rowgroup"
    | `Colgroup -> "colgroup"
    | `Groups -> "groups"
    | `Rows -> "rows"
    | `Cols -> "cols"
    | `Zero -> "0"
    | `One -> "1"
    | `Yes -> "yes"
    | `No -> "no"
    | `Auto -> "auto"
    | `Circle -> "circle"
    | `Poly -> "poly"
    | `Alternate -> "alternate"
    | `Archives -> "archives"
    | `Author -> "author"
    | `Bookmark -> "bookmark"
    | `External -> "external"
    | `First -> "first"
    | `Help -> "help"
    | `Icon -> "icon"
    | `Index -> "index"
    | `Last -> "last"
    | `License -> "license"
    | `Next -> "next"
    | `Nofollow -> "nofollow"
    | `Noreferrer -> "noreferrer"
    | `Pingback -> "pingback"
    | `Prefetch -> "prefetch"
    | `Prev -> "prev"
    | `Search -> "search"
    | `Stylesheet -> "stylesheet"
    | `Sidebar -> "sidebar"
    | `Tag -> "tag"
    | `Up -> "up"
    | `Verbatim -> "verbatim"
    | `Latin -> "latin"
    | `Latin_name -> "latin-name"
    | `Latin_prose -> "latin-prose"
    | `Full_width_latin -> "full-width-latin"
    | `Kana -> "kana"
    | `Katakana -> "katakana"
    | `Numeric -> "numeric"
    | `Tel -> "tel"
    | `Email -> "email"
    | `Url -> "url"
    | `Text -> "text"
    | `Decimal -> "decimal"
    | `Other s -> s

  let string_of_input_type = function
    | `Button -> "button"
    | `Checkbox -> "checkbox"
    | `Color -> "color"
    | `Date -> "date"
    | `Datetime -> "datetime"
    | `Datetime_local -> "datetime-local"
    | `Email -> "email"
    | `File -> "file"
    | `Hidden -> "hidden"
    | `Image -> "image"
    | `Month -> "month"
    | `Number -> "number"
    | `Password -> "password"
    | `Radio -> "radio"
    | `Range -> "range"
    | `Readonly -> "readonly"
    | `Reset -> "reset"
    | `Search -> "search"
    | `Submit -> "submit"
    | `Tel -> "tel"
    | `Text -> "text"
    | `Time -> "time"
    | `Url -> "url"
    | `Week -> "week"

  let string_of_script_type = function
    | `Javascript -> "application/javascript"
    | `Module -> "module"
    | `Mime s -> s

  let string_of_number_or_datetime = function
    | `Number n -> string_of_int n
    | `Datetime t -> t

  let string_of_character = String.make 1

  let string_of_number = string_of_int

  let unoption_string = function Some x -> x | None -> ""

  let string_of_step = function Some x -> string_of_float x | None -> "any"

  let string_of_sizes = function
    | Some l ->
        String.concat " "
          (List.map (fun (x, y) -> Printf.sprintf "%dx%d" x y) l)
    | None -> "any"

  let string_of_sandbox l =
    String.concat " " (List.map string_of_sandbox_token l)

  let string_of_numbers l = String.concat "," (List.map string_of_number l)

  let string_of_mediadesc l =
    String.concat ", " (List.map string_of_mediadesc_token l)

  let string_of_linktypes l =
    String.concat " " (List.map string_of_linktype l)

  let string_of_srcset (type a) (f : a -> string)
      (l : [< a Html.Terminology.image_candidate] list) =
    let f = function
      | `Url url -> f url
      | `Url_width (url, v) ->
          Printf.sprintf "%s %sw" (f url) (string_of_number v)
      | `Url_pixel (url, v) ->
          Printf.sprintf "%s %sx" (f url) (string_of_float v)
    in
    String.concat ", " (List.map f l)

  let string_of_autocomplete (l : Html.Terminology.autocomplete_option) =
    match l with
    | `On | `Tokens [] -> "on"
    | `Off -> "off"
    | `Tokens strs -> String.concat " " strs
end

module Make (Model : Model.Sig) = struct
  module Model = Model
  module Svg = SvgMake.Make (Model)

  module At = struct
    module A = Model.At

    type 'a t = A.t

    let user f name v = A.string name (Model.Wrap.map f v)

    let bool = user Bool.to_string

    let linktypes name x = user C.string_of_linktypes name x

    let mediadesc name x = user C.string_of_mediadesc name x

    let srcset name x = user (C.string_of_srcset Model.Uri.to_string) name x

    let constant a () = A.string a (Model.Wrap.pure a)

    let class' = A.space_sep "class"

    let id = A.string "id"

    let user_data name = A.string ("data-" ^ name)

    let title = A.string "title"

    (* I18N: *)
    let xml_lang = A.string "xml:lang"

    let lang = A.string "lang"

    (* Style: *)
    let style = A.string "style"

    let property = A.string "property"

    (* Events: *)
    let onabort x = A.handler "onabort" x

    let onafterprint x = A.handler "onafterprint" x

    let onbeforeprint x = A.handler "onbeforeprint" x

    let onbeforeunload x = A.handler "onbeforeunload" x

    let onblur x = A.handler "onblur" x

    let oncanplay x = A.handler "oncanplay" x

    let oncanplaythrough x = A.handler "oncanplaythrough" x

    let onchange x = A.handler "onchange" x

    let onclose x = A.handler "onclose" x

    let ondurationchange x = A.handler "ondurationchange" x

    let onemptied x = A.handler "onemptied" x

    let onended x = A.handler "onended" x

    let onerror x = A.handler "onerror" x

    let onfocus x = A.handler "onfocus" x

    let onformchange x = A.handler "onformchange" x

    let onforminput x = A.handler "onforminput" x

    let onhashchange x = A.handler "onhashchange" x

    let oninput x = A.handler "oninput" x

    let oninvalid x = A.handler "oninvalid" x

    let onoffline x = A.handler "onoffline" x

    let ononline x = A.handler "ononline" x

    let onpause x = A.handler "onpause" x

    let onplay x = A.handler "onplay" x

    let onplaying x = A.handler "onplaying" x

    let onpagehide x = A.handler "onpagehide" x

    let onpageshow x = A.handler "onpageshow" x

    let onpopstate x = A.handler "onpopstate" x

    let onprogress x = A.handler "onprogress" x

    let onratechange x = A.handler "onratechange" x

    let onreadystatechange x = A.handler "onreadystatechange" x

    let onredo x = A.handler "onredo" x

    let onresize x = A.handler "onresize" x

    let onscroll x = A.handler "onscroll" x

    let onseeked x = A.handler "onseeked" x

    let onseeking x = A.handler "onseeking" x

    let onselect x = A.handler "onselect" x

    let onshow x = A.handler "onshow" x

    let onstalled x = A.handler "onstalled" x

    let onstorage x = A.handler "onstorage" x

    let onsubmit x = A.handler "onsubmit" x

    let onsuspend x = A.handler "onsuspend" x

    let ontimeupdate x = A.handler "ontimeupdate" x

    let onundo x = A.handler "onundo" x

    let onunload x = A.handler "onunload" x

    let onvolumechange x = A.handler "onvolumechange" x

    let onwaiting x = A.handler "onwaiting" x

    let onload x = A.handler "onload" x

    let onloadeddata x = A.handler "onloadeddata" x

    let onloadedmetadata x = A.handler "onloadedmetadata" x

    let onloadstart x = A.handler "onloadstart" x

    let onmessage x = A.handler "onmessage" x

    let onmousewheel x = A.handler "onmousewheel" x

    (** Javascript mouse events *)
    let onclick x = A.handler "onclick" x

    let oncontextmenu x = A.handler "oncontextmenu" x

    let ondblclick x = A.handler "ondblclick" x

    let ondrag x = A.handler "ondrag" x

    let ondragend x = A.handler "ondragend" x

    let ondragenter x = A.handler "ondragenter" x

    let ondragleave x = A.handler "ondragleave" x

    let ondragover x = A.handler "ondragover" x

    let ondragstart x = A.handler "ondragstart" x

    let ondrop x = A.handler "ondrop" x

    let onmousedown x = A.handler "onmousedown" x

    let onmouseup x = A.handler "onmouseup" x

    let onmouseover x = A.handler "onmouseover" x

    let onmousemove x = A.handler "onmousemove" x

    let onmouseout x = A.handler "onmouseout" x

    (** Javascript touch events *)
    let ontouchstart x = A.handler "ontouchstart" x

    let ontouchend x = A.handler "ontouchend" x

    let ontouchmove x = A.handler "ontouchmove" x

    let ontouchcancel x = A.handler "ontouchcancel" x

    (** Javascript keyboard events *)
    let onkeypress x = A.handler "onkeypress" x

    let onkeydown x = A.handler "onkeydown" x

    let onkeyup x = A.handler "onkeyup" x

    (* Other Attributes *)
    let version = A.string "version"

    let xmlns x = user C.string_of_big_variant "xmlns" x

    let manifest = A.uri "manifest"

    let cite = A.uri "cite"

    let xml_space x = user C.string_of_big_variant "xml:space" x

    let accesskey c = user C.string_of_character "accesskey" c

    let charset = A.string "charset"

    let accept_charset = A.space_sep "accept-charset"

    let accept = A.comma_sep "accept"

    let href = A.uri "href"

    let hreflang = A.string "hreflang"

    let download file = user C.unoption_string "download" file

    let rel x = linktypes "rel" x

    let tabindex = A.int "tabindex"

    let mime_type = A.string "type"

    let alt = A.string "alt"

    let height p = A.int "height" p

    let src = A.uri "src"

    let width p = A.int "width" p

    let label_for = A.string "for"

    let output_for = A.space_sep "for"

    let selected = constant "selected"

    let text_value = A.string "value"

    let int_value = A.int "value"

    let value = A.string "value"

    let float_value = A.float "value"

    let action = A.uri "action"

    let method' x = user C.string_of_big_variant "method" x

    let formmethod = method'

    let enctype = A.string "enctype"

    let checked = constant "checked"

    let disabled = constant "disabled"

    let readonly = constant "readonly"

    let maxlength = A.int "maxlength"

    let minlength = A.int "minlength"

    let name = A.string "name"

    let allowfullscreen = constant "allowfullscreen"

    let allowpaymentrequest = constant "allowpaymentrequest"

    let referrerpolicy x = user C.string_of_referrerpolicy "referrerpolicy" x

    let autocomplete x = user C.string_of_autocomplete "autocomplete" x

    let async = constant "async"

    let autofocus = constant "autofocus"

    let autoplay = constant "autoplay"

    let muted = constant "muted"

    let crossorigin x = user C.string_of_big_variant "crossorigin" x

    let integrity = A.string "integrity"

    let mediagroup = A.string "mediagroup"

    let challenge = A.string "challenge"

    let contenteditable ce = bool "contenteditable" ce

    let contextmenu = A.string "contextmenu"

    let controls = constant "controls"

    let dir x = user C.string_of_big_variant "dir" x

    let draggable d = bool "draggable" d

    let form = A.string "form"

    let formaction = A.uri "formaction"

    let formenctype = A.string "formenctype"

    let formnovalidate = constant "formnovalidate"

    let formtarget = A.string "formtarget"

    let hidden = constant "hidden"

    let high = A.float "high"

    let icon = A.uri "icon"

    let ismap = constant "ismap"

    let keytype = A.string "keytype"

    let list = A.string "list"

    let loop = constant "loop"

    let low = A.float "low"

    let max = A.float "max"

    let input_max = user C.string_of_number_or_datetime "max"

    let min = A.float "min"

    let input_min = user C.string_of_number_or_datetime "min"

    let inputmode x = user C.string_of_big_variant "inputmode" x

    let novalidate = constant "novalidate"

    let open' = constant "open"

    let optimum = A.float "optimum"

    let pattern = A.string "pattern"

    let placeholder = A.string "placeholder"

    let poster = A.uri "poster"

    let preload x = user C.string_of_big_variant "preload" x

    let pubdate = constant "pubdate"

    let radiogroup = A.string "radiogroup"

    let required = constant "required"

    let reversed = constant "reserved"

    let sandbox x = user C.string_of_sandbox "sandbox" x

    let spellcheck sc = bool "spellcheck" sc

    let scoped = constant "scoped"

    let seamless = constant "seamless"

    let sizes sizes = user C.string_of_sizes "sizes" sizes

    let span = A.int "span"

    (*let srcdoc*)
    let srcset = srcset "srcset"

    let img_sizes = A.comma_sep "sizes"

    let start = A.int "start"

    let step step = user C.string_of_step "step" step

    let translate x = user C.string_of_big_variant "translate" x

    let wrap x = user C.string_of_big_variant "wrap" x

    let size = A.int "size"

    let input_type it = user C.string_of_input_type "type" it

    let menu_type x = user C.string_of_big_variant "type" x

    let command_type x = user C.string_of_big_variant "type" x

    let button_type bt = user C.string_of_input_type "type" bt

    let script_type sc = user C.string_of_script_type "type" sc

    let multiple = constant "multiple"

    let cols = A.int "cols"

    let rows = A.int "rows"

    let summary = A.string "summary"

    let align x = user C.string_of_big_variant "align" x

    let axis = A.string "axis"

    let colspan = A.int "colspan"

    let headers = A.space_sep "headers"

    let rowspan = A.int "rowspan"

    let scope x = user C.string_of_big_variant "scope" x

    let border = A.int "border"

    let rules x = user C.string_of_big_variant "rules" x

    let char c = user C.string_of_character "char" c

    let data = A.uri "data"

    let codetype = A.string "codetype"

    let frameborder x = user C.string_of_big_variant "frameborder" x

    let marginheight = A.int "marginheight"

    let marginwidth = A.int "marginwidth"

    let scrolling x = user C.string_of_big_variant "scrolling" x

    let target = A.string "target"

    let content = A.string "content"

    let http_equiv = A.string "http-equiv"

    let media = mediadesc "media"

    let datetime = A.string "datetime"

    let shape x = user C.string_of_big_variant "shape" x

    let coords coords = user C.string_of_numbers "coords" coords

    let usemap = A.string "usemap"

    let defer = constant "defer"

    let label = A.string "label"

    let role = A.space_sep "role"

    let aria name = A.space_sep ("aria-" ^ name)
  end

  module El = struct
    module E = Model.El
    module W = Model.Wrap

    let unary tag ?(a = W.empty) elt =
      E.node tag a (Model.Wrap.singleton elt)

    let star tag ?(a = W.empty) elts = E.node tag a elts

    let option_cons opt elts =
      match opt with None -> elts | Some x -> W.cons x elts

    let nullary tag ?(a = W.empty) () = E.node tag a Model.Wrap.empty

    let body = star "body"

    let head = star "head"

    let title = unary "title"

    let footer = star "footer"

    let html ?(a = W.empty) head body =
      let content = W.cons head (W.singleton body) in
      E.node "html" a content

    let header = star "header"

    let section = star "section"

    let nav = star "nav"

    let txt s = E.pcdata s

    let h1 = star "h1"

    let h2 = star "h2"

    let h3 = star "h3"

    let h4 = star "h4"

    let h5 = star "h5"

    let h6 = star "h6"

    let hgroup = star "hgroup"

    let address = star "address"

    let blockquote = star "blockquote"

    let dialog = star "dialog"

    let div = star "div"

    let p = star "p"

    let pre = star "pre"

    let abbr = star "abbr"

    let br = nullary "br"

    let cite = star "cite"

    let code = star "code"

    let dfn = star "dfn"

    let em = star "em"

    let kbd = star "kbd"

    let q = star "q"

    let samp = star "samp"

    let span = star "span"

    let strong = star "strong"

    let time = star "time"

    let var = star "var"

    let a = star "a"

    let dl = star "dl"

    let ol = star "ol"

    let ul = star "ul"

    let dd = star "dd"

    let dt = star "dt"

    let li = star "li"

    let hr = nullary "hr"

    let b = star "b"

    let i = star "i"

    let u = star "u"

    let small = star "small"

    let sub = star "sub"

    let sup = star "sup"

    let mark = star "mark"

    let rp = star "rp"

    let rt = star "rt"

    let ruby = star "ruby"

    let wbr = nullary "wbr"

    let bdo ~dir ?(a = W.empty) elts =
      E.node "bdo" (W.cons (At.dir dir) a) elts

    let area ~alt ?(a = W.empty) () =
      E.node "area" (W.cons (At.alt alt) a) W.empty

    let map = star "map"

    let del = star "del"

    let ins = star "ins"

    let script = unary "script"

    let noscript = star "noscript"

    let template = star "template"

    let article = star "article"

    let aside = star "aside"

    let main = star "main"

    let video_audio name ?src ?srcs ?(a = W.empty) elts =
      let a =
        match src with None -> a | Some uri -> W.cons (At.src uri) a
      in
      match srcs with
      | None -> E.node name a elts
      | Some srcs -> E.node name a (W.append srcs elts)

    let audio = video_audio "audio"

    let video = video_audio "video"

    let canvas = star "canvas"

    let command ~label ?(a = W.empty) () =
      E.node "command" (W.cons (At.label label) a) W.empty

    let menu ?children ?(a = W.empty) () =
      let children =
        match children with
        | None -> W.empty
        | Some (`Lis l) | Some (`Flows l) -> l
      in
      E.node "menu" a children

    let embed = nullary "embed"

    let source = nullary "source"

    let meter = star "meter"

    let output_elt = star "output"

    let form = star "form"

    let svg ?(a = W.empty) children = Svg.El.svg ~a children

    let input = nullary "input"

    let keygen = nullary "keygen"

    let label = star "label"

    let option = unary "option"

    let select = star "select"

    let textarea = unary "textarea"

    let button = star "button"

    let datalist ?children ?(a = W.empty) () =
      let children =
        match children with
        | None -> W.empty
        | Some (`Options x | `Phras x) -> x
      in
      E.node "datalist" a children

    let progress = star "progress"

    let legend = star "legend"

    let details = star "details"

    let summary = star "summary"

    let fieldset ?legend ?(a = W.empty) elts =
      E.node "fieldset" a (option_cons legend elts)

    let optgroup ~label ?(a = W.empty) elts =
      E.node "optgroup" (W.cons (At.label label) a) elts

    let figcaption = star "figcaption"

    let figure ?figcaption ?(a = W.empty) elts =
      let content =
        match figcaption with
        | None -> elts
        | Some (`Top c) -> W.cons c elts
        | Some (`Bottom c) -> W.append elts (W.singleton c)
      in
      E.node "figure" a content

    let caption = star "caption"

    let tablex ?caption ?columns ?thead ?(tfoot = W.empty) ?(a = W.empty)
        elts =
      let content = option_cons thead (W.append tfoot elts) in
      let content = option_cons columns content in
      let content = option_cons caption content in
      E.node "table" a content

    let table = tablex

    let td = star "td"

    let th = star "th"

    let tr = star "tr"

    let colgroup = star "colgroup"

    let col = nullary "col"

    let thead = star "thead"

    let tbody = star "tbody"

    let tfoot = star "tfoot"

    let iframe = star "iframe"

    let object_ ?params ?(a = W.empty) elts =
      let elts =
        match params with None -> elts | Some e -> W.append e elts
      in
      E.node "object" a elts

    let param = nullary "param"

    let img ~src ~alt ?(a = W.empty) () =
      let a = W.cons (At.src src) @@ W.cons (At.alt alt) a in
      E.node "img" a W.empty

    let picture ~img ?(a = W.empty) elts =
      let tail_node = W.cons img W.empty in
      let content = W.append elts tail_node in
      E.node "picture" a content

    let meta = nullary "meta"

    let style ?(a = W.empty) elts = E.node "style" a elts

    let link ~rel ~href ?(a = W.empty) () =
      E.node "link" (W.cons (At.rel rel) @@ W.cons (At.href href) a) W.empty

    let base = nullary "base"

    let node x ?(a = Model.Wrap.empty) l = Model.El.node x a l
  end
end
