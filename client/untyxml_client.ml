open Js_of_ocaml
module Html = Html
module Svg = Svg
module Widget = Widget

module Unsafe = struct
  let read_var variable =
    let content = Js.(to_string @@ Js.Unsafe.pure_js_expr variable) in
    let s = Scanf.sscanf content "%S" (fun s -> s) in
    Marshal.from_string s 0
end

module Utils = Utils
module Request = Request

module Lwd = struct
  include Lwd

  let sample (x : 'a Lwd.t) : 'a = Lwd.(quick_sample (observe x))
end

module Hook = Hook

let ( let*? ) = Promise.( let*? )

let return = Promise.return

module Promise = Promise
