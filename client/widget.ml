open Js_of_ocaml
open Html

let target e = Js.Opt.get e##.target (fun () -> failwith "No target")

let js_value (e : Dom_html.element Js.t) : string =
  Js.to_string @@ Js.Unsafe.get e (Js.string "value")

let event_value (e : LwdModel.Ev.keyboard Js.t) = js_value (target e)

type 'a t = {set: 'a -> unit; value: 'a Lwd.t; co: Co.t}

let content w = w.co

let value w = w.value

let set w x = w.set x

let from_var default f =
  let v = Lwd.var default in
  {co = f v; value = Lwd.get v; set = Lwd.set v}

let embed ~project ~embed widget =
  { widget with
    value = Lwd.map ~f:embed widget.value
  ; set = (fun value -> widget.set (project value)) }

let list : type a. a t list -> a list t =
 fun list ->
  let co = Co.list @@ List.map content list in
  let value = Lwd_utils.flatten_l @@ List.map value list in
  let set l = List.iter2 (fun w v -> set w v) list l in
  {co; value; set}

let pair ?(f = fun x y -> Co.list [x; y]) w1 w2 =
  { value = Lwd.pair w1.value w2.value
  ; co = f w1.co w2.co
  ; set = (fun (x, y) -> w1.set x ; w2.set y) }

(* module Cell = struct
 *   type 'a t =
 *     {lwd: 'a Lwd.t; peek: set:'a -> unit; mutable hooks: ('a -> unit) list}
 * 
 *   let on_change hook cell =
 *     {cell with set = (fun new_value -> hook new_value ; cell.set new_value)}
 * 
 *   let var v =
 *     let v = Lwd.var v in
 *     {lwd = Lwd.get v; peek = (fun () -> Lwd.peek v); set = Lwd.set v}
 * 
 *   let either (side : [`Left | `Right]) x y =
 *     let v = Lwd.var side in
 *     let lwd =
 *       Lwd.bind (Lwd.get v) ~f:(fun b ->
 *           if b = `Left then Lwd.map ~f:Either.left x.lwd
 *           else Lwd.map ~f:Either.right y.lwd )
 *     in
 *     let set = function
 *       | Either.Left a -> x.set a ; Lwd.set v `Left
 *       | Right b -> y.set b ; Lwd.set v `Right
 *     in
 *     let peek () =
 *       if Lwd.peek v = `Left then Either.Left (x.peek ())
 *       else Either.Right (y.peek ())
 *     in
 *     {lwd; set; peek}
 * 
 *   let lwd c = c.lwd
 * 
 *   let set c v = c.set v
 * 
 *   let peek c = c.peek ()
 * 
 *   let project ~project ~upd cell =
 *     { lwd = Lwd.map ~f:project cell.lwd
 *     ; peek = (fun () -> project (cell.peek ()))
 *     ; set = (fun v -> cell.set (upd (cell.peek ()) v)) }
 * 
 *   let map ~forward ~backward cell =
 *     { lwd = Lwd.map ~f:forward cell.lwd
 *     ; peek = (fun () -> forward (cell.peek ()))
 *     ; set = (fun v -> cell.set (backward v)) }
 * 
 *   let pair c1 c2 =
 *     { lwd = Lwd.pair c1.lwd c2.lwd
 *     ; peek = (fun () -> (c1.peek (), c2.peek ()))
 *     ; set = (fun (v1, v2) -> c1.set v1 ; c2.set v2) }
 * 
 *   let const v = {lwd = Lwd.pure v; peek = (fun () -> v); set = (fun _ -> ())}
 * end
 * 
 * type 'a t = 'a Cell.t -> Co.t *)

let textarea ?(a = []) () : string t =
  from_var "" (fun var ->
      let onchange event =
        Lwd.set var (event_value event) ;
        true
      in
      Co.textarea ~a:(a_onkeyup onchange :: a) (Co.txtw (Lwd.get var)) )

let no_hook _ = ()

let input_with_validation ~construct ~validate ~default ?on_change ?(a = [])
    () : _ t =
  let last_seen = Lwd.var default in
  let text = Lwd.var (construct default) in
  let value = Lwd.get last_seen in
  let set x =
    Lwd.set last_seen x ;
    Lwd.set text (construct x)
  in
  let onchange event =
    Lwd.set text (event_value event) ;
    Hook.run_maybe on_change (Lwd.peek text) ;
    ( match validate (event_value event) with
    | Ok x -> Lwd.set last_seen x
    | _ -> () ) ;
    true
  in
  let side =
    Lwd.map (Lwd.get text) ~f:(fun s ->
        match validate s with
        | Ok _ -> Co.empty
        | Error "" -> Co.empty
        | Error e -> Co.span ~cl:["input-error-message"] [Co.txt e] )
  in
  let cl =
    Lwd.map (Lwd.get text) ~f:(fun s ->
        match validate s with
        | Ok _ -> ["input-valid"]
        | _ -> ["input-invalid"] )
  in
  let co =
    Co.list
      [ Co.input
          ~a:
            At.(
              (a_value @:!= Lwd.get text)
              :: (a_class @:!= cl) :: a_onkeyup onchange :: a)
          ()
      ; Co.reactive side ]
  in
  {co; set; value}

let input =
  input_with_validation
    ~construct:(fun s -> s)
    ~validate:(fun s -> Ok s)
    ~default:""

let int default =
  input_with_validation ~construct:string_of_int ~default
    ~validate:(fun s ->
      try Ok (int_of_string s) with _ -> Error "Number expected" )
    ()

let float default =
  input_with_validation ~construct:string_of_float ~default
    ~validate:(fun s ->
      try Ok (float_of_string s) with _ -> Error "Number expected" )
    ()

let bool ?on_change ?(cl = "button") ?(a = []) element : bool t =
  from_var false (fun var ->
      let cl =
        Lwd.map (Lwd.get var) ~f:(fun selected ->
            if selected then [cl; "selected"] else [cl; "unselected"] )
      in
      let onclick _ =
        let b = not (Lwd.peek var) in
        Hook.run_maybe on_change b ;
        Lwd.set var b ;
        true
      in
      Co.div ~a:At.((a_class @:!= cl) :: a_onclick onclick :: a) element )

let map_co f (widget : 'a t) : 'a t = {widget with co = f widget.co}

let before elem x = map_co (fun c -> Co.list [elem; c]) x

let after elem x = map_co (fun c -> Co.list [c; elem]) x

(* let either (type a b) (left : (b -> unit) -> a t) (right : (a -> unit) ->
   b t) : (a, b) Either.t t = fun init -> let widget_left = Lwd.var None in
   let widget_right = Lwd.var None in let is_left = Lwd.var false in let rec
   set = function | Either.Left x -> ( Lwd.set is_left true ; match Lwd.peek
   widget_left with | None -> Lwd.set widget_left (Some (left (fun b -> set
   (Right b)) x)) | Some w -> w.set x ) | Right y -> ( Lwd.set is_left false
   ; match Lwd.peek widget_right with | None -> Lwd.set widget_right (Some
   (right (fun a -> set (Left a)) y)) | Some w -> w.set y ) in set init ; let
   state = Lwd.map (Lwd.get is_left) ~f:(fun b -> if b then Either.Left
   (Option.get (Lwd.peek widget_left)) else Either.Right (Option.get
   (Lwd.peek widget_right)) ) in let lwd = Lwd.bind state ~f:(function | Left
   x -> Lwd.map ~f:Either.left x.lwd | Right y -> Lwd.map ~f:Either.right
   y.lwd ) in let content = Co.reactive @@ Lwd.map state ~f:(function | Left
   x -> x.content | Right y -> y.content ) in {lwd; set; content} *)
(* *)

let action ?title:tit ?(a = []) ?cl f =
  Co.a
    ~a:
      ( a
      @ At.
          [ a_onclick (fun _ -> f () ; true)
          ; a_title @:?= tit
          ; a_class @:?= cl ] )

(*let action_ev ?title ?a ?cl content = let ev, trigger = Event.create () in
  let elem = action ?title ?a ?cl trigger content in (elem, ev)*)

let action_err ?title ?cl f elt =
  let status = Lwd.var `Hidden in
  let dep_cl =
    let f = function
      | `Hidden -> ["hidden"]
      | `Success _ -> ["success"]
      | `Progress -> ["progress"]
      | `Error _ -> ["error"]
    in
    Lwd.map ~f (Lwd.get status)
  in
  let sp =
    let children =
      Lwd.map (Lwd.get status) ~f:(function
        | `Hidden | `Progress -> Co.empty
        | `Success e -> Co.txt e
        | `Error e -> Co.txt e )
    in
    Co.span ~a:At.[a_class @:!= dep_cl] [Co.reactive children]
  in
  let ( let* ) = Lwt.bind in
  let f' () =
    Lwd.set status `Progress ;
    let* result = f () in
    ( match result with
    | Ok x ->
        ignore
        @@ Dom_html.setTimeout (fun () -> Lwd.set status `Hidden) 1500. ;
        Lwd.set status (`Success x)
    | Error e ->
        ignore
        @@ Dom_html.setTimeout (fun () -> Lwd.set status `Hidden) 2000. ;
        Lwd.set status (`Error e) ) ;
    Lwt.return ()
  in
  let button = action ?title ?cl (fun () -> Lwt.async f') elt in
  Co.span [button; sp]
