open Js_of_ocaml

module WrapValue = struct
  type 'a t = 'a Lwd.t

  let pure = Lwd.pure

  let map f x = Lwd.map ~f x
end

module WrapSeq = struct
  type 'a t = 'a Lwd_seq.t Lwd.t

  let empty = Lwd.pure Lwd_seq.empty

  let append = Lwd.map2 ~f:Lwd_seq.concat

  let singleton x = Lwd.pure (Lwd_seq.element x)

  let cons x l = append (singleton x) l
end

module Ev = struct
  type 'a handler = 'a Js.t -> bool

  type base = Dom_html.event

  type keyboard = Dom_html.keyboardEvent

  type mouse = Dom_html.mouseEvent

  type touch = Dom_html.touchEvent
end

module Uri = Untyxml_f.Default.UriString

module At = struct
  type value =
    | String : string option Lwd.t -> value
    | Handler : 'a Ev.handler -> value

  type t = string * value

  let string name s = (name, String s)

  let handler a f = (a, Handler f)
end

module Co = struct
  type t = Dom.node Js.t Lwd_seq.t Lwd.t

  let empty = Lwd.pure Lwd_seq.empty

  let list l = List.fold_left WrapSeq.append WrapSeq.empty l

  let seq (l : t Lwd_seq.t Lwd.t) : t = Lwd_seq.bind l (fun x -> x)

  let txt (text : string Lwd.t) : t =
    match Lwd.is_pure text with
    | Some s ->
        Lwd.pure
          (Lwd_seq.element
             ( Dom_html.document##createTextNode (Js.string s)
               :> Dom.node Js.t ) )
    | None ->
        let node =
          Lwd_seq.element (Dom_html.document##createTextNode (Js.string ""))
        in
        Lwd.map text ~f:(fun text ->
            ( match Lwd_seq.view node with
            | Lwd_seq.Element elt -> elt##.data := Js.string text
            | _ -> assert false ) ;
            (node : Dom.text Js.t Lwd_seq.t :> Dom.node Js.t Lwd_seq.t) )

  let attach_attrib_pure (node : #Dom.element Js.t) name =
    match name with
    | "style" -> (
        function
        | None -> node##.style##.cssText := Js.string ""
        | Some v -> node##.style##.cssText := Js.string v )
    | "value" -> (
        function
        | None -> (Obj.magic node : _ Js.t)##.value := Js.string ""
        | Some v -> (Obj.magic node : _ Js.t)##.value := Js.string v )
    | name -> (
        let name = Js.string name in
        function
        | None -> node##removeAttribute name
        | Some v -> node##setAttribute name (Js.string v) )

  let attach_event (node : #Dom.element Js.t) name value =
    Js.Unsafe.set node (Js.string name) (fun ev -> Js.bool (value ev))

  let attach_attribs node l =
    Lwd_utils.pack
      ((), fun () () -> ())
      (List.map
         (fun (name, value) ->
           match value with
           | At.Handler h -> Lwd.pure (attach_event node name h)
           | String s -> (
             match Lwd.is_pure s with
             | Some None -> Lwd.pure ()
             | Some (Some s) ->
                 Lwd.pure (attach_attrib_pure node name (Some s))
             | None -> Lwd.map s ~f:(attach_attrib_pure node name) ) )
         l )

  let createElement ~ns name =
    let name = Js.string name in
    match ns with
    | None -> Dom_html.document##createElement name
    | Some ns -> Dom_html.document##createElementNS ns name

  let rec find_ns : At.t list -> Js.js_string Js.t option = function
    | [] -> None
    | ("xmlns", String value) :: _ -> (
      (* The semantics should not differ whether an Lwd value is pure or not,
         but let's do an exception for xml namespaces (those are managed
         differently from other and can't be changed at runtime). *)
      match Lwd.is_pure value with
      | None | Some None ->
          prerr_endline "xmlns attribute should be static" ;
          None
      | Some (Some x) -> Some (Js.string x) )
    | _ :: rest -> find_ns rest

  type child_tree =
    | Leaf of Dom.node Js.t
    | Inner of
        { mutable bound: Dom.node Js.t Js.opt
        ; left: child_tree
        ; right: child_tree }

  let child_node node = Leaf node

  let child_join left right = Inner {bound = Js.null; left; right}

  let js_lwd_to_remove = Js.string "lwd-to-remove"
  (* HACK Could be turned into a Javascript symbol *)

  let contains_focus node =
    Js.to_bool
      (Js.Unsafe.meth_call
         (node : Dom.node Js.t)
         "contains"
         [|Js.Unsafe.inject Dom_html.document##.activeElement|] )

  let attach (self : Dom.node Js.t) (children : Dom.node Js.t Lwd_seq.t Lwd.t)
      : unit Lwd.t =
    let reducer =
      ref (Lwd_seq.Reducer.make ~map:child_node ~reduce:child_join)
    in
    Lwd.map children ~f:(fun children ->
        let dropped, reducer' =
          Lwd_seq.Reducer.update_and_get_dropped !reducer children
        in
        reducer := reducer' ;
        let schedule_for_removal child () =
          match child with
          | Leaf node -> Js.Unsafe.set node js_lwd_to_remove Js._true
          | Inner _ -> ()
        in
        Lwd_seq.Reducer.fold_dropped `Map schedule_for_removal dropped () ;
        let preserve_focus = contains_focus self in
        ( match Lwd_seq.Reducer.reduce reducer' with
        | None -> ()
        | Some tree ->
            let rec update acc = function
              | Leaf x ->
                  Js.Unsafe.delete x js_lwd_to_remove ;
                  if x##.parentNode != Js.some self then
                    ignore (self##insertBefore x acc)
                  else if x##.nextSibling != acc then
                    if
                      (* Parent is correct but sibling is not: swap nodes,
                         but be cautious with focus *)
                      preserve_focus && contains_focus x
                    then (
                      let rec shift_siblings () =
                        let sibling = x##.nextSibling in
                        if sibling == acc then true
                        else
                          match Js.Opt.to_option sibling with
                          | None -> false
                          | Some sibling ->
                              ignore (self##insertBefore sibling (Js.some x)) ;
                              shift_siblings ()
                      in
                      if not (shift_siblings ()) then
                        ignore (self##insertBefore x acc) )
                    else ignore (self##insertBefore x acc) ;
                  Js.some x
              | Inner t ->
                  if Js.Opt.test t.bound then t.bound
                  else
                    let acc = update acc t.right in
                    let acc = update acc t.left in
                    t.bound <- acc ;
                    acc
            in
            ignore (update Js.null tree) ) ;
        let remove_child child () =
          match child with
          | Leaf node ->
              if Js.Opt.test (Js.Unsafe.get node js_lwd_to_remove) then
                ignore (self##removeChild node)
          | Inner _ -> ()
        in
        Lwd_seq.Reducer.fold_dropped `Map remove_child dropped () )

  type elem = Dom_html.element Js.t * t

  let node name a (children : t) : elem =
    let e = createElement ~ns:(find_ns a) name in
    let e' = Lwd_seq.element e in
    ( e
    , Lwd.map2
        (attach (e :> Dom.node Js.t) children)
        (attach_attribs e a)
        ~f:(fun () () -> (e' :> Dom.node Js.t Lwd_seq.t)) )

  let elem = snd

  let reactive (x : t Lwd.t) : t = seq (Lwd.map ~f:Lwd_seq.element x)

  let reactive_list (x : t list Lwd.t) : t = reactive (Lwd.map ~f:list x)
end

let attach = Co.attach
