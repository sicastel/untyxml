open Js_of_ocaml

include
  Untyxml_f.Model.Sig
    with type Co.t = Dom.node Js.t Lwd_seq.t Lwd.t
    with type 'a WrapValue.t = 'a Lwd.t
    with type 'a WrapSeq.t = 'a Lwd_seq.t Lwd.t
    with type 'a Ev.handler = 'a Js.t -> bool
    with type Uri.t = string
    with type Ev.touch = Dom_html.touchEvent
    with type Ev.keyboard = Dom_html.keyboardEvent
    with type Ev.mouse = Dom_html.mouseEvent
    with type Ev.base = Dom_html.event
    with type Co.elem = Dom_html.element Js.t * Dom.node Js.t Lwd_seq.t Lwd.t

val attach : Dom.node Js.t -> Dom.node Js.t Lwd_seq.t Lwd.t -> unit Lwd.t
