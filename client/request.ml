open Js_of_ocaml_lwt
open XmlHttpRequest
open Promise

let content_of_frame frame =
  if frame.code = 200 then return frame.content
  else fail "Request failed with code %d" frame.code

let get url : string Promise.t =
  let*? frame = Promise.protect (fun () -> get url) in
  content_of_frame frame

let post url content =
  let*? frame =
    protect (fun () ->
        perform_raw_url ~override_method:`POST ~contents:(`String content)
          url )
  in
  content_of_frame frame

let json ~to_json ~of_json url obj =
  let*? x = post url (Yojson.Safe.to_string @@ to_json obj) in
  match of_json (Yojson.Safe.from_string x) with
  | Error e -> fail "Invalid json reply: %s\n" e
  | Ok (Error e) -> fail "Server error: %s" e
  | Ok (Ok x) -> Promise.return x
