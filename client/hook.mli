type 'a t

val create : unit -> 'a t

val add : 'a t -> f:('a -> unit) -> unit

val run_maybe : 'a t option -> 'a -> unit

val func : ('a -> unit) -> 'a t
