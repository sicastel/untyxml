type 'a t = ('a, string) Result.t Lwt.t

let ( let* ) = Lwt.bind

let return x = Lwt.return (Ok x)

let ( let*? ) m f =
  let* x = m in
  match x with Error e -> Lwt.return (Error e) | Ok x -> f x

let fail fmt = Printf.kprintf (fun x -> Lwt.return (Error x)) fmt

let protect (x : unit -> 'a Lwt.t) : 'a t =
  Lwt.catch
    (fun () -> Lwt.map (fun x -> Ok x) (x ()))
    (function
      | Failure s -> Lwt.return (Error s)
      | e -> Lwt.return (Error (Printexc.to_string e)) )
