open Js_of_ocaml

let get_hash () =
  let s = Js.to_string @@ Js.decodeURI Dom_html.window##.location##.hash in
  try String.sub s 1 (String.length s - 1) with _ -> ""

let set_hash s =
  Dom_html.window##.location##.hash := Js.encodeURI @@ Js.string s

let hash = Lwd.var (get_hash ())

let run (x : unit Lwd.t) : unit =
  let root = Lwd.observe x in
  Lwd.set_on_invalidate root (fun _ -> Lwd.quick_sample root) ;
  Lwd.quick_sample root

let setup () =
  (* run (Lwd.map ~f:set_hash (Lwd.get hash)) ;*)
  Dom_html.window##.onhashchange
  := Dom_html.handler (fun _ ->
         let _new_hash = get_hash () in
         (* if Lwd.peek hash <> new_hash then Lwd.set hash new_hash ;*)
         Js._false )

let attach ?(document = Dom_html.document) ~id children =
  let x =
    Js.Opt.get
      (document##getElementById (Js.string id))
      (fun _ -> failwith ("Cannot find id " ^ id))
  in
  let root = Lwd.observe (LwdModel.attach (x :> Dom.node Js.t) children) in
  let update_dom _dt = Lwd.quick_sample root in
  Lwd.set_on_invalidate root (fun _ ->
      ignore
      @@ Dom_html.window##requestAnimationFrame (Js.wrap_callback update_dom) ) ;
  update_dom ()
