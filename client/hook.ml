type 'a t = ('a -> unit) list ref

let create () = ref []

let add hook ~f = hook := f :: !hook

let run_maybe hook value =
  match hook with None -> () | Some l -> List.iter (fun f -> f value) !l

let func f = ref [f]
