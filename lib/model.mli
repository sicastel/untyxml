(** A model is a way to construct Xml document. Main instances will be the
    Dom (using js_of_ocaml) or a Xml library. *)
module type Sig = sig
  module WrapValue : sig
    type +'a t

    val pure : 'a -> 'a t

    val map : ('a -> 'b) -> 'a t -> 'b t
  end

  module WrapSeq : sig
    type +'a t

    val empty : 'a t

    val singleton : 'a -> 'a t

    val cons : 'a -> 'a t -> 'a t

    val append : 'a t -> 'a t -> 'a t
  end

  module Ev : sig
    type -'a handler

    type base

    type mouse

    type touch

    type keyboard
  end

  module Uri : sig
    type t

    val of_string : string -> t

    val to_string : t -> string
  end

  module At : sig
    type t

    val string : string -> string option WrapValue.t -> t

    val handler : string -> 'a Ev.handler -> t
  end

  module Co : sig
    type t

    type elem

    val txt : string WrapValue.t -> t

    val empty : t

    val list : t list -> t

    val seq : t WrapSeq.t -> t

    val node : string -> At.t list -> t -> elem

    val reactive : t WrapValue.t -> t

    val reactive_list : t list WrapValue.t -> t

    val elem : elem -> t
  end
end
