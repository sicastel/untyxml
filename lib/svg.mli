module Terminology : sig
  (** {2 Attributes} *)
  type conditional_processing =
    [`RequiredExtensions | `RequiredFeatures | `SystemLanguage]

  type core = [`Id | `Xml_base | `Xml_lang | `Xml_space | `User_data]

  type transfer =
    [ `Type_transfert
    | `TableValues
    | `Slope
    | `Intercept
    | `Amplitude
    | `Exponent
    | `Offset_transfer ]

  type document_event =
    [`OnAbort | `OnError | `OnResize | `OnScroll | `OnUnload | `OnZoom]

  type filter_primitive = [`Height | `Result | `Width | `X | `Y]

  type animation_attr_target = [`AttributeType | `AttributeName]

  type animation_timing =
    [ `Begin
    | `Dur
    | `End
    | `Min
    | `Max
    | `Restart
    | `RepeatCount
    | `RepeatDur
    | `Fill_Animation ]

  type animation_value =
    [`CalcMode | `Valuesanim | `KeyTimes | `KeySplines | `From | `To | `By]

  type animation_addition = [`Additive | `Accumulate]

  type presentation =
    [ `Alignment_Baseline
    | `Baseline_Shift
    | `Clip
    | `Clip_Path
    | `Clip_Rule
    | `Color
    | `Color_Interpolation
    | `Color_interpolation_filters
    | `Color_profile
    | `Color_rendering
    | `Cursor
    | `Direction
    | `Display
    | `Dominant_Baseline
    | `Enable_background
    | `Fill
    | `Fill_opacity
    | `Fill_rule
    | `Filter
    | `Flood_Color
    | `Flood_Opacity
    | `Font_Family
    | `Font_Size
    | `Font_Size_Adjust
    | `Font_Stretch
    | `Font_Style
    | `Font_Variant
    | `Font_Weight
    | `Glyph_Orientation_Horizontal
    | `Glyph_Orientation_Vertical
    | `Image_Rendering
    | `Kerning
    | `Letter_Spacing
    | `Lighting_Color
    | `Marker_End
    | `Marker_Mid
    | `Marker_Start
    | `Mask
    | `Opacity
    | `Overflow
    | `Pointer_Events
    | `Shape_Rendering
    | `Stop_Color
    | `Stop_Opacity
    | `Stroke
    | `Stroke_Dasharray
    | `Stroke_Dashoffset
    | `Stroke_Linecap
    | `Stroke_Linejoin
    | `Stroke_Miterlimit
    | `Stroke_Opacity
    | `Stroke_Width
    | `Text_Anchor
    | `Text_Decoration
    | `Text_Rendering
    | `Unicode_Bidi
    | `Visibility
    | `Word_Spacing
    | `Writing_Mode ]

  type graphical_event =
    [ `OnActivate
    | `OnClick
    | `OnFocusIn
    | `OnFocusOut
    | `OnLoad
    | `OnMouseDown
    | `OnMouseMove
    | `OnMouseOut
    | `OnMouseOver
    | `OnMouseUp ]

  type xlink =
    [ `Xlink_href
    | `Xlink_type
    | `Xlink_role
    | `Xlink_arcrole
    | `Xlink_title
    | `Xlink_show
    | `Xlink_actuate ]

  (** {2 Generic data types} *)

  (** An IRI reference is an Internationalized Resource Identifier with an
      optional fragment identifier, as defined in Internationalized Resource
      Identifiers [RFC3987]. An IRI reference serves as a reference to a
      resource or (with a fragment identifier) to a secondary resource. See
      References and the ‘defs’ element.. *)
  type iri = string

  (** {2 Units} *)

  (** SVG defines several units to measure time, length, angles. *)
  module Unit : sig
    type 'a quantity = float * 'a option

    type angle = [`Deg | `Grad | `Rad] quantity

    type length =
      [`Em | `Ex | `Px | `In | `Cm | `Mm | `Pt | `Pc | `Percent] quantity

    type time = [`S | `Ms] quantity

    type frequency = [`Hz | `KHz] quantity
  end

  open Unit

  type coord = length

  type number = float

  type number_optional_number = number * number option

  type percentage = float

  type strings = string list

  type color = string

  type icccolor = string

  type paint_whitout_icc =
    [`None | `CurrentColor | `Color of color * icccolor option]

  type paint = [paint_whitout_icc | `Icc of iri * paint_whitout_icc option]

  type fill_rule = [`Nonzero | `Evenodd]

  (* Transformation *)
  type transform =
    [ `Matrix of float * float * float * float * float * float
    | `Translate of float * float option
    | `Scale of float * float option
    | `Rotate of angle * (float * float) option
    | `SkewX of angle
    | `SkewY of angle ]

  type spacestrings = string list

  type commastrings = string list

  type transforms = transform list

  type fourfloats = float * float * float * float

  type lengths = length list

  type numbers = float list

  type numbers_semicolon = float list

  type coords = (float * float) list

  type rotate = float list
end

module Attrib : sig
  type conditional_processing =
    [`RequiredExtensions | `RequiredFeatures | `SystemLanguage]

  type core = [`Id | `Xml_base | `Xml_lang | `Xml_space | `User_data]

  type transfer =
    [ `Type_transfert
    | `TableValues
    | `Slope
    | `Intercept
    | `Amplitude
    | `Exponent
    | `Offset_transfer ]

  type document_event =
    [`OnAbort | `OnError | `OnResize | `OnScroll | `OnUnload | `OnZoom]

  type filter_primitive = [`Height | `Result | `Width | `X | `Y]

  type animation_event = [`OnBegin | `OnEnd | `OnRepeat | `OnLoad]

  type animation_attr_target = [`AttributeType | `AttributeName]

  type animation_timing =
    [ `Begin
    | `Dur
    | `End
    | `Min
    | `Max
    | `Restart
    | `RepeatCount
    | `RepeatDur
    | `Fill_Animation ]

  type animation_value =
    [`CalcMode | `Valuesanim | `KeyTimes | `KeySplines | `From | `To | `By]

  type animation_addition = [`Additive | `Accumulate]

  type presentation =
    [ `Alignment_Baseline
    | `Baseline_Shift
    | `Clip
    | `Clip_Path
    | `Clip_Rule
    | `Color
    | `Color_Interpolation
    | `Color_interpolation_filters
    | `Color_profile
    | `Color_rendering
    | `Cursor
    | `Direction
    | `Display
    | `Dominant_Baseline
    | `Enable_background
    | `Fill
    | `Fill_opacity
    | `Fill_rule
    | `Filter
    | `Flood_Color
    | `Flood_Opacity
    | `Font_Family
    | `Font_Size
    | `Font_Size_Adjust
    | `Font_Stretch
    | `Font_Style
    | `Font_Variant
    | `Font_Weight
    | `Glyph_Orientation_Horizontal
    | `Glyph_Orientation_Vertical
    | `Image_Rendering
    | `Kerning
    | `Letter_Spacing
    | `Lighting_Color
    | `Marker_End
    | `Marker_Mid
    | `Marker_Start
    | `Mask
    | `Opacity
    | `Overflow
    | `Pointer_Events
    | `Shape_Rendering
    | `Stop_Color
    | `Stop_Opacity
    | `Stroke
    | `Stroke_Dasharray
    | `Stroke_Dashoffset
    | `Stroke_Linecap
    | `Stroke_Linejoin
    | `Stroke_Miterlimit
    | `Stroke_Opacity
    | `Stroke_Width
    | `Text_Anchor
    | `Text_Decoration
    | `Text_Rendering
    | `Unicode_Bidi
    | `Visibility
    | `Word_Spacing
    | `Writing_Mode ]

  type graphical_event =
    [ `OnActivate
    | `OnClick
    | `OnFocusIn
    | `OnFocusOut
    | `OnLoad
    | `OnMouseDown
    | `OnMouseMove
    | `OnMouseOut
    | `OnMouseOver
    | `OnMouseUp ]

  type xlink =
    [ `Xlink_href
    | `Xlink_type
    | `Xlink_role
    | `Xlink_arcrole
    | `Xlink_title
    | `Xlink_show
    | `Xlink_actuate ]

  type svg =
    [ conditional_processing
    | core
    | document_event
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `X
    | `Y
    | `Width
    | `Height
    | `ViewBox
    | `PreserveAspectRatio
    | `ZoomAndPlan
    | `Version
    | `BaseProfile
    | `ContentScriptType
    | `ContentStyleType
    | `X
    | `Y ]

  type g =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform ]

  type defs =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform ]

  type desc = [core | `Class | `Style]

  type title = desc

  type symbol =
    [ `Class
    | `Style
    | `ExternalResourcesRequired
    | `PreserveAspectRatio
    | `ViewBox ]

  type use =
    [ core
    | conditional_processing
    | graphical_event
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `X
    | `Y
    | `Width
    | `Height
    | `Xlink_href ]

  type image =
    [ core
    | conditional_processing
    | graphical_event
    | xlink
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `PreserveAspectRatio
    | `Transform
    | `X
    | `Y
    | `Width
    | `Height
    | `Xlink_href ]

  type switch =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform ]

  type style = [core | `Title | `Media | `Type]

  type path =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `D
    | `PathLength ]

  type rect =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `X
    | `Y
    | `Width
    | `Height
    | `Rx
    | `Ry ]

  type circle =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `R
    | `Cx
    | `Cy ]

  type ellipse =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `Rx
    | `Ry
    | `Cx
    | `Cy ]

  type line =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `X1
    | `Y1
    | `X2
    | `Y2 ]

  type polyline =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `Points ]

  type polygon =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `Points ]

  type text =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | `Class
    | `Transform
    | `LengthAdjust
    | `X_list
    | `Y_list
    | `Dx_list
    | `Dy_list
    | `Rotate
    | `TextLength ]

  type tspan =
    [ core
    | conditional_processing
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `X_list
    | `Y_list
    | `Dx_list
    | `Dy_list
    | `Rotate
    | `TextLength
    | `LengthAdjust ]

  type tref =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Xlink_href ]

  type textpath =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Xlink_href
    | `StartOffset
    | `Method
    | `Spacing ]

  type altglyph =
    [ conditional_processing
    | core
    | graphical_event
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `X_list
    | `Y_list
    | `Dx_list
    | `Dy_list
    | `GlyphRef
    | `Format
    | `Rotate
    | `Xlink_href ]

  type altglyphdef = [ | core]

  type altglyphitem = [ | core]

  type glyphref =
    [ core
    | presentation
    | xlink
    | `Class
    | `Style
    | `X
    | `Y
    | `Dx
    | `Dy
    | `GlyphRef
    | `Format
    | `Xlink_href ]

  type marker =
    [ core
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `ViewBox
    | `PreserveAspectRatio
    | `RefX
    | `RefY
    | `MarkerUnits
    | `MarkerWidth
    | `MarkerHeight
    | `Orient ]

  type colorprofile =
    [core | xlink | `Local | `Name | `Rendering_Intent | `Xlink_href]

  type lineargradient =
    [ core
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `X1
    | `Y1
    | `X2
    | `Y2
    | `GradientUnits
    | `GradientTransform
    | `SpreadMethod
    | `Xlink_href ]

  type radialgradient =
    [ core
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Cx
    | `Cy
    | `R
    | `Fx
    | `Fy
    | `GradientUnits
    | `GradientTransform
    | `SpreadMethod
    | `Xlink_href ]

  type stop = [core | presentation | `Class | `Style | `Offset]

  type pattern =
    [ conditional_processing
    | core
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `ViewBox
    | `PreserveAspectRatio
    | `X
    | `Y
    | `Width
    | `Height
    | `PatternUnits
    | `PatternContentUnits
    | `PatternTransform
    | `Xlink_href ]

  type clippath =
    [ conditional_processing
    | core
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `ClipPathUnits ]

  type mask =
    [ conditional_processing
    | core
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `X
    | `Y
    | `Width
    | `Height
    | `MaskUnits
    | `MaskContentUnits ]

  type filter =
    [ core
    | presentation
    | xlink
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `X
    | `Y
    | `Width
    | `Height
    | `FilterRes
    | `FilterUnits
    | `PrimitiveUnits
    | `Xlink_href ]

  type fedistantlight = [core | `Azimuth | `Elevation]

  type fepointlight = [core | `X | `Y | `Z]

  type fespotlight =
    [ core
    | `X
    | `Y
    | `Z
    | `PointsAtX
    | `PointsAtY
    | `PointsAtZ
    | `SpecularExponent
    | `LimitingConeAngle ]

  type feblend =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `In
    | `In2
    | `Mode ]

  type fecolormatrix =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `Typefecolor
    | `Values
    | `In ]

  type fecomponenttransfer =
    [core | presentation | filter_primitive | `Class | `Style | `In]

  type fefunca = [core | transfer]

  type fefuncg = [core | transfer]

  type fefuncb = [core | transfer]

  type fefuncr = [core | transfer]

  type fecomposite =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `In
    | `In2
    | `OperatorComposite
    | `K1
    | `K2
    | `K3
    | `K4 ]

  type feconvolvematrix =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `In
    | `Order
    | `KernelMatrix
    | `Divisor
    | `Bias
    | `TargetX
    | `TargetY
    | `EdgeMode
    | `KernelUnitLength
    | `PreserveAlpha ]

  type fediffuselighting =
    [ core
    | filter_primitive
    | presentation
    | `Class
    | `Style
    | `In
    | `SurfaceScale
    | `DiffuseConstant
    | `KernelUnitLength ]

  type fedisplacementmap =
    [ core
    | filter_primitive
    | presentation
    | `Class
    | `Style
    | `In
    | `In2
    | `Scale
    | `XChannelSelector
    | `YChannelSelector ]

  type feflood = [core | presentation | filter_primitive | `Class | `Style]

  type fegaussianblur =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `In
    | `StdDeviation ]

  type feimage =
    [ core
    | presentation
    | filter_primitive
    | xlink
    | `Xlink_href
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `PreserveAspectRadio ]

  type femerge = [core | presentation | filter_primitive | `Class | `Style]

  type femorphology =
    [ core
    | presentation
    | filter_primitive
    | `OperatorMorphology
    | `Class
    | `Style
    | `In
    | `Radius ]

  type feoffset =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `Dx
    | `Dy
    | `In ]

  type fespecularlighting =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `In
    | `SurfaceScale
    | `SpecularConstant
    | `SpecularExponent
    | `KernelUnitLength ]

  type fetile =
    [core | presentation | filter_primitive | `Class | `Style | `In]

  type feturbulence =
    [ core
    | presentation
    | filter_primitive
    | `Class
    | `Style
    | `BaseFrequency
    | `NumOctaves
    | `Seed
    | `StitchTiles
    | `TypeStitch ]

  type cursor =
    [ core
    | conditional_processing
    | xlink
    | `X
    | `Y
    | `ExternalResourcesRequired
    | `Xlink_href ]

  type a =
    [ core
    | conditional_processing
    | xlink
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `Xlink_href
    | `Xlink_show
    | `Xlink_actuate
    | `Target ]

  type view =
    [ core
    | `ExternalResourcesRequired
    | `ViewBox
    | `PreserveAspectRatio
    | `ZoomAndPan
    | `ViewTarget ]

  type animation =
    [ conditional_processing
    | core
    | animation_event
    | xlink
    | animation_attr_target
    | animation_timing
    | animation_value
    | animation_addition
    | `ExternalResourcesRequired ]

  type set =
    [ core
    | conditional_processing
    | xlink
    | animation_event
    | animation_attr_target
    | animation_timing
    | `To
    | `ExternalResourcesRequired ]

  type animatemotion =
    [ conditional_processing
    | core
    | animation_event
    | xlink
    | animation_timing
    | animation_value
    | animation_addition
    | `ExternalResourcesRequired
    | `Path
    | `KeyPoints
    | `Rotate
    | `Origin ]

  type mpath = [core | xlink | `ExternalResourcesRequired | `Xlink_href]

  type animatecolor =
    [ conditional_processing
    | core
    | animation_event
    | xlink
    | animation_attr_target
    | animation_timing
    | animation_value
    | animation_addition
    | `ExternalResourcesRequired ]

  type animatetransform =
    [ conditional_processing
    | core
    | animation_event
    | xlink
    | animation_attr_target
    | animation_timing
    | animation_value
    | animation_addition
    | `ExternalResourcesRequired
    | `Typeanimatetransform ]

  type font =
    [ core
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `HorizOriginX
    | `HorizOriginY
    | `HorizAdvX
    | `VertOriginX
    | `VertOriginY
    | `VertAdvY ]

  type glyph =
    [ core
    | presentation
    | `Class
    | `Style
    | `D
    | `HorizAdvX
    | `VertOriginX
    | `VertOriginY
    | `VertAdvY
    | `Unicode
    | `GlyphName
    | `Orientation
    | `ArabicForm
    | `Lang ]

  type missingglyph =
    [ core
    | presentation
    | `Class
    | `Style
    | `D
    | `HorizAdvX
    | `VertOriginX
    | `VertOriginY
    | `VertAdvY ]

  type hkern = [core | `U1 | `G1 | `U2 | `G2 | `K]

  type vkern = [core | `U1 | `G1 | `U2 | `G2 | `K]

  type font_face =
    [ core
    | `Font_Family
    | `Font_Style
    | `Font_Variant
    | `Font_Weight
    | `Font_Stretch
    | `Font_Size
    | `UnicodeRange
    | `UnitsPerEm
    | `Panose1
    | `Stemv
    | `Stemh
    | `Slope
    | `CapHeight
    | `XHeight
    | `AccentHeight
    | `Ascent
    | `Descent
    | `Widths
    | `Bbox
    | `Ideographic
    | `Alphabetic
    | `Mathematical
    | `Hanging
    | `VIdeographic
    | `VAlphabetic
    | `VMathematical
    | `VHanging
    | `UnderlinePosition
    | `UnderlineThickness
    | `StrikethroughPosition
    | `StrikethroughThickness
    | `OverlinePosition
    | `OverlineThickness ]

  type font_face_src = core

  type font_face_uri = [core | xlink | `Xlink_href]

  type font_face_format = [core | `String]

  type font_face_name = [core | `Name]

  type metadata = [ | core]

  type script =
    [core | xlink | `ExternalResourcesRequired | `Type | `Xlink_href]

  type foreignobject =
    [ core
    | conditional_processing
    | graphical_event
    | presentation
    | `Class
    | `Style
    | `ExternalResourcesRequired
    | `Transform
    | `X
    | `Y
    | `Width
    | `Height ]

  type alignment_baseline =
    [ `After_edge
    | `Alphabetic
    | `Auto
    | `Baseline
    | `Before_edge
    | `Central
    | `Hanging
    | `Ideographic
    | `Inherit
    | `Mathematical
    | `Middle
    | `Text_after_edge
    | `Text_before_edge ]

  type dominant_baseline =
    [ `Auto
    | `Use_script
    | `No_change
    | `Reset_size
    | `Ideographic
    | `Alphabetic
    | `Hanging
    | `Mathematical
    | `Central
    | `Middle
    | `Text_after_edge
    | `Text_before_edge
    | `Inherit ]

  type in_value =
    [ `SourceGraphic
    | `SourceAlpha
    | `BackgroundImage
    | `BackgroundAlpha
    | `FillPaint
    | `StrokePaint
    | `Ref of string ]
  [@@reflect.total_variant]

  type offset = [`Number of int | `Percentage of int]

  type big_variant =
    [ `A
    | `Absolute_colorimetric
    | `Align
    | `Always
    | `Atop
    | `Arithmetic
    | `Auto
    | `B
    | `Bever
    | `Blink
    | `Butt
    | `CSS
    | `Darken
    | `Default
    | `Dilate
    | `Disable
    | `Discrete
    | `Duplicate
    | `End
    | `Erode
    | `Exact
    | `FractalNoise
    | `Freeze
    | `HueRotate
    | `G
    | `Gamma
    | `GeometricPrecision
    | `H
    | `Identity
    | `In
    | `Inherit
    | `Initial
    | `Isolated
    | `Lighten
    | `Line_through
    | `Linear
    | `LuminanceToAlpha
    | `Magnify
    | `Matrix
    | `Medial
    | `Middle
    | `Miter
    | `Multiply
    | `Never
    | `New
    | `None
    | `Normal
    | `NoStitch
    | `ObjectBoundingBox
    | `OnLoad
    | `OnRequest
    | `OptimizeLegibility
    | `OptimizeSpeed
    | `Other
    | `Out
    | `Over
    | `Overline
    | `Paced
    | `Pad
    | `Perceptual
    | `Preserve
    | `R
    | `Reflect
    | `Remove
    | `Repeat
    | `Replace
    | `Relative_colorimetric
    | `Rotate
    | `Round
    | `Saturate
    | `Saturation
    | `Scale
    | `Screen
    | `SkewX
    | `SkewY
    | `Spacing
    | `SpacingAndGlyphs
    | `Spline
    | `Square
    | `Start
    | `Stitch
    | `Stretch
    | `StrokeWidth
    | `Sum
    | `Table
    | `Terminal
    | `Translate
    | `Turbulence
    | `Underline
    | `UserSpaceOnUse
    | `V
    | `WhenNotActive
    | `Wrap
    | `XML
    | `Xor ]
end

module type Sig = sig
  open Terminology

  module Model : Model.Sig

  module X := ModelType.Make(Model).At

  module At : module type of X

  open X

  val a_x : (coord, [> `X]) cons

  val a_y : (coord, [> `Y]) cons

  val a_width : (Unit.length, [> `Width]) cons

  val a_height : (Unit.length, [> `Height]) cons

  val a_preserveAspectRatio : (string, [> `PreserveAspectRatio]) cons

  val a_contentScriptType : (string, [> `ContentScriptType]) cons
    [@@ocaml.deprecated "Removed in SVG2"]
  (** @deprecated Removed in SVG2 *)

  val a_contentStyleType : (string, [> `ContentStyleType]) cons
    [@@ocaml.deprecated "Removed in SVG2"]
  (** @deprecated Removed in SVG2 *)

  val a_href : (iri, [> `Xlink_href]) cons

  val a_xlink_href : (iri, [> `Xlink_href]) cons
    [@@ocaml.deprecated "Use a_href"]
  (** @deprecated Use a_href *)

  val a_requiredFeatures : (string list, [> `RequiredFeatures]) cons
    [@@ocaml.deprecated "Removed in SVG2"]
  (** @deprecated Removed in SVG2 *)

  val a_requiredExtensions : (string list, [> `RequiredExtension]) cons

  val a_systemLanguage : (string list, [> `SystemLanguage]) cons

  val a_externalRessourcesRequired :
    (bool, [> `ExternalRessourcesRequired]) cons

  val a_id : (string, [> `Id]) cons

  val a_user_data : string -> (string, [> `User_data]) cons

  val a_xml_base : (iri, [> `Xml_Base]) cons
    [@@ocaml.deprecated "Removed in SVG2"]
  (** @deprecated Removed in SVG2 *)

  val a_xml_lang : (iri, [> `Xml_Lang]) cons

  val a_xml_space : ([`Default | `Preserve], [> `Xml_Space]) cons
    [@@ocaml.deprecated "Use CSS white-space"]
  (** @deprecated Use CSS white-space *)

  val a_type' : (string, [> `Type]) cons

  val a_media : (string list, [> `Media]) cons

  val a_xlink_title : (string, [> `Title]) cons
    [@@ocaml.deprecated "Use a child title element"]
  (** @deprecated Use a child title element *)

  val a_class : (string list, [> `Class]) cons

  val a_style : (string, [> `Style]) cons

  val a_transform : (transforms, [> `Transform]) cons

  val a_viewBox : (fourfloats, [> `ViewBox]) cons

  val a_d : (string, [> `D]) cons

  val a_pathLength : (float, [> `PathLength]) cons

  (* XXX: better language support *)
  val a_rx : (Unit.length, [> `Rx]) cons

  val a_ry : (Unit.length, [> `Ry]) cons

  val a_cx : (Unit.length, [> `Cx]) cons

  val a_cy : (Unit.length, [> `Cy]) cons

  val a_r : (Unit.length, [> `R]) cons

  val a_x1 : (coord, [> `X1]) cons

  val a_y1 : (coord, [> `Y1]) cons

  val a_x2 : (coord, [> `X2]) cons

  val a_y2 : (coord, [> `Y2]) cons

  val a_points : (coords, [> `Points]) cons

  val a_x_list : (lengths, [> `X_list]) cons
    [@@reflect.attribute "x" ["text"; "tspan"; "tref"; "altGlyph"]]

  val a_y_list : (lengths, [> `Y_list]) cons
    [@@reflect.attribute "y" ["text"; "tspan"; "tref"; "altGlyph"]]

  val a_dx : (number, [> `Dx]) cons

  val a_dy : (number, [> `Dy]) cons

  val a_dx_list : (lengths, [> `Dx_list]) cons
    [@@reflect.attribute "dx" ["text"; "tspan"; "tref"; "altGlyph"]]

  val a_dy_list : (lengths, [> `Dy_list]) cons
    [@@reflect.attribute "dy" ["text"; "tspan"; "tref"; "altGlyph"]]

  val a_lengthAdjust :
    ([`Spacing | `SpacingAndGlyphs], [> `LengthAdjust]) cons

  val a_textLength : (Unit.length, [> `TextLength]) cons

  val a_text_anchor :
    ([`Start | `Middle | `End | `Inherit], [> `Text_Anchor]) cons

  val a_text_decoration :
    ( [`None | `Underline | `Overline | `Line_through | `Blink | `Inherit]
    , [> `Text_Decoration] )
    cons

  val a_text_rendering :
    ( [ `Auto
      | `OptimizeSpeed
      | `OptimizeLegibility
      | `GeometricPrecision
      | `Inherit ]
    , [> `Text_Rendering] )
    cons

  val a_rotate : (numbers, [> `Rotate]) cons

  val a_startOffset : (Unit.length, [> `StartOffset]) cons

  val a_method' : ([`Align | `Stretch], [> `Method]) cons

  val a_spacing : ([`Auto | `Exact], [> `Spacing]) cons

  val a_glyphRef : (string, [> `GlyphRef]) cons

  val a_format : (string, [> `Format]) cons

  val a_markerUnits :
    ([`StrokeWidth | `UserSpaceOnUse], [> `MarkerUnits]) cons

  val a_refX : (coord, [> `RefX]) cons

  val a_refY : (coord, [> `RefY]) cons

  val a_markerWidth : (Unit.length, [> `MarkerWidth]) cons

  val a_markerHeight : (Unit.length, [> `MarkerHeight]) cons

  val a_orient : (Unit.angle option, [> `Orient]) cons

  val a_local : (string, [> `Local]) cons

  val a_rendering_intent :
    ( [ `Auto
      | `Perceptual
      | `Relative_colorimetric
      | `Saturation
      | `Absolute_colorimetric ]
    , [> `Rendering_Indent] )
    cons

  val a_gradientUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [`GradientUnits]) cons

  val a_gradientTransform : (transforms, [> `Gradient_Transform]) cons

  val a_spreadMethod : ([`Pad | `Reflect | `Repeat], [> `SpreadMethod]) cons

  val a_fx : (coord, [> `Fx]) cons

  val a_fy : (coord, [> `Fy]) cons

  val a_offset :
    ([`Number of number | `Percentage of percentage], [> `Offset]) cons

  val a_patternUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [> `PatternUnits]) cons

  val a_patternContentUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [> `PatternContentUnits]) cons

  val a_patternTransform : (transforms, [> `PatternTransform]) cons

  val a_clipPathUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [> `ClipPathUnits]) cons

  val a_maskUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [> `MaskUnits]) cons

  val a_maskContentUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [> `MaskContentUnits]) cons

  val a_primitiveUnits :
    ([`UserSpaceOnUse | `ObjectBoundingBox], [> `PrimitiveUnits]) cons

  val a_filterRes : (number_optional_number, [> `FilterResUnits]) cons

  val a_result : (string, [> `Result]) cons

  val a_in' :
    ( [ `SourceGraphic
      | `SourceAlpha
      | `BackgroundImage
      | `BackgroundAlpha
      | `FillPaint
      | `StrokePaint
      | `Ref of string ]
    , [> `In] )
    cons

  val a_in2 :
    ( [ `SourceGraphic
      | `SourceAlpha
      | `BackgroundImage
      | `BackgroundAlpha
      | `FillPaint
      | `StrokePaint
      | `Ref of string ]
    , [> `In2] )
    cons

  val a_azimuth : (float, [> `Azimuth]) cons

  val a_elevation : (float, [> `Elevation]) cons

  val a_pointsAtX : (float, [> `PointsAtX]) cons

  val a_pointsAtY : (float, [> `PointsAtY]) cons

  val a_pointsAtZ : (float, [> `PointsAtZ]) cons

  val a_specularExponent : (float, [> `SpecularExponent]) cons

  val a_specularConstant : (float, [> `SpecularConstant]) cons

  val a_limitingConeAngle : (float, [> `LimitingConeAngle]) cons

  val a_mode :
    ([`Normal | `Multiply | `Screen | `Darken | `Lighten], [> `Mode]) cons

  val a_feColorMatrix_type :
    ( [`Matrix | `Saturate | `HueRotate | `LuminanceToAlpha]
    , [> `Typefecolor] )
    cons

  val a_values : (numbers, [> `Values]) cons

  val a_transfer_type :
    ( [`Identity | `Table | `Discrete | `Linear | `Gamma]
    , [> `Type_transfert] )
    cons

  val a_tableValues : (numbers, [> `TableValues]) cons

  val a_intercept : (number, [> `Intercept]) cons

  val a_amplitude : (number, [> `Amplitude]) cons

  val a_exponent : (number, [> `Exponent]) cons

  val a_transfer_offset : (number, [> `Offset_transfer]) cons
    [@@reflect.attribute
      "offset" ["feFuncR"; "feFuncG"; "feFuncB"; "feFuncA"]]

  val a_feComposite_operator :
    ( [`Over | `In | `Out | `Atop | `Xor | `Arithmetic]
    , [> `OperatorComposite] )
    cons

  val a_k1 : (number, [> `K1]) cons

  val a_k2 : (number, [> `K2]) cons

  val a_k3 : (number, [> `K3]) cons

  val a_k4 : (number, [> `K4]) cons

  val a_order : (number_optional_number, [> `Order]) cons

  val a_kernelMatrix : (numbers, [> `KernelMatrix]) cons

  val a_divisor : (number, [> `Divisor]) cons

  val a_bias : (number, [> `Bias]) cons

  val a_kernelUnitLength :
    (number_optional_number, [> `KernelUnitLength]) cons

  val a_targetX : (int, [> `TargetX]) cons

  val a_targetY : (int, [> `TargetY]) cons

  val a_edgeMode : ([`Duplicate | `Wrap | `None], [> `TargetY]) cons

  val a_preserveAlpha : (bool, [> `TargetY]) cons

  val a_surfaceScale : (number, [> `SurfaceScale]) cons

  val a_diffuseConstant : (number, [> `DiffuseConstant]) cons

  val a_scale : (number, [> `Scale]) cons

  val a_xChannelSelector : ([`R | `G | `B | `A], [> `XChannelSelector]) cons

  val a_yChannelSelector : ([`R | `G | `B | `A], [> `YChannelSelector]) cons

  val a_stdDeviation : (number_optional_number, [> `StdDeviation]) cons

  val a_feMorphology_operator :
    ([`Erode | `Dilate], [> `OperatorMorphology]) cons
    [@@reflect.attribute "operator" ["feMorphology"]]

  val a_radius : (number_optional_number, [> `Radius]) cons

  val a_baseFrenquency : (number_optional_number, [> `BaseFrequency]) cons

  val a_numOctaves : (int, [> `NumOctaves]) cons

  val a_seed : (number, [> `Seed]) cons

  val a_stitchTiles : ([`Stitch | `NoStitch], [> `StitchTiles]) cons

  val a_feTurbulence_type :
    ([`FractalNoise | `Turbulence], [> `TypeStitch]) cons
    [@@reflect.attribute "type" ["feTurbulence"]]

  val a_target : (string, [> `Xlink_target]) cons

  val a_attributeName : (string, [> `AttributeName]) cons

  val a_attributeType : ([`CSS | `XML | `Auto], [> `AttributeType]) cons

  val a_begin' : (string, [> `Begin]) cons

  val a_dur : (string, [> `Dur]) cons

  val a_min : (string, [> `Min]) cons

  val a_max : (string, [> `Max]) cons

  val a_restart : ([`Always | `WhenNotActive | `Never], [> `Restart]) cons

  val a_repeatCount : (string, [> `RepeatCount]) cons

  val a_repeatDur : (string, [> `RepeatDur]) cons

  val a_fill : (paint, [> `Fill]) cons

  val a_animation_fill : ([`Freeze | `Remove], [> `Fill_Animation]) cons
    [@@reflect.attribute "fill" ["animation"]]

  val a_fill_rule : (fill_rule, [> `Fill_rule]) cons

  val a_calcMode :
    ([`Discrete | `Linear | `Paced | `Spline], [> `CalcMode]) cons

  val a_animation_values : (string list, [> `Valuesanim]) cons
    [@@reflect.attribute "values" ["animation"]]

  val a_keyTimes : (string list, [> `KeyTimes]) cons

  val a_keySplines : (string list, [> `KeySplines]) cons

  val a_from : (string, [> `From]) cons

  val a_to' : (string, [> `To]) cons

  val a_by : (string, [> `By]) cons

  val a_zoomAndPan : ([`Disable | `Magnify], [> `ZoomAndSpan]) cons

  val a_additive : ([`Replace | `Sum], [> `Additive]) cons

  val a_accumulate : ([`None | `Sum], [> `Accumulate]) cons

  val a_keyPoints : (numbers_semicolon, [> `KeyPoints]) cons

  val a_path : (string, [> `Path]) cons

  val a_animateTransform_type :
    ( [`Translate | `Scale | `Rotate | `SkewX | `SkewY]
    , [`Typeanimatetransform] )
    cons

  val a_horiz_origin_x : (number, [> `HorizOriginX]) cons

  val a_horiz_origin_y : (number, [> `HorizOriginY]) cons

  val a_horiz_adv_x : (number, [> `HorizAdvX]) cons

  val a_vert_origin_x : (number, [> `VertOriginX]) cons

  val a_vert_origin_y : (number, [> `VertOriginY]) cons

  val a_vert_adv_y : (number, [> `VertAdvY]) cons

  val a_unicode : (string, [> `Unicode]) cons

  val a_glyph_name : (string, [> `glyphname]) cons

  val a_orientation : ([`H | `V], [> `Orientation]) cons

  val a_arabic_form :
    ([`Initial | `Medial | `Terminal | `Isolated], [> `Arabicform]) cons

  val a_lang : (string, [> `Lang]) cons

  val a_u1 : (string, [> `U1]) cons

  val a_u2 : (string, [> `U2]) cons

  val a_g1 : (string, [> `G1]) cons

  val a_g2 : (string, [> `G2]) cons

  val a_k : (string, [> `K]) cons

  val a_font_family : (string, [> `Font_Family]) cons

  val a_font_style : (string, [> `Font_Style]) cons

  val a_font_variant : (string, [> `Font_Variant]) cons

  val a_font_weight : (string, [> `Font_Weight]) cons

  val a_font_stretch : (string, [> `Font_Stretch]) cons

  val a_font_size : (string, [> `Font_Size]) cons

  val a_unicode_range : (string, [> `UnicodeRange]) cons

  val a_units_per_em : (string, [> `UnitsPerEm]) cons

  val a_stemv : (number, [> `Stemv]) cons

  val a_stemh : (number, [> `Stemh]) cons

  val a_slope : (number, [> `Slope]) cons

  val a_cap_height : (number, [> `CapHeight]) cons

  val a_x_height : (number, [> `XHeight]) cons

  val a_accent_height : (number, [> `AccentHeight]) cons

  val a_ascent : (number, [> `Ascent]) cons

  val a_widths : (string, [> `Widths]) cons

  val a_bbox : (string, [> `Bbox]) cons

  val a_ideographic : (number, [> `Ideographic]) cons

  val a_alphabetic : (number, [> `Alphabetic]) cons

  val a_mathematical : (number, [> `Mathematical]) cons

  val a_hanging : (number, [> `Hanging]) cons

  val a_videographic : (number, [> `VIdeographic]) cons

  val a_v_alphabetic : (number, [> `VAlphabetic]) cons

  val a_v_mathematical : (number, [> `VMathematical]) cons

  val a_v_hanging : (number, [> `VHanging]) cons

  val a_underline_position : (number, [> `UnderlinePosition]) cons

  val a_underline_thickness : (number, [> `UnderlineThickness]) cons

  val a_strikethrough_position : (number, [> `StrikethroughPosition]) cons

  val a_strikethrough_thickness : (number, [> `StrikethroughThickness]) cons

  val a_overline_position : (number, [> `OverlinePosition]) cons

  val a_overline_thickness : (number, [> `OverlineThickness]) cons

  val a_string : (string, [> `String]) cons

  val a_name : (string, [> `Name]) cons

  val a_alignment_baseline :
    ( [ `Auto
      | `Baseline
      | `Before_edge
      | `Text_before_edge
      | `Middle
      | `Central
      | `After_edge
      | `Text_after_edge
      | `Ideographic
      | `Alphabetic
      | `Hanging
      | `Mathematical
      | `Inherit ]
    , [> `Alignment_Baseline] )
    cons

  val a_dominant_baseline :
    ( [ `Auto
      | `Use_script
      | `No_change
      | `Reset_size
      | `Ideographic
      | `Alphabetic
      | `Hanging
      | `Mathematical
      | `Central
      | `Middle
      | `Text_after_edge
      | `Text_before_edge
      | `Inherit ]
    , [> `Dominant_Baseline] )
    cons

  val a_stop_color : (color, [> `Stop_Color]) cons

  val a_stop_opacity : (number, [> `Stop_Opacity]) cons

  val a_stroke : (paint, [> `Stroke]) cons

  val a_stroke_width : (Unit.length, [> `Stroke_Width]) cons

  val a_stroke_linecap :
    ([`Butt | `Round | `Square], [> `Stroke_Linecap]) cons

  val a_stroke_linejoin :
    ([`Miter | `Round | `Bever], [> `Stroke_Linejoin]) cons

  val a_stroke_miterlimit : (float, [> `Stroke_Miterlimit]) cons

  val a_stroke_dasharray : (Unit.length list, [> `Stroke_Dasharray]) cons

  val a_stroke_dashoffset : (Unit.length, [> `Stroke_Dashoffset]) cons

  val a_stroke_opacity : (float, [> `Stroke_Opacity]) cons

  (** {2 Events}

      {3 Javascript events} *)

  val a_onabort : (Model.Ev.base, [> `OnAbort]) cons_handler

  val a_onactivate : (Model.Ev.base, [> `OnActivate]) cons_handler

  val a_onbegin : (Model.Ev.base, [> `OnBegin]) cons_handler

  val a_onend : (Model.Ev.base, [> `OnEnd]) cons_handler

  val a_onerror : (Model.Ev.base, [> `OnError]) cons_handler

  val a_onfocusin : (Model.Ev.base, [> `OnFocusIn]) cons_handler

  val a_onfocusout : (Model.Ev.base, [> `OnFocusOut]) cons_handler

  val a_onload : (Model.Ev.base, [> `OnLoad]) cons_handler
    [@@ocaml.deprecated "Removed in SVG2"]
  (** @deprecated Removed in SVG2 *)

  val a_onrepeat : (Model.Ev.base, [> `OnRepeat]) cons_handler

  val a_onresize : (Model.Ev.base, [> `OnResize]) cons_handler

  val a_onscroll : (Model.Ev.base, [> `OnScroll]) cons_handler

  val a_onunload : (Model.Ev.base, [> `OnUnload]) cons_handler

  val a_onzoom : (Model.Ev.base, [> `OnZoom]) cons_handler

  (** {3 Javascript mouse events} *)

  val a_onclick : (Model.Ev.mouse, [> `OnClick]) cons_handler

  val a_onmousedown : (Model.Ev.mouse, [> `OnMouseDown]) cons_handler

  val a_onmouseup : (Model.Ev.mouse, [> `OnMouseUp]) cons_handler

  val a_onmouseover : (Model.Ev.mouse, [> `OnMouseOver]) cons_handler

  val a_onmouseout : (Model.Ev.mouse, [> `OnMouseOut]) cons_handler

  val a_onmousemove : (Model.Ev.mouse, [> `OnMouseMove]) cons_handler

  val a_ontouchstart : (Model.Ev.touch, [> `OnTouchStart]) cons_handler

  (** {3 Javascript touch events} *)

  val a_ontouchend : (Model.Ev.touch, [> `OnTouchEnd]) cons_handler

  val a_ontouchmove : (Model.Ev.touch, [> `OnTouchMove]) cons_handler

  val a_ontouchcancel : (Model.Ev.touch, [> `OnTouchCancel]) cons_handler

  module Co : sig
    module X := ModelType.Make(Model).Co

    include module type of X

    val svg : Attrib.svg star

    val g : Attrib.g star

    val defs : Attrib.defs star

    val desc : Attrib.desc unary

    val title : Attrib.title unary

    val symbol : Attrib.symbol star

    val use : Attrib.use star

    val image : Attrib.image star

    val switch : Attrib.switch star

    val style : Attrib.style unary

    val path : Attrib.path star

    val rect : Attrib.rect star

    val circle : Attrib.circle star

    val ellipse : Attrib.ellipse star

    val line : Attrib.line star

    val polyline : Attrib.polyline star

    val polygon : Attrib.polygon star

    val text : Attrib.text star

    val tspan : Attrib.tspan star

    val tref : Attrib.tref star
      [@@ocaml.deprecated "Removed in SVG2"]
    (** @deprecated Removed in SVG2 *)

    val textPath : Attrib.textpath star

    val marker : Attrib.marker star

    val linearGradient : Attrib.lineargradient star

    val radialGradient : Attrib.radialgradient star

    val stop : Attrib.stop star

    val pattern : Attrib.pattern star

    val clipPath : Attrib.clippath star

    val filter : Attrib.filter star

    val feDistantLight : Attrib.fedistantlight star

    val fePointLight : Attrib.fepointlight star

    val feSpotLight : Attrib.fespotlight star

    val feBlend : Attrib.feblend star

    val feColorMatrix : Attrib.fecolormatrix star

    val feComponentTransfer : Attrib.fecomponenttransfer star

    val feFuncA : Attrib.fefunca star

    val feFuncG : Attrib.fefuncg star

    val feFuncB : Attrib.fefuncb star

    val feFuncR : Attrib.fefuncr star

    val feComposite : Attrib.fecomposite star

    val feConvolveMatrix : Attrib.feconvolvematrix star

    val feDiffuseLighting : Attrib.fediffuselighting star

    val feDisplacementMap : Attrib.fedisplacementmap star

    val feFlood : Attrib.feflood star

    val feGaussianBlur : Attrib.fegaussianblur star

    val feImage : Attrib.feimage star

    val feMerge : Attrib.femerge star

    val feMorphology : Attrib.femorphology star

    val feOffset : Attrib.feoffset star

    val feSpecularLighting : Attrib.fespecularlighting star

    val feTile : Attrib.fetile star

    val feTurbulence : Attrib.feturbulence star

    val cursor : Attrib.cursor star

    val a : Attrib.a star

    val view : Attrib.view star

    val script : Attrib.script unary

    val animation : Attrib.animation star

    val set : Attrib.set star

    val animateMotion : Attrib.animatemotion star

    val mpath : Attrib.mpath star

    val animateColor : Attrib.animatecolor star

    val animateTransform : Attrib.animatetransform star

    val metadata : Attrib.metadata star

    val foreignObject : Attrib.foreignobject star
  end
end
