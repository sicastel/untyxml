module Terminology : sig
  type datetime = string

  type number_or_datetime = [`Number of int | `Datetime of datetime]

  type referrerpolicy =
    [ `Empty
    | `No_referrer
    | `No_referrer_when_downgrade
    | `Origin
    | `Origin_when_cross_origin
    | `Same_origin
    | `Strict_origin
    | `Strict_origin_when_cross_origin
    | `Unsafe_url ]

  type sandbox_token =
    [ `Allow_forms
    | `Allow_pointer_lock
    | `Allow_popups
    | `Allow_top_navigation
    | `Allow_same_origin
    | `Allow_script ]

  type 'uri image_candidate =
    [`Url of 'uri | `Url_width of 'uri * int | `Url_pixel of 'uri * float]

  type autocomplete_option = [`On | `Off | `Tokens of string list]

  type mediadesc_token =
    [ `All
    | `Aural
    | `Braille
    | `Embossed
    | `Handheld
    | `Print
    | `Projection
    | `Screen
    | `Speech
    | `Tty
    | `Tv
    | `Raw_mediadesc of string ]

  type mediadesc = mediadesc_token list

  type shape = [`Rect | `Circle | `Poly | `Default]

  type script_type = [`Javascript | `Module | `Mime of string]

  type linktype =
    [ `Alternate
    | `Archives
    | `Author
    | `Bookmark
    | `Canonical
    | `External
    | `First
    | `Help
    | `Icon
    | `Index
    | `Last
    | `License
    | `Next
    | `Nofollow
    | `Noreferrer
    | `Noopener
    | `Pingback
    | `Prefetch
    | `Prev
    | `Search
    | `Stylesheet
    | `Sidebar
    | `Tag
    | `Up
    | `Other of string ]
end

(** Types for the attributes of elements *)
module Attrib : sig
  type i18n = [`XML_lang | `Lang]

  type core =
    [ `Accesskey
    | `Class
    | `Contenteditable
    | `Contextmenu
    | `Dir
    | `Draggable
    | `Hidden
    | `Id
    | i18n
    | `Spellcheck
    | `Style_Attr
    | `Tabindex
    | `Translate
    | `Title
    | `User_data
    | `XMLns ]

  (** {2 Events} *)

  (** Javascript events *)
  type events =
    [ `OnAbort
    | `OnBlur
    | `OnCanPlay
    | `OnCanPlayThrough
    | `OnChange
    | `OnClick
    | `OnClose
    | `OnContextMenu
    | `OnDblClick
    | `OnDrag
    | `OnDragEnd
    | `OnDragEnter
    | `OnDragLeave
    | `OnDragOver
    | `OnDragStart
    | `OnDrop
    | `OnDurationChange
    | `OnEmptied
    | `OnEnded
    | `OnError
    | `OnFocus
    | `OnFormChange
    | `OnFormInput
    | `OnInput
    | `OnInvalid
    | `OnMouseDown
    | `OnMouseUp
    | `OnMouseOver
    | `OnMouseMove
    | `OnMouseOut
    | `OnMouseWheel
    | `OnPause
    | `OnPlay
    | `OnPlaying
    | `OnProgress
    | `OnRateChange
    | `OnReadyStateChange
    | `OnScroll
    | `OnSeeked
    | `OnSeeking
    | `OnSelect
    | `OnShow
    | `OnStalled
    | `OnSubmit
    | `OnSuspend
    | `OnTimeUpdate
    | `OnTouchStart
    | `OnTouchEnd
    | `OnTouchMove
    | `OnTouchCancel
    | `OnVolumeChange
    | `OnWaiting
    | `OnKeyPress
    | `OnKeyDown
    | `OnKeyUp
    | `OnLoad
    | `OnLoadedData
    | `OnLoadedMetaData
    | `OnLoadStart ]

  (** {2 ARIA} *)

  type aria = [`Role | `Aria]

  (** Common attributes *)
  type common = [core | i18n | events | aria]

  type pcdata = [`PCDATA]

  type txt = [`PCDATA]

  type notag

  type no_attribute_allowed

  type noattrib = [`No_attribute_allowed of no_attribute_allowed]

  type html = [common | `Manifest]

  type head = [ | common]

  type body =
    [ common
    | `OnAfterPrint
    | `OnBeforePrint
    | `OneBeforeUnload
    | `OnHashChange
    | `OnMessage
    | `OnOffLine
    | `OnOnLine
    | `OnPageHide
    | `OnPageShow
    | `OnPopState
    | `OnRedo
    | `OnResize
    | `OnStorage
    | `OnUndo
    | `OnUnload ]

  type svg = Svg.Attrib.svg
  (* NAME: base, KIND: nullary, TYPE: [= common | `Href | `Target], [= `Base
     ], ARG: notag, ATTRIB: OUT: [= `Base ] *)

  type base = [common | `Href | `Target]

  type title = noattrib

  (* NAME: footer, KIND: star, TYPE: [= common ], [=
     flow5_without_header_footer ], [=`Footer], ARG: [=
     flow5_without_header_footer ], ATTRIB: OUT: [=`Footer] *)

  type footer = [ | common]

  (* NAME: header, KIND: star, TYPE: [= common ], [=
     flow5_without_header_footer ], [=`Header], ARG: [=
     flow5_without_header_footer ], ATTRIB: OUT: [=`Header] *)
  type header = [ | common]

  (* NAME: section, KIND: star, TYPE: [= common ], [= flow5 ], [=`Section],
     ARG: [= flow5 ], ATTRIB: OUT: [=`Section] *)
  type section = [ | common]

  (* NAME: nav, KIND: star, TYPE: [= common ], [= flow5 ], [=`Nav], ARG: [=
     flow5 ], ATTRIB: OUT: [=`Nav] *)

  type nav = [ | common]

  (* NAME: h, KIND: star, TYPE: [= common ], [= phrasing ], [=`H1], ARG: [=
     phrasing ], ATTRIB: OUT: [=`H1] *)

  type h1 = [ | common]

  (* NAME: h, KIND: star, TYPE: [= common ], [= phrasing ], [=`H2], ARG: [=
     phrasing ], ATTRIB: OUT: [=`H2] *)

  type h2 = [ | common]

  (* NAME: h, KIND: star, TYPE: [= common ], [= phrasing ], [=`H3], ARG: [=
     phrasing ], ATTRIB: OUT: [=`H3] *)

  type h3 = [ | common]

  (* NAME: h, KIND: star, TYPE: [= common ], [= phrasing ], [=`H4], ARG: [=
     phrasing ], ATTRIB: OUT: [=`H4] *)

  type h4 = [ | common]

  (* NAME: h, KIND: star, TYPE: [= common ], [= phrasing ], [=`H5], ARG: [=
     phrasing ], ATTRIB: OUT: [=`H5] *)

  type h5 = [ | common]

  (* NAME: h, KIND: star, TYPE: [= common ], [= phrasing ], [=`H6], ARG: [=
     phrasing ], ATTRIB: OUT: [=`H6] *)

  type h6 = [ | common]

  (* NAME: hgroup, KIND: plus, TYPE: [= common ], [= `H1 | `H2 | `H3 | `H4 |
     `H5 | `H6 ], [=`Hgroup], ARG: [= `H1 | `H2 | `H3 | `H4 | `H5 | `H6 ],
     ATTRIB: OUT: [=`Hgroup] *)

  type hgroup = [ | common]

  (* NAME: address, KIND: star, TYPE: [= common ], [=
     flow5_without_sectioning_heading_header_footer_address ], [=`Address],
     ARG: [= flow5_without_sectioning_heading_header_footer_address ],
     ATTRIB: OUT: [=`Address] *)

  type address = [ | common]

  (* NAME: article, KIND: star, TYPE: [= common ], [= flow5 ], [=`Article],
     ARG: [= flow5 ], ATTRIB: OUT: [=`Article] *)

  type article = [ | common]

  (* NAME: aside, KIND: star, TYPE: [= common ], [= flow5 ], [=`Aside], ARG:
     [= flow5 ], ATTRIB: OUT: [=`Aside] *)

  type aside = [ | common]

  (* NAME: main, KIND: star, TYPE: [= common ], [= flow5 ], [=`Main], ARG: [=
     flow5 ], ATTRIB: OUT: [=`Main] *)

  type main = [ | common]

  (* NAME: p, KIND: star, TYPE: [= common ], [=phrasing ], [=`P], ARG:
     [=phrasing ], ATTRIB: OUT: [=`P] *)

  type p = [ | common]

  (* NAME: pre, KIND: star, TYPE: [= common ],[= phrasing ], [=`Pre], ARG: [=
     phrasing ], ATTRIB: OUT: [=`Pre] *)

  type pre = [ | common]

  (* NAME: blockquote, KIND: star, TYPE: [= common | `Cite ],[= flow5 ],
     [=`Blockquote], ARG: [= flow5 ], ATTRIB: OUT: [=`Blockquote] *)

  type blockquote = [common | `Cite]

  (* NAME: dialog, KIND: star, TYPE: [= common | `Open ], [= flow5 ],
     [=`Dialog], ARG: [= flow5 ], ATTRIB: OUT: [=`Dialog] *)

  type dialog = [common | `Open]

  (* NAME: div, KIND: star, TYPE: [= common ], [= flow5 ], [=`Div], ARG: [=
     flow5 ], ATTRIB: OUT: [=`Div] *)

  type div = [ | common]

  (* NAME: ol, KIND: star, TYPE: [= common | `Reserved |`Start ], [= `Li of
     [= common | `Int_Value ]], [=`Ol], ARG: [= `Li of [= common | `Int_Value
     ]], ATTRIB: OUT: [=`Ol] *)

  type ol = [common | `Reversed | `Start]

  (* NAME: li, KIND: star, TYPE: [= common | `Int_Value] as 'a, [=flow5 ],
     [=`Li of 'a], ARG: [=flow5 ], ATTRIB: OUT: [=`Li of 'a] *)

  type li = [common | `Int_Value]

  (* NAME: ul, KIND: star, TYPE: [= common ], [= `Li of [= common] ], [=`Ul],
     ARG: [= `Li of [= common] ], ATTRIB: OUT: [=`Ul] *)

  type ul = [ | common]

  (* NAME: dd, KIND: star, TYPE: [= common ], [= flow5 ], [=`Dd], ARG: [=
     flow5 ], ATTRIB: OUT: [=`Dd] *)

  type dd = [ | common]

  (* NAME: dt, KIND: star, TYPE: [= common ], [= phrasing], [=`Dt], ARG: [=
     phrasing], ATTRIB: OUT: [=`Dt] *)

  type dt = [ | common]

  type dl = [ | common]

  (* NAME: figcaption, KIND: star, TYPE: [= common ], [= flow5],
     [=`Figcaption], ARG: [= flow5], ATTRIB: OUT: [=`Figcaption] *)

  type figcaption = [ | common]

  (* figure *)

  type figure = [ | common]

  (* Rp, Rt and ruby *)

  type rp = [ | common]

  type rt = [ | common]

  type ruby = [ | common]

  (* NAME: hr, KIND: nullary, TYPE: [= common ], [=`Hr], ARG: notag, ATTRIB:
     OUT: [=`Hr] *)

  type hr = [ | common]

  (* NAME: b, KIND: star, TYPE: [= common ], [= phrasing ], [=`B], ARG: [=
     phrasing ], ATTRIB: OUT: [=`B] *)

  type b = [ | common]

  (* NAME: i, KIND: star, TYPE: [= common ], [= phrasing ], [=`I], ARG: [=
     phrasing ], ATTRIB: OUT: [=`I] *)

  type i = [ | common]

  (* NAME: u, KIND: star, TYPE: [= common ], [= phrasing ], [=`U], ARG: [=
     phrasing ], ATTRIB: OUT: [=`U] *)

  type u = [ | common]

  (* NAME: small, KIND: star, TYPE: [= common ], [= phrasing ], [=`Small],
     ARG: [= phrasing ], ATTRIB: OUT: [=`Small] *)

  type small = [ | common]

  (* NAME: sub, KIND: star, TYPE: [= common ], [= phrasing ], [=`Sub], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Sub] *)

  type sub = [ | common]

  (* NAME: sup, KIND: star, TYPE: [= common ], [= phrasing ], [=`Sup], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Sup] *)

  type sup = [ | common]

  (* NAME: mark, KIND: star, TYPE: [= common ],[= phrasing ],[= `Mark ], ARG:
     [= phrasing ], ATTRIB: OUT: [= `Mark ] *)

  type mark = [ | common]

  (* NAME: wbr, KIND: nullary, TYPE: [= common ],[= `Wbr ], ARG: notag,
     ATTRIB: OUT: [= `Wbr ] *)

  type wbr = [ | common]

  (* NAME: bdo, KIND: star, TYPE: [= common ],[= phrasing ],[= `Bdo ], ARG:
     [= phrasing ], ATTRIB: OUT: [= `Bdo ] *)

  type bdo = [ | common]

  (* NAME: abbr, KIND: star, TYPE: [= common ], [=phrasing ], [=`Abbr], ARG:
     [=phrasing ], ATTRIB: OUT: [=`Abbr] *)

  type abbr = [ | common]

  (* NAME: br, KIND: nullary, TYPE: [= common ], [=`Br], ARG: notag, ATTRIB:
     OUT: [=`Br] *)

  type br = [ | common]

  (* NAME: cite, KIND: star, TYPE: [= common ], [= phrasing ], [=`Cite], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Cite] *)

  type cite = [ | common]

  (* NAME: code, KIND: star, TYPE: [= common ], [= phrasing ], [=`Code], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Code] *)

  type code = [ | common]

  (* NAME: dfn, KIND: star, TYPE: [= common ], [= phrasing_without_dfn ],
     [=`Dfn], ARG: [= phrasing_without_dfn ], ATTRIB: OUT: [=`Dfn] *)

  type dfn = [ | common]

  (* NAME: em, KIND: star, TYPE: [= common ], [= phrasing ], [=`Em], ARG: [=
     phrasing ], ATTRIB: OUT: [=`Em] *)

  type em = [ | common]

  (* NAME: kbd, KIND: star, TYPE: [= common ], [= phrasing ], [=`Kbd], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Kbd] *)

  type kbd = [ | common]

  (* NAME: q, KIND: star, TYPE: [= common | `Cite ], [= phrasing ], [=`Q],
     ARG: [= phrasing ], ATTRIB: OUT: [=`Q] *)

  type q = [common | `Cite]

  (* NAME: samp, KIND: star, TYPE: [= common ], [= phrasing ], [=`Samp], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Samp] *)

  type samp = [ | common]

  (* NAME: span, KIND: star, TYPE: [= common ], [= phrasing ], [=`Span], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Span] *)

  type span = [ | common]

  (* NAME: strong, KIND: star, TYPE: [= common ], [= phrasing ], [=`Strong],
     ARG: [= phrasing ], ATTRIB: OUT: [=`Strong] *)

  type strong = [ | common]

  (* NAME: time, KIND: star, TYPE: [= common |`Datetime |`Pubdate], [=
     phrasing_without_time ], [=`Time], ARG: [= phrasing_without_time ],
     ATTRIB: OUT: [=`Time] *)

  type time = [common | `Datetime | `Pubdate]

  (* NAME: var, KIND: star, TYPE: [= common ], [= phrasing ], [=`Var], ARG:
     [= phrasing ], ATTRIB: OUT: [=`Var] *)

  type var = [ | common]

  (* NAME: a, KIND: star, TYPE: [= common | `Href | `Hreflang | `Media | `Rel
     | `Target | `Mime_type ], 'a, [= `A of 'a ], ARG: 'a, ATTRIB: OUT: [= `A
     of 'a ] *)

  type a =
    [ common
    | `Href
    | `Hreflang
    | `Media
    | `Rel
    | `Target
    | `Mime_type
    | `Download ]

  (* NAME: del, KIND: star, TYPE: [= common | `Cite | `Datetime ], 'a,[=`Del
     of 'a], ARG: 'a, ATTRIB: OUT: [=`Del of 'a] *)

  type del = [common | `Cite | `Datetime]

  (* NAME: ins, KIND: star, TYPE: [= common | `Cite | `Datetime ],'a ,[=`Ins
     of 'a], ARG: 'a , ATTRIB: OUT: [=`Ins of 'a] *)

  type ins = [common | `Cite | `Datetime]

  (* NAME: iframe, KIND: ndbox, TYPE: *| `Srcdoc*, ARG: , ATTRIB: OUT: *)

  type iframe =
    [ common
    | `Allowfullscreen
    | `Allowpaymentrequest
    | `Src
    | (*| `Srcdoc*)
      `Name
    | `Sandbox
    | `Seamless
    | `Width
    | `Height
    | `Referrerpolicy ]

  type object_ =
    [common | `Data | `Form | `Mime_type | `Height | `Width | `Name | `Usemap]

  (* NAME: param, KIND: nullary, TYPE: [= common | `Name | `Text_Value ],[=
     `Param ], ARG: notag, ATTRIB: OUT: [= `Param ] *)

  type param = [common | `Name | `Text_Value]

  (* NAME: embed, KIND: nullary, TYPE: [= common | `Src | `Height |
     `Mime_type | `Width], [=`Embed], ARG: notag, ATTRIB: OUT: [=`Embed] *)

  type embed = [common | `Src | `Height | `Mime_type | `Width]

  type img = [common | `Height | `Ismap | `Width | `Srcset | `Img_sizes]

  (* Attributes used by audio and video. *)
  type media =
    [ `Crossorigin
    | `Preload
    | `Autoplay
    | `Mediagroup
    | `Loop
    | `Muted
    | `Controls ]

  type audio = [common | media]

  type video = [common | media | `Poster | `Width | `Height]

  (* NAME: canvas, KIND: star, TYPE: [= common |`Width |`Height],'a,
     [=`Canvas of 'a], ARG: 'a, ATTRIB: OUT: [=`Canvas of 'a] *)

  type canvas = [common | `Width | `Height]

  (* NAME: source, KIND: nullary, TYPE: [= common |`Src |`Mime_type |`Media
     ], [=`Source], ARG: notag, ATTRIB: OUT: [=`Source] *)

  type source = [common | `Src | `Srcset | `Mime_type | `Media]

  (* NAME: area, KIND: nullary, TYPE: [= common | `Alt | `Coords | `Shape|
     `Target | `Rel | `Media| `Hreflang | `Mime_type],[=`Area], ARG: notag,
     ATTRIB: OUT: [=`Area] *)

  type area =
    [ common
    | `Alt
    | `Coords
    | `Shape
    | `Target
    | `Rel
    | `Media
    | `Hreflang
    | `Mime_type
    | `Download ]

  (* NAME: map, KIND: plus, TYPE: [=common | `Name ],'a, [=`Map of 'a], ARG:
     'a, ATTRIB: OUT: [=`Map of 'a] *)

  type map = [common | `Name]

  (* NAME: caption, KIND: star, TYPE: [= common ], [= flow5_without_table],
     [=`Caption], ARG: [= flow5_without_table], ATTRIB: OUT: [=`Caption] *)

  type caption = [ | common]

  (* NAME: table, KIND: plus, TYPE: [= common | `Summary ], [= `Tr ],
     [=`Table], ARG: [= `Tr ], ATTRIB: OUT: [=`Table] *)

  type table = [common | `Summary]

  (* NAME: tablex, KIND: star, TYPE: [= common | `Summary ], [= `Tbody ],
     [=`Table], ARG: [= `Tbody ], ATTRIB: OUT: [=`Table] *)

  type tablex = [common | `Summary]

  (* NAME: colgroup, KIND: star, TYPE: [= common | `Span ],[= `Col ],
     [=`Colgroup], ARG: [= `Col ], ATTRIB: OUT: [=`Colgroup] *)

  type colgroup = [common | `Span]

  (* NAME: col, KIND: nullary, TYPE: [= common | `Span], [=`Col], ARG: notag,
     ATTRIB: OUT: [=`Col] *)

  type col = [common | `Span]

  (* NAME: thead, KIND: star, TYPE: [= common],[= `Tr ], [=`Thead], ARG: [=
     `Tr ], ATTRIB: OUT: [=`Thead] *)

  type thead = [ | common]

  (* NAME: tbody, KIND: star, TYPE: [= common],[= `Tr ], [=`Tbody], ARG: [=
     `Tr ], ATTRIB: OUT: [=`Tbody] *)

  type tbody = [ | common]

  (* NAME: tfoot, KIND: star, TYPE: [= common],[= `Tr ], [=`Tfoot], ARG: [=
     `Tr ], ATTRIB: OUT: [=`Tfoot] *)

  type tfoot = [ | common]

  (* NAME: td, KIND: star, TYPE: [= common | `Colspan | `Headers | `Rowspan
     ], [= flow5 ], [=`Td], ARG: [= flow5 ], ATTRIB: OUT: [=`Td] *)

  type td = [common | `Colspan | `Headers | `Rowspan]

  (* NAME: th, KIND: star, TYPE: [= common | `Colspan | `Headers | `Rowspan |
     `Scope], [= flow5], [=`Th], ARG: [= flow5], ATTRIB: OUT: [=`Th] *)

  type th = [common | `Colspan | `Headers | `Rowspan | `Scope]

  (* NAME: tr, KIND: star, TYPE: [= common ],[= `Td | `Th ], [=`Tr], ARG: [=
     `Td | `Th ], ATTRIB: OUT: [=`Tr] *)

  type tr = [ | common]

  (* NAME: form, KIND: plus, TYPE: [= common |`Accept_charset | `Action |
     `Enctype | `Method | `Name | `Target | `Autocomplete | `Novalidate ], [=
     flow5_without_form ], [=`Form], ARG: [= flow5_without_form ], ATTRIB:
     OUT: [=`Form] *)

  type form =
    [ common
    | `Accept_charset
    | `Action
    | `Enctype
    | `Method
    | `Name
    | `Target
    | `Autocomplete
    | `Novalidate ]

  (* NAME: fieldset, KIND: star, TYPE: [= common | `Disabled | `Form |
     `Name], [= flow5 ], [=`Fieldset], ARG: [= flow5 ], ATTRIB: OUT:
     [=`Fieldset] *)

  type fieldset = [common | `Disabled | `Form | `Name]

  (* NAME: legend, KIND: star, TYPE: [= common ],[= phrasing], [=`Legend],
     ARG: [= phrasing], ATTRIB: OUT: [=`Legend] *)

  type legend = [ | common]

  (* NAME: label, KIND: star, TYPE: [= common | `Label_for | `Form ],[=
     phrasing_without_label], [=`Label], ARG: [= phrasing_without_label],
     ATTRIB: OUT: [=`Label] *)

  type label = [common | `Label_for | `Form]

  (* NAME: input, KIND: nullary, TYPE: [= input_attr ], [=`Input], ARG:
     notag, ATTRIB: OUT: [=`Input] *)

  type input =
    [ common
    | `Accept
    | `Alt
    | `Autocomplete
    | `Autofocus
    | `Checked
    | `Disabled
    | `Form
    | `Formation
    | `Formenctype
    | `Method
    | `Formnovalidate
    | `Formtarget
    | `Height
    | `List
    | `Input_Max
    | `Maxlength
    | `Minlength
    | `Input_Min
    | `Multiple
    | `Name
    | `Pattern
    | `Placeholder
    | `ReadOnly
    | `Required
    | `Size
    | `Src
    | `Step
    | `Input_Type
    | `Value
    | `Width
    | `Inputmode ]

  type textarea =
    [ common
    | `Autofocus
    | `Disabled
    | `Form
    | `Maxlength
    | `Minlength
    | `Name
    | `Placeholder
    | `ReadOnly
    | `Required
    | `Wrap
    | `Rows
    | `Cols ]

  (* NAME: button, KIND: star, TYPE: [= button_attr ], [=
     phrasing_without_interactive ], [=`Button], ARG: [=
     phrasing_without_interactive ], ATTRIB: OUT: [=`Button] *)

  type button =
    [ common
    | `Autofocus
    | `Disabled
    | `Form
    | `Formaction
    | `Formenctype
    | `Method
    | `Formnovalidate
    | `Formtarget
    | `Name
    | `Text_Value
    | `Button_Type ]

  (* NAME: select, KIND: star, TYPE: [= common |`Autofocus | `Multiple |
     `Name | `Size | `Form | `Disabled ], [ `Optgroup | `Option ],[=`Select],
     ARG: [ `Optgroup | `Option ], ATTRIB: OUT: [=`Select] *)

  type select =
    [ common
    | `Autofocus
    | `Multiple
    | `Name
    | `Size
    | `Form
    | `Disabled
    | `Required ]

  (* NAME: datalist, KIND: nullary, TYPE: [= common ], [=`Datalist], ARG:
     notag, ATTRIB: OUT: [=`Datalist] *)

  type datalist = [ | common]

  (* NAME: optgroup, KIND: star, TYPE: [= common | `Disabled | `Label ], [=
     `Option ], [=`Optgroup], ARG: [= `Option ], ATTRIB: OUT: [=`Optgroup] *)

  type optgroup = [common | `Disabled | `Label]

  type option =
    [common | `Selected | `Text_Value | `Disabled | `Label | `Value]

  (* NAME: keygen, KIND: nullary, TYPE: [= common | `Autofcus | `Challenge |
     `Disabled | `Form | `Keytype | `Name ], [=`Keygen], ARG: notag, ATTRIB:
     OUT: [=`Keygen] *)

  type keygen =
    [common | `Autofcus | `Challenge | `Disabled | `Form | `Keytype | `Name]

  (* NAME: progress, KIND: star, TYPE: [= common | `Float_Value |`Max| `Form
     ],[= phrasing_without_progress], [=`Progress], ARG: [=
     phrasing_without_progress], ATTRIB: OUT: [=`Progress] *)

  type progress = [common | `Float_Value | `Max | `Form]

  (* NAME: meter, KIND: star, TYPE: [= common |`Float_Value |`Min |`Max |`Low
     |`High |`Optimum |`Form],[= phrasing_without_meter ],[=`Meter], ARG: [=
     phrasing_without_meter ], ATTRIB: OUT: [=`Meter] *)

  type meter =
    [common | `Float_Value | `Min | `Max | `Low | `High | `Optimum | `Form]

  (* NAME: output_elt, KIND: star, TYPE: [= common |`Form |`Output_for
     |`Name],[= phrasing ],[=`Output], ARG: [= phrasing ], ATTRIB: OUT:
     [=`Output] *)

  type output_elt = [common | `Form | `Output_for | `Name]

  (* NAME: details, KIND: star, TYPE: [= common | `Open ], [= flow5] elt, [=
     `Details], ARG: [= flow5] elt, ATTRIB: OUT: [= `Details] *)

  type details = [common | `Open]

  (* NAME: summary, KIND: star, TYPE: [= common ],[= phrasing ], [=`Summary],
     ARG: [= phrasing ], ATTRIB: OUT: [=`Summary] *)

  type summary = [ | common]

  (* NAME: command, KIND: nullary, TYPE: [= common |`Icon |`Disabled
     |`Checked|`Radiogroup |`Command_Type], [=`Command], ARG: notag, ATTRIB:
     OUT: [=`Command] *)

  type command =
    [common | `Icon | `Disabled | `Checked | `Radiogroup | `Command_Type]

  (* NAME: menu, KIND: nullary, TYPE: [= common |`Label |`Menu_Type
     ],[=`Menu], ARG: notag, ATTRIB: OUT: [=`Menu] *)

  type menu = [common | `Label | `Menu_Type]

  (* NAME: noscript, KIND: plus, TYPE: [= common ], 'a, [=`Noscript of 'a],
     ARG: 'a, ATTRIB: OUT: [=`Noscript of 'a] *)
  type noscript = [ | common]

  (* NAME: meta, KIND: nullary, TYPE: [= common | `Http_equiv | `Name |
     `Content | `Charset ], [=`Meta], ARG: notag, ATTRIB: OUT: [=`Meta] *)

  type meta = [common | `Http_equiv | `Name | `Content | `Charset | `Property]

  (* NAME: style, KIND: star, TYPE: [= common | `Media | `Mime_type | `Scoped
     ], [= `PCDATA ], [=`Style], ARG: [= `PCDATA ], ATTRIB: OUT: [=`Style] *)

  type style = [common | `Media | `Mime_type | `Scoped]

  type subressource_integrity = [`Crossorigin | `Integrity]

  type script =
    [ common
    | subressource_integrity
    | `Async
    | `Charset
    | `Src
    | `Defer
    | `Script_type ]

  (* NAME: template, KIND: star, TYPE: [= common ], [= flow5 ], [=`Template],
     ARG: [= flow5 ], ATTRIB: OUT: [=`Template] *)

  type template = [ | common]

  (* NAME: link, KIND: nullary, TYPE: [= common | `Hreflang | `Media | `Rel |
     `Href | `Sizes | `Mime_type ], [=`Link], ARG: notag, ATTRIB: OUT:
     [=`Link] *)

  type link =
    [ common
    | subressource_integrity
    | `Hreflang
    | `Media
    | `Rel
    | `Href
    | `Sizes
    | `Mime_type ]

  type picture = [ | common]

  type referrerpolicy =
    [ `Empty
    | `No_referrer
    | `No_referrer_when_downgrade
    | `Origin
    | `Origin_when_cross_origin
    | `Same_origin
    | `Strict_origin
    | `Strict_origin_when_cross_origin
    | `Unsafe_url ]

  type big_variant =
    [ `W3_org_1999_xhtml
    | `Default
    | `Preserve
    | `Selected
    | `Get
    | `Post
    | `Checked
    | `Disabled
    | `ReadOnly
    | `Async
    | `Autofocus
    | `Autoplay
    | `Muted
    | `Anonymous
    | `Use_credentials
    | `Controls
    | `Ltr
    | `Rtl
    | `Formnovalidate
    | `Hidden
    | `Ismap
    | `Loop
    | `Novalidate
    | `Open
    | `Audio
    | `Metadata
    | `None
    | `Pubdate
    | `Required
    | `Reversed
    | `Scoped
    | `Seamless
    | `Hard
    | `Soft
    | `Multiple
    | `Checkbox
    | `Command
    | `Radio
    | `Context
    | `Toolbar
    | `Char
    | `Justify
    | `Left
    | `Right
    | `Col
    | `Colgroup
    | `Row
    | `Rowgroup
    | `All
    | `Cols
    | `Groups
    | `None
    | `Rows
    | `Rect
    | `Circle
    | `Poly
    | `Default
    | `One
    | `Zero
    | `Auto
    | `No
    | `Yes
    | `Defer
    | `Verbatim
    | `Latin
    | `Latin_name
    | `Latin_prose
    | `Full_width_latin
    | `Kana
    | `Katakana
    | `Numeric
    | `Tel
    | `Email
    | `Url
    | `Text
    | `Decimal
    | `Search ]

  type sandbox_token =
    [ `Allow_forms
    | `Allow_pointer_lock
    | `Allow_popups
    | `Allow_top_navigation
    | `Allow_same_origin
    | `Allow_script ]

  type input_type =
    [ `Button
    | `Checkbox
    | `Color
    | `Date
    | `Datetime
    | `Datetime_local
    | `Email
    | `File
    | `Hidden
    | `Image
    | `Month
    | `Number
    | `Password
    | `Radio
    | `Range
    | `Reset
    | `Search
    | `Submit
    | `Tel
    | `Text
    | `Time
    | `Url
    | `Week ]
end

module type Sig = sig
  open Terminology

  module Model : Model.Sig

  module Svg : Svg.Sig

  type 'a wrap := 'a Model.WrapValue.t

  module X := ModelType.Make(Model).At

  module At : module type of X

  open X

  val a_class : (string list, [> `Class]) cons
  (** This attribute assigns a class name or set of class names to an
      element. Any number of elements may be assigned the same class name or
      names. *)

  val a_user_data : string -> (string, [> `User_data]) cons
  (** May be used to specify custom attributes. The example given by the W3C
      is as follows :

      {v
<ol>
  <li data-length="2m11s">Beyond The Sea</li>
</ol>
      v}

      It should be used for preprocessing ends only. *)

  val a_id : (string, [> `Id]) cons
  (** This tute assigns a name to an element. This name must be unique in a
      document. The text should be without any space. *)

  val a_title : (string, [> `Title]) cons
  (** This tute offers advisory information about the element for which it is
      set. Values of the title tute may be rendered by user agents in a
      variety of ways. For instance, visual browsers frequently display the
      title as a {i tool tip} (a short message that appears when the pointing
      device pauses over an object). Audio user agents may speak the title
      information in a similar context. The title tute has an additional role
      when used with the [link] element to designate an external style sheet.
      Please consult the section on links and style sheets for details. *)

  (** {3 I18N} *)

  val a_xml_lang : (string, [> `XML_lang]) cons

  val a_lang : (string, [> `Lang]) cons

  (** {3 Events}

      {4 Javascript events} *)

  val a_onabort : (Model.Ev.base, [> `OnAbort]) cons_handler

  val a_onafterprint : (Model.Ev.base, [> `OnAfterPrint]) cons_handler

  val a_onbeforeprint : (Model.Ev.base, [> `OnBeforePrint]) cons_handler

  val a_onbeforeunload : (Model.Ev.base, [> `OnBeforeUnload]) cons_handler

  val a_onblur : (Model.Ev.base, [> `OnBlur]) cons_handler

  val a_oncanplay : (Model.Ev.base, [> `OnCanPlay]) cons_handler

  val a_oncanplaythrough :
    (Model.Ev.base, [> `OnCanPlayThrough]) cons_handler

  val a_onchange : (Model.Ev.base, [> `OnChange]) cons_handler

  val a_onclose : (Model.Ev.base, [> `OnClose]) cons_handler

  val a_ondurationchange :
    (Model.Ev.base, [> `OnDurationChange]) cons_handler

  val a_onemptied : (Model.Ev.base, [> `OnEmptied]) cons_handler

  val a_onended : (Model.Ev.base, [> `OnEnded]) cons_handler

  val a_onerror : (Model.Ev.base, [> `OnError]) cons_handler

  val a_onfocus : (Model.Ev.base, [> `OnFocus]) cons_handler

  val a_onformchange : (Model.Ev.base, [> `OnFormChange]) cons_handler

  val a_onforminput : (Model.Ev.base, [> `OnFormInput]) cons_handler

  val a_onhashchange : (Model.Ev.base, [> `OnHashChange]) cons_handler

  val a_oninput : (Model.Ev.base, [> `OnInput]) cons_handler

  val a_oninvalid : (Model.Ev.base, [> `OnInvalid]) cons_handler

  val a_onmousewheel : (Model.Ev.base, [> `OnMouseWheel]) cons_handler

  val a_onoffline : (Model.Ev.base, [> `OnOffLine]) cons_handler

  val a_ononline : (Model.Ev.base, [> `OnOnLine]) cons_handler

  val a_onpause : (Model.Ev.base, [> `OnPause]) cons_handler

  val a_onplay : (Model.Ev.base, [> `OnPlay]) cons_handler

  val a_onplaying : (Model.Ev.base, [> `OnPlaying]) cons_handler

  val a_onpagehide : (Model.Ev.base, [> `OnPageHide]) cons_handler

  val a_onpageshow : (Model.Ev.base, [> `OnPageShow]) cons_handler

  val a_onpopstate : (Model.Ev.base, [> `OnPopState]) cons_handler

  val a_onprogress : (Model.Ev.base, [> `OnProgress]) cons_handler

  val a_onratechange : (Model.Ev.base, [> `OnRateChange]) cons_handler

  val a_onreadystatechange :
    (Model.Ev.base, [> `OnReadyStateChange]) cons_handler

  val a_onredo : (Model.Ev.base, [> `OnRedo]) cons_handler

  val a_onresize : (Model.Ev.base, [> `OnResize]) cons_handler

  val a_onscroll : (Model.Ev.base, [> `OnScroll]) cons_handler

  val a_onseeked : (Model.Ev.base, [> `OnSeeked]) cons_handler

  val a_onseeking : (Model.Ev.base, [> `OnSeeking]) cons_handler

  val a_onselect : (Model.Ev.base, [> `OnSelect]) cons_handler

  val a_onshow : (Model.Ev.base, [> `OnShow]) cons_handler

  val a_onstalled : (Model.Ev.base, [> `OnStalled]) cons_handler

  val a_onstorage : (Model.Ev.base, [> `OnStorage]) cons_handler

  val a_onsubmit : (Model.Ev.base, [> `OnSubmit]) cons_handler

  val a_onsuspend : (Model.Ev.base, [> `OnSuspend]) cons_handler

  val a_ontimeupdate : (Model.Ev.base, [> `OnTimeUpdate]) cons_handler

  val a_onundo : (Model.Ev.base, [> `OnUndo]) cons_handler

  val a_onunload : (Model.Ev.base, [> `OnUnload]) cons_handler

  val a_onvolumechange : (Model.Ev.base, [> `OnVolumeChange]) cons_handler

  val a_onwaiting : (Model.Ev.base, [> `OnWaiting]) cons_handler

  val a_onload : (Model.Ev.base, [> `OnLoad]) cons_handler

  val a_onloadeddata : (Model.Ev.base, [> `OnLoadedData]) cons_handler

  val a_onloadedmetadata :
    (Model.Ev.base, [> `OnLoadedMetaData]) cons_handler

  val a_onloadstart : (Model.Ev.base, [> `OnLoadStart]) cons_handler

  val a_onmessage : (Model.Ev.base, [> `OnMessage]) cons_handler

  (** {4 Mouse events} *)

  val a_onclick : (Model.Ev.mouse, [> `OnClick]) cons_handler

  val a_oncontextmenu : (Model.Ev.mouse, [> `OnContextMenu]) cons_handler

  val a_ondblclick : (Model.Ev.mouse, [> `OnDblClick]) cons_handler

  val a_ondrag : (Model.Ev.mouse, [> `OnDrag]) cons_handler

  val a_ondragend : (Model.Ev.mouse, [> `OnDragEnd]) cons_handler

  val a_ondragenter : (Model.Ev.mouse, [> `OnDragEnter]) cons_handler

  val a_ondragleave : (Model.Ev.mouse, [> `OnDragLeave]) cons_handler

  val a_ondragover : (Model.Ev.mouse, [> `OnDragOver]) cons_handler

  val a_ondragstart : (Model.Ev.mouse, [> `OnDragStart]) cons_handler

  val a_ondrop : (Model.Ev.mouse, [> `OnDrop]) cons_handler

  val a_onmousedown : (Model.Ev.mouse, [> `OnMouseDown]) cons_handler

  val a_onmouseup : (Model.Ev.mouse, [> `OnMouseUp]) cons_handler

  val a_onmouseover : (Model.Ev.mouse, [> `OnMouseOver]) cons_handler

  val a_onmousemove : (Model.Ev.mouse, [> `OnMouseMove]) cons_handler

  val a_onmouseout : (Model.Ev.mouse, [> `OnMouseOut]) cons_handler

  val a_ontouchstart : (Model.Ev.touch, [> `OnTouchStart]) cons_handler
  (** {4 Touch events} *)

  val a_ontouchend : (Model.Ev.touch, [> `OnTouchEnd]) cons_handler

  val a_ontouchmove : (Model.Ev.touch, [> `OnTouchMove]) cons_handler

  val a_ontouchcancel : (Model.Ev.touch, [> `OnTouchCancel]) cons_handler

  (** {4 Keyboard events} *)

  val a_onkeypress : (Model.Ev.keyboard, [> `OnKeyPress]) cons_handler

  val a_onkeydown : (Model.Ev.keyboard, [> `OnKeyDown]) cons_handler

  val a_onkeyup : (Model.Ev.keyboard, [> `OnKeyUp]) cons_handler

  (** {3 Other tutes} *)

  val a_allowfullscreen : unit -> [> `Allowfullscreen] t

  val a_allowpaymentrequest : unit -> [> `Allowpaymentrequest] t

  val a_autocomplete : (autocomplete_option, [> `Autocomplete]) cons
  (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete>
        autocomplete documentation. *)

  val a_async : unit -> [> `Async] t

  val a_autofocus : unit -> [> `Autofocus] t

  val a_autoplay : unit -> [> `Autoplay] t

  val a_muted : unit -> [> `Muted] t

  val a_crossorigin :
    ([`Anonymous | `Use_credentials], [> `Crossorigin]) cons

  val a_integrity : (string, [> `Integrity]) cons

  val a_mediagroup : (string, [> `Mediagroup]) cons

  val a_challenge : (string, [> `Challenge]) cons

  val a_contenteditable : (bool, [> `Contenteditable]) cons

  val a_contextmenu : (string, [> `Contextmenu]) cons

  val a_controls : unit -> [> `Controls] t

  val a_dir : ([`Rtl | `Ltr], [> `Dir]) cons

  val a_draggable : (bool, [> `Draggable]) cons

  val a_form : (string, [> `Form]) cons

  val a_formaction : (Model.Uri.t, [> `Formaction]) cons

  val a_formenctype : (string, [> `Formenctype]) cons

  val a_formnovalidate : unit -> [> `Formnovalidate] t

  val a_formtarget : (string, [> `Formtarget]) cons

  val a_hidden : unit -> [> `Hidden] t

  val a_high : (float, [> `High]) cons

  val a_icon : (Model.Uri.t, [> `Icon]) cons

  val a_ismap : unit -> [> `Ismap] t

  val a_keytype : (string, [> `Keytype]) cons

  val a_list : (string, [> `List]) cons

  val a_loop : unit -> [> `Loop] t

  val a_low : (float, [> `High]) cons

  val a_max : (float, [> `Max]) cons

  val a_input_max : (number_or_datetime, [> `Input_Max]) cons

  val a_min : (float, [> `Min]) cons

  val a_input_min : (number_or_datetime, [> `Input_Min]) cons

  val a_inputmode :
    ( [`None | `Text | `Decimal | `Numeric | `Tel | `Search | `Email | `Url]
    , [> `Inputmode] )
    cons
  (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/inputmode>
        inputmode documentation. *)

  val a_novalidate : unit -> [> `Novalidate] t

  val a_open' : unit -> [> `Open] t

  val a_optimum : (float, [> `Optimum]) cons

  val a_pattern : (string, [> `Pattern]) cons

  val a_placeholder : (string, [> `Placeholder]) cons

  val a_poster : (Model.Uri.t, [> `Poster]) cons

  val a_preload : ([`None | `Metadata | `Audio], [> `Preload]) cons

  val a_pubdate : unit -> [> `Pubdate] t

  val a_radiogroup : (string, [> `Radiogroup]) cons

  val a_referrerpolicy : (referrerpolicy, [> `Referrerpolicy]) cons
  (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#Attributes> *)

  val a_required : unit -> [> `Required] t

  val a_reversed : unit -> [> `Reversed] t

  val a_sandbox : (sandbox_token list, [> `Sandbox]) cons

  val a_spellcheck : (bool, [> `Spellcheck]) cons

  val a_scoped : unit -> [> `Scoped] t

  val a_seamless : unit -> [> `Seamless] t

  val a_sizes : ((int * int) list option, [> `Sizes]) cons

  val a_span : (int, [> `Span]) cons

  val a_srcset : (Model.Uri.t image_candidate list, [> `Srcset]) cons

  val a_img_sizes : (string list, [> `Img_sizes]) cons

  val a_start : (int, [> `Start]) cons

  val a_step : (float option, [> `Step]) cons

  val a_translate : ([`Yes | `No], [> `Translate]) cons
  (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/translate>
        [translate] global tute documentation. *)

  val a_wrap : ([`Soft | `Hard], [> `Wrap]) cons

  val a_version : (string, [> `Version]) cons

  val a_xmlns : ([`W3_org_1999_xhtml], [> `XMLns]) cons

  val a_manifest : (Model.Uri.t, [> `Manifest]) cons

  val a_cite : (Model.Uri.t, [> `Cite]) cons

  val a_xml_space : ([`Default | `Preserve], [> `XML_space]) cons

  val a_accesskey : (char, [> `Accesskey]) cons
  (** This tute assigns an access key to an element. An access key is a
      single character from the document character set. NB: authors should
      consider the input method of the expected reader when specifying an
      accesskey. *)

  val a_charset : (string, [> `Charset]) cons
  (** This tute specifies the character encoding of the resource designated
      by the link. Please consult the section on character encodings for more
      details. *)

  val a_accept_charset : (string list, [> `Accept_charset]) cons

  val a_accept : (string list, [> `Accept]) cons

  val a_href : (Model.Uri.t, [> `Href]) cons
  (** This tute specifies the location of a Web resource, thus defining a
      link between the current element (the source anchor) and the
      destination anchor defined by this tute. *)

  val a_hreflang : (string, [> `Hreflang]) cons
  (** This tute specifies the base language of the resource designated by
      href and may only be used when href is specified. *)

  val a_download : (string option, [> `Download]) cons

  val a_rel : (linktype list, [> `Rel]) cons
  (** This tute describes the relationship from the current document to the
      anchor specified by the href tute. The value of this tute is a
      space-separated list of link types. *)

  (** This tute is used to describe a reverse link from the anchor specified
      by the href tute to the current document. The value of this tute is a
      space-separated list of link types. *)

  val a_tabindex : (int, [> `Tabindex]) cons
  (** This tute specifies the position of the current element in the tabbing
      order for the current document. This value must be a number between 0
      and 32767. User agents should ignore leading zeros. *)

  val a_mime_type : (string, [> `Mime_type]) cons
  (** This tute gives an advisory hint as to the content type of the content
      available at the link target address. It allows user agents to opt to
      use a fallback mechanism rather than fetch the content if they are
      advised that they will get content in a content type they do not
      support.Authors who use this tute take responsibility to manage the
      risk that it may become inconsistent with the content available at the
      link target address. *)

  val a_datetime : (string, [> `Datetime]) cons

  val a_action : (Model.Uri.t, [> `Action]) cons
  (** This tute specifies a form processing agent. User agent behavior for a
      value other than an HTTP URI is undefined. *)

  val a_checked : unit -> [> `Checked] t
  (** When the [type] tute has the value ["radio"] or ["checkbox"], this
      boolean tute specifies that the button is on. User agents must ignore
      this tute for other control types. *)

  val a_cols : (int, [> `Cols]) cons
  (** This tute specifies the visible width in average character widths.
      Users should be able to enter longer lines than this, so user agents
      should provide some means to scroll through the contents of the control
      when the contents extend beyond the visible area. User agents may
      visible text lines to keep long lines visible without the need for
      scrolling. *)

  val a_enctype : (string, [> `Enctype]) cons

  val a_label_for : (string, [> `Label_for]) cons
    [@@reflect.attribute "for" ["label"]]

  val a_output_for : (string list, [> `Output_for]) cons
    [@@reflect.attribute "for" ["output"]]

  val a_maxlength : (int, [> `Maxlength]) cons

  val a_minlength : (int, [> `Minlength]) cons

  val a_method' : ([`Get | `Post], [> `Method]) cons

  val a_multiple : unit -> [> `Multiple] t

  val a_name : (string, [> `Name]) cons
  (** This tute assigns the control name. *)

  val a_rows : (int, [> `Rows]) cons
  (** This tute specifies the number of visible text lines. Users should be
      able to enter more lines than this, so user agents should provide some
      means to scroll through the contents of the control when the contents
      extend beyond the visible area. *)

  val a_selected : unit -> [> `Selected] t
  (** When set, this boolean tute specifies that this option is pre-selected. *)

  val a_size : (int, [> `Size]) cons

  val a_src : (Model.Uri.t, [> `Src]) cons

  val a_input_type :
    ( [ `Url
      | `Tel
      | `Text
      | `Time
      | `Search
      | `Password
      | `Checkbox
      | `Range
      | `Radio
      | `Submit
      | `Reset
      | `Number
      | `Hidden
      | `Month
      | `Week
      | `File
      | `Email
      | `Image
      | `Datetime_local
      | `Datetime
      | `Date
      | `Color
      | `Button ]
    , [> `Input_Type] )
    cons
    [@@reflect.attribute "type" ["input"]]

  val a_text_value : (string, [> `Text_Value]) cons
    [@@reflect.attribute "value" ["param"; "button"; "option"]]
  (** This tute specifies the initial value of the control. If this tute is
      not set, the initial value is set to the contents of the [option]
      element. *)

  val a_int_value : (int, [> `Int_Value]) cons
    [@@reflect.attribute "value" ["li"]]

  val a_value : (string, [> `Value]) cons

  val a_float_value : (float, [> `Float_Value]) cons
    [@@reflect.attribute "value" ["progress"; "meter"]]

  val a_disabled : unit -> [> `Disabled] t

  val a_readonly : unit -> [> `ReadOnly] t

  val a_button_type : ([`Button | `Submit | `Reset], [> `Button_Type]) cons
    [@@reflect.attribute "type" ["button"]]

  val a_script_type : (script_type, [> `Script_type]) cons
    [@@reflect.attribute "type" ["script"]]

  val a_command_type :
    ([`Command | `Checkbox | `Radio], [> `Command_Type]) cons
    [@@reflect.attribute "type" ["command"]]

  val a_menu_type : ([`Context | `Toolbar], [> `Menu_Type]) cons
    [@@reflect.attribute "type" ["menu"]]

  val a_label : (string, [> `Label]) cons

  val a_colspan : (int, [> `Colspan]) cons

  val a_headers : (string list, [> `Headers]) cons

  val a_rowspan : (int, [> `Rowspan]) cons

  val a_alt : (string, [> `Alt]) cons

  val a_height : (int, [> `Height]) cons

  val a_width : (int, [> `Width]) cons

  val a_shape : (shape, [> `Shape]) cons

  val a_coords : (int list, [> `Coords]) cons

  val a_usemap : (string, [> `Usemap]) cons

  val a_data : (Model.Uri.t, [> `Data]) cons

  val a_scrolling : ([`Yes | `No | `Auto], [> `Scrolling]) cons

  val a_target : (string, [> `Target]) cons

  val a_content : (string, [> `Content]) cons

  val a_http_equiv : (string, [> `Http_equiv]) cons

  val a_defer : unit -> [> `Defer] t

  val a_media : (mediadesc, [> `Media]) cons

  val a_style : (string, [> `Style_Attr]) cons

  val a_property : (string, [> `Property]) cons

  (** {3 ARIA support} *)

  (** {{:https://www.w3.org/TR/wai-aria-1.1/} WAI-ARIA} is a specification
      written by the W3C, defining a set of additional HTML tutes that can be
      applied to elements to provide additional semantics and improve
      accessibility wherever it is lacking. See for example a
      {{:https://developer.mozilla.org/en-US/docs/Learn/Accessibility/WAI-ARIbasics}
      WAI-ARIA tutorial}. *)

  val a_role : (string list, [> `Role]) cons
  (** @see <https://www.w3.org/TR/role-attribute> Role tute specification
      @see <https://www.w3.org/TR/wai-aria-1.1/#role_definitions>
        List of WAI-ARIA roles *)

  (** {2:elements Elements} *)

  module Co : sig
    module X := ModelType.Make(Model).Co

    include module type of X

    type 'a star_cl := ?cl:string list -> ?id:string -> 'a star

    val html : ?a:Attrib.html At.t list -> t -> t -> t
      [@@reflect.filter_whitespace] [@@reflect.element "html"]

    val head : ?a:Attrib.head At.t list -> t list -> t
      [@@reflect.filter_whitespace] [@@reflect.element "head"]

    val base : Attrib.base nullary

    val title : Attrib.title unary

    val body : Attrib.body star

    val svg : ?a:Attrib.svg Svg.At.t list -> t list -> t

    (** {3 Section} *)

    val footer : Attrib.footer star_cl

    val header : Attrib.header star_cl

    val section : Attrib.section star_cl

    val nav : Attrib.nav star_cl

    val h1 : Attrib.h1 star_cl

    val h2 : Attrib.h2 star_cl

    val h3 : Attrib.h3 star_cl

    val h4 : Attrib.h4 star_cl

    val h5 : Attrib.h5 star_cl

    val h6 : Attrib.h6 star_cl

    val hgroup : Attrib.hgroup star_cl

    val address : Attrib.address star_cl

    val article : Attrib.article star_cl

    val aside : Attrib.aside star_cl

    val main : Attrib.main star_cl

    (** {3 Grouping content} *)

    val p : Attrib.p star_cl

    val pre : Attrib.pre star_cl

    val blockquote : Attrib.blockquote star_cl

    val dialog : Attrib.dialog star_cl
    (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dialog> *)

    val div : Attrib.div star_cl

    val dl : Attrib.dl star_cl

    val ol : Attrib.ol star_cl [@@reflect.filter_whitespace]

    val ul : Attrib.ul star_cl [@@reflect.filter_whitespace]

    val dd : Attrib.dd star_cl

    val dt : Attrib.dt star_cl

    val li : Attrib.li star_cl

    val figcaption : Attrib.figcaption star_cl

    val figure : ?figcaption:[`Top of t | `Bottom of t] -> Attrib.figure star
      [@@reflect.element "figure"]

    val hr : Attrib.hr nullary

    (** {3 Semantic} *)

    val b : Attrib.b star_cl

    val i : Attrib.i star_cl

    val u : Attrib.u star_cl

    val small : Attrib.small star_cl

    val sub : Attrib.sub star_cl

    val sup : Attrib.sup star_cl

    val mark : Attrib.mark star_cl

    val wbr : Attrib.wbr nullary

    val bdo : dir:[`Ltr | `Rtl] wrap -> Attrib.bdo star

    val abbr : Attrib.abbr star_cl

    val br : Attrib.br nullary

    val cite : Attrib.cite star_cl

    val code : Attrib.code star_cl

    val dfn : Attrib.dfn star_cl

    val em : Attrib.em star_cl

    val kbd : Attrib.kbd star_cl

    val q : Attrib.q star_cl

    val samp : Attrib.samp star_cl

    val span : Attrib.span star_cl

    val strong : Attrib.strong star_cl

    val time : Attrib.time star_cl

    val var : Attrib.var star_cl

    (** {3 Hypertext} *)

    val a : Attrib.a star_cl

    (** {3 Edit} *)

    val del : Attrib.del star_cl

    val ins : Attrib.ins star_cl

    (** {3 Embedded} *)

    val img : src:Model.Uri.t wrap -> alt:string wrap -> Attrib.img nullary

    val picture : img:t -> Attrib.picture star
      [@@reflect.filter_whitespace] [@@reflect.element "picture"]
    (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture>
          Picture element documentation on MDN *)

    val iframe : Attrib.iframe star_cl

    val object_ : ?params:t list -> Attrib.object_ star
      [@@reflect.element "object_" "object"]

    val param : Attrib.param nullary

    val embed : Attrib.embed nullary

    val audio : ?src:Model.Uri.t wrap -> ?srcs:t list -> Attrib.audio star
      [@@reflect.element "audio_video"]

    val video : ?src:Model.Uri.t wrap -> ?srcs:t list -> Attrib.video star
      [@@reflect.element "audio_video"]

    val canvas : Attrib.canvas star_cl

    val source : Attrib.source nullary

    val area :
         alt:string wrap
      -> [ Attrib.common
         | `Alt
         | `Coords
         | `Shape
         | `Target
         | `Rel
         | `Media
         | `Hreflang
         | `Mime_type ]
         nullary

    val map : Attrib.map star_cl

    (** {3 Tables Data} *)

    val caption : Attrib.caption star_cl

    val table :
         ?caption:t
      -> ?columns:t
      -> ?thead:t
      -> ?tfoot:t list
      -> Attrib.table star
      [@@reflect.filter_whitespace] [@@reflect.element "table"]

    val tablex :
         ?caption:t
      -> ?columns:t
      -> ?thead:t
      -> ?tfoot:t list
      -> Attrib.tablex star
      [@@reflect.filter_whitespace] [@@reflect.element "table" "table"]

    val colgroup : Attrib.colgroup star_cl [@@reflect.filter_whitespace]

    val col : Attrib.col nullary

    val thead : Attrib.thead star_cl [@@reflect.filter_whitespace]

    val tbody : Attrib.tbody star_cl [@@reflect.filter_whitespace]

    val tfoot : Attrib.tfoot star_cl [@@reflect.filter_whitespace]

    val td : Attrib.td star_cl

    val th : Attrib.th star_cl

    val tr : Attrib.tr star_cl [@@reflect.filter_whitespace]

    (** {3 Forms} *)

    val form : Attrib.form star_cl

    val fieldset : ?legend:t -> Attrib.fieldset star
      [@@reflect.element "fieldset"]

    val legend : Attrib.legend star_cl

    val label : Attrib.label star_cl
    (** Label authorizes only one control inside them that should be labelled
        with a [for] tute (although it is not necessary). Such constraints
        are not currently enforced by the type-system *)

    val input : Attrib.input nullary

    val button : Attrib.button star_cl

    val select : Attrib.select star_cl [@@reflect.filter_whitespace]

    val datalist :
         ?children:[`Options of t list | `Phras of t list]
      -> Attrib.datalist nullary

    val optgroup : label:string wrap -> Attrib.optgroup star

    val option : Attrib.option unary

    val textarea : Attrib.textarea unary

    val keygen : Attrib.keygen nullary

    val progress : Attrib.progress star_cl

    val meter : Attrib.meter star_cl

    val output_elt : Attrib.output_elt star_cl
      [@@reflect.element "star_cl" "output"]

    (** {3 Data} *)

    (** {3 Interactive} *)

    val details : Attrib.details star_cl [@@reflect.element "details"]

    val summary : Attrib.summary star_cl

    val command : label:string wrap -> Attrib.command nullary

    val menu : ?children:[`Lis of t | `Flows of t] -> Attrib.menu nullary
      [@@reflect.element "menu"]

    (** {3 Scripting} *)

    val script : Attrib.script unary [@@reflect.element "script"]

    val noscript : Attrib.noscript star_cl

    val template : Attrib.template star_cl
    (** @see <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template>
          Template element documentation on MDN *)

    val meta : Attrib.meta nullary

    (** {3 Style Sheets} *)

    val style : Attrib.style star

    val link :
      rel:linktype list -> href:Model.Uri.t wrap -> Attrib.link nullary

    (** {3 Ruby} *)

    val rt : Attrib.rt star_cl

    val rp : Attrib.rp star_cl

    val ruby : Attrib.ruby star_cl
  end
end
