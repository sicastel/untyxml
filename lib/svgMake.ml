module C = struct
  let string_of_alignment_baseline = function
    | `Auto -> "auto"
    | `Baseline -> "baseline"
    | `Before_edge -> "before-edge"
    | `Text_before_edge -> "text-before-edge"
    | `Middle -> "middle"
    | `Central -> "central"
    | `After_edge -> "after-edge"
    | `Text_after_edge -> "text-after-edge"
    | `Ideographic -> "ideographic"
    | `Alphabetic -> "alphabetic"
    | `Hanging -> "hanging"
    | `Mathematical -> "mathematical"
    | `Inherit -> "inherit"

  let string_of_big_variant = function
    | `A -> "a"
    | `Absolute_colorimetric -> "absolute_colorimetric"
    | `Align -> ""
    | `Always -> "always"
    | `Atop -> "atop"
    | `Arithmetic -> "arithmetic"
    | `Auto -> "auto"
    | `B -> "b"
    | `Bever -> "bevel"
    | `Blink -> "blink"
    | `Butt -> "butt"
    | `CSS -> "CSS"
    | `Darken -> "darken"
    | `Default -> "default"
    | `Dilate -> "dilate"
    | `Disable -> "disable"
    | `Discrete -> "discrete"
    | `Duplicate -> "duplicate"
    | `End -> "end"
    | `Erode -> "erode"
    | `Exact -> "exact"
    | `FractalNoise -> "fractalNoise"
    | `Freeze -> "freeze"
    | `HueRotate -> "hueRotate"
    | `G -> "g"
    | `Gamma -> "gamma"
    | `GeometricPrecision -> "geometricPrecision"
    | `H -> "h"
    | `Identity -> "identity"
    | `In -> "in"
    | `Inherit -> "inherit"
    | `Initial -> "initial"
    | `Isolated -> "isolated"
    | `Lighten -> "lighten"
    | `Line_through -> "line-through"
    | `Linear -> "linear"
    | `LuminanceToAlpha -> "luminanceToAlpha"
    | `Magnify -> "magnify"
    | `Matrix -> "matrix"
    | `Medial -> "medial"
    | `Middle -> "middle"
    | `Miter -> "miter"
    | `Multiply -> "multiply"
    | `Never -> "never"
    | `New -> "new"
    | `None -> "none"
    | `Normal -> "normal"
    | `NoStitch -> "noStitch"
    | `ObjectBoundingBox -> "objectBoundingBox"
    | `OnLoad -> "onLoad"
    | `OnRequest -> "onRequest"
    | `OptimizeLegibility -> "optimizeLegibility"
    | `OptimizeSpeed -> "optimizeSpeed"
    | `Other -> "other"
    | `Out -> "out"
    | `Over -> "over"
    | `Overline -> "overline"
    | `Paced -> "paced"
    | `Pad -> "pad"
    | `Perceptual -> "perceptual"
    | `Preserve -> "preserve"
    | `R -> "r"
    | `Reflect -> "reflect"
    | `Remove -> "remove"
    | `Repeat -> "repeat"
    | `Replace -> "replace"
    | `Relative_colorimetric -> "relative_colorimetric"
    | `Rotate -> "rotate"
    | `Round -> "round"
    | `Saturate -> "saturate"
    | `Saturation -> "saturation"
    | `Scale -> "scale"
    | `Screen -> "screen"
    | `SkewX -> "skewX"
    | `SkewY -> "skewY"
    | `Spacing -> "spacing"
    | `SpacingAndGlyphs -> "spacingAndGlyphs"
    | `Spline -> "spline"
    | `Square -> "square"
    | `Start -> "start"
    | `Stitch -> "stitch"
    | `Stretch -> "stretch"
    | `StrokeWidth -> "stroke-width"
    | `Sum -> "sum"
    | `Table -> "table"
    | `Terminal -> "terminal"
    | `Translate -> "translate"
    | `Turbulence -> "turbulence"
    | `Underline -> "underline"
    | `UserSpaceOnUse -> "userSpaceOnUse"
    | `V -> "v"
    | `WhenNotActive -> "whenNotActive"
    | `Wrap -> "wrap"
    | `XML -> "XML"
    | `Xor -> "xor"

  let opt_concat ?(sep = " ") s f = function
    | Some x -> s ^ sep ^ f x
    | None -> s

  let list ?(sep = " ") f l = String.concat sep (List.map f l)

  let string_of_paint_whitout_icc = function
    | `None -> "none"
    | `CurrentColor -> "currentColor"
    | `Color (c, icc) -> opt_concat c (fun x -> x) icc

  let string_of_paint = function
    | `Icc (iri, None) -> iri
    | `Icc (iri, Some b) -> iri ^ " " ^ string_of_paint_whitout_icc b
    | #Svg.Terminology.paint_whitout_icc as c ->
        string_of_paint_whitout_icc c

  let string_of_fill_rule = function
    | `Nonzero -> "nonzero"
    | `Evenodd -> "evenodd"

  (* let rel x     = (x, None) *)
  (* let deg x     = (x, Some `Deg) *)
  (* let grad x    = (x, Some `Grad) *)
  (* let rad x     = (x, Some `Rad) *)
  (* let ms x      = (x, Some `Ms) *)
  (* let s x       = (x, Some `S) *)
  (* let em x      = (x, Some `Em) *)
  (* let ex x      = (x, Some `Ex) *)
  (* let px x      = (x, Some `Px) *)
  (* let in_ x     = (x, Some `In) *)
  (* let cm x      = (x, Some `Cm) *)
  (* let mm x      = (x, Some `Mm) *)
  (* let pt x      = (x, Some `Pt) *)
  (* let pc x      = (x, Some `Pc) *)
  (* let percent x = (x, Some `Percent) *)
  (* let hz x      = (x, Some `Hz) *)
  (* let khz x     = (x, Some `KHz) *)

  let to_string f (n, unit) =
    Printf.sprintf "%g%s" n
      (match unit with Some unit -> f unit | None -> "")

  let angle_names = function
    | `Deg -> "deg"
    | `Grad -> "grad"
    | `Rad -> "rad"

  let string_of_angle a = to_string angle_names a

  (* let time_names = function `Ms -> "ms" | `S -> "s" *)
  (* let string_of_time a = to_string time_names a *)

  let length_names = function
    | `Em -> "em"
    | `Ex -> "ex"
    | `Px -> "px"
    | `In -> "in"
    | `Cm -> "cm"
    | `Mm -> "mm"
    | `Pt -> "pt"
    | `Pc -> "pc"
    | `Percent -> "%"

  let string_of_length (a : Svg.Terminology.Unit.length) =
    to_string length_names a

  let string_of_coord = string_of_length

  let string_of_length = string_of_length

  let string_of_coords = list (fun (a, b) -> Printf.sprintf "%g, %g" a b)

  let string_of_number = string_of_float

  let string_of_lengths = list string_of_length

  let string_of_percentage x = string_of_number x ^ "%"

  let string_of_fourfloats (a, b, c, d) =
    Printf.sprintf "%s %s %s %s" (string_of_number a) (string_of_number b)
      (string_of_number c) (string_of_number d)

  let string_of_number_optional_number = function
    | x, Some y -> Printf.sprintf "%g, %g" x y
    | x, None -> Printf.sprintf "%g" x

  let string_of_offset = function
    | `Number x -> string_of_float x
    | `Percentage x -> string_of_percentage x

  let string_of_orient = function
    | None -> "auto"
    | Some __svg -> string_of_angle __svg

  let string_of_transform = function
    | `Matrix (a, b, c, d, e, f) ->
        Printf.sprintf "matrix(%g %g %g %g %g %g)" a b c d e f
    | `Translate x ->
        Printf.sprintf "translate(%s)" (string_of_number_optional_number x)
    | `Scale x ->
        Printf.sprintf "scale(%s)" (string_of_number_optional_number x)
    | `Rotate (angle, x) ->
        Printf.sprintf "rotate(%s %s)" (string_of_angle angle)
          ( match x with
          | Some (x, y) -> Printf.sprintf "%g %g" x y
          | None -> "" )
    | `SkewX angle -> Printf.sprintf "skewX(%s)" (string_of_angle angle)
    | `SkewY angle -> Printf.sprintf "skewY(%s)" (string_of_angle angle)

  let string_of_transforms l =
    String.concat " " (List.map string_of_transform l)

  let string_of_numbers = list string_of_number

  let string_of_numbers_semicolon = list ~sep:"; " string_of_number

  let string_of_paint = string_of_paint

  let string_of_fill_rule = string_of_fill_rule

  let string_of_strokedasharray = function
    | [] -> "none"
    | l -> list string_of_length l

  let string_of_dominant_baseline = function
    | `Auto -> "auto"
    | `Use_script -> "usescript"
    | `No_change -> "nochange"
    | `Reset_size -> "resetsize"
    | `Ideographic -> "ideographic"
    | `Alphabetic -> "alphabetic"
    | `Hanging -> "hanging"
    | `Mathematical -> "mathematical"
    | `Central -> "central"
    | `Middle -> "middle"
    | `Text_after_edge -> "textafteredge"
    | `Text_before_edge -> "textbeforeedge"
    | `Inherit -> "inherit"

  let string_of_in_value = function
    | `SourceGraphic -> "sourceGraphic"
    | `SourceAlpha -> "sourceAlpha"
    | `BackgroundImage -> "backgroundImage"
    | `BackgroundAlpha -> "backgroundAlpha"
    | `FillPaint -> "fillPaint"
    | `StrokePaint -> "strokePaint"
    | `Ref _svg -> _svg
end

module Make (Model : Model.Sig) = struct
  module Model = Model
  module X = ModelType.Make (Model)
  module At = X.At
  open At

  let a_x = user C.string_of_coord "x"

  let a_y = user C.string_of_coord "y"

  let a_width = user C.string_of_length "width"

  let a_height = user C.string_of_length "height"

  let a_preserveAspectRatio = string "preserveAspectRatio"

  let a_contentScriptType = string "contentScriptType"

  let a_contentStyleType = string "contentStyleType"

  let a_zoomAndPan = user C.string_of_big_variant "zoomAndPan"

  let a_href = string "href"

  let a_xlink_href = string "xlink:href"

  let a_requiredFeatures = space_sep "requiredFeatures"

  let a_requiredExtensions = space_sep "requiredExtension"

  let a_systemLanguage = comma_sep "systemLanguage"

  let a_externalRessourcesRequired =
    user Bool.to_string "externalRessourcesRequired"

  let a_id = string "id"

  let a_user_data name = string ("data-" ^ name)

  let a_xml_base = string "xml:base"

  let a_xml_lang = string "xml:lang"

  let a_xml_space = user C.string_of_big_variant "xml:space"

  let a_type' = string "type"

  let a_media = comma_sep "media"

  let a_xlink_title = string "xlink:title"

  let a_class = space_sep "class"

  let a_style = string "style"

  let a_transform = user C.string_of_transforms "transform"

  let a_viewBox = user C.string_of_fourfloats "viewBox"

  let a_d = string "d"

  let a_pathLength = float "pathLength"

  let a_rx = user C.string_of_length "rx"

  let a_ry = user C.string_of_length "ry"

  let a_cx = user C.string_of_length "cx"

  let a_cy = user C.string_of_length "cy"

  let a_r = user C.string_of_length "r"

  let a_x1 = user C.string_of_coord "x1"

  let a_y1 = user C.string_of_coord "y1"

  let a_x2 = user C.string_of_coord "x2"

  let a_y2 = user C.string_of_coord "y2"

  let a_points = user C.string_of_coords "points"

  let a_x_list = user C.string_of_lengths "x"

  let a_y_list = user C.string_of_lengths "y"

  let a_dx = user string_of_float "dx"

  let a_dy = user string_of_float "dy"

  let a_dx_list = user C.string_of_lengths "dx"

  let a_dy_list = user C.string_of_lengths "dy"

  let a_lengthAdjust = user C.string_of_big_variant "lengthAdjust"

  let a_textLength = user C.string_of_length "textLength"

  let a_text_anchor = user C.string_of_big_variant "text-anchor"

  let a_text_decoration = user C.string_of_big_variant "text-decoration"

  let a_text_rendering = user C.string_of_big_variant "text-rendering"

  let a_rotate = user C.string_of_numbers "rotate"

  let a_startOffset = user C.string_of_length "startOffset"

  let a_method' = user C.string_of_big_variant "method"

  let a_spacing = user C.string_of_big_variant "spacing"

  let a_glyphRef = string "glyphRef"

  let a_format = string "format"

  let a_markerUnits = user C.string_of_big_variant "markerUnits"

  let a_refX = user C.string_of_coord "refX"

  let a_refY = user C.string_of_coord "refY"

  let a_markerWidth = user C.string_of_length "markerWidth"

  let a_markerHeight = user C.string_of_length "markerHeight"

  let a_orient = user C.string_of_orient "orient"

  let a_local = string "local"

  let a_rendering_intent = user C.string_of_big_variant "rendering-intent"

  let a_gradientUnits = user C.string_of_big_variant "gradientUnits"

  let a_gradientTransform = user C.string_of_transforms "gradientTransform"

  let a_spreadMethod = user C.string_of_big_variant "spreadMethod"

  let a_fx = user C.string_of_coord "fx"

  let a_fy = user C.string_of_coord "fy"

  let a_offset = user C.string_of_offset "offset"

  let a_patternUnits = user C.string_of_big_variant "patternUnits"

  let a_patternContentUnits =
    user C.string_of_big_variant "patternContentUnits"

  let a_patternTransform = user C.string_of_transforms "patternTransform"

  let a_clipPathUnits = user C.string_of_big_variant "clipPathUnits"

  let a_maskUnits = user C.string_of_big_variant "maskUnits"

  let a_maskContentUnits = user C.string_of_big_variant "maskContentUnits"

  let a_primitiveUnits = user C.string_of_big_variant "primitiveUnits"

  let a_filterRes = user C.string_of_number_optional_number "filterResUnits"

  let a_result = string "result"

  let a_in' = user C.string_of_in_value "in"

  let a_in2 = user C.string_of_in_value "in2"

  let a_azimuth = float "azimuth"

  let a_elevation = float "elevation"

  let a_pointsAtX = float "pointsAtX"

  let a_pointsAtY = float "pointsAtY"

  let a_pointsAtZ = float "pointsAtZ"

  let a_specularExponent = float "specularExponent"

  let a_specularConstant = float "specularConstant"

  let a_limitingConeAngle = float "limitingConeAngle"

  let a_mode = user C.string_of_big_variant "mode"

  let a_feColorMatrix_type = user C.string_of_big_variant "type"

  let a_values = user C.string_of_numbers "values"

  let a_transfer_type = user C.string_of_big_variant "type"

  let a_tableValues = user C.string_of_numbers "tableValues"

  let a_intercept = user C.string_of_number "intercept"

  let a_amplitude = user C.string_of_number "amplitude"

  let a_exponent = user C.string_of_number "exponent"

  let a_transfer_offset = user C.string_of_number "offset"

  let a_feComposite_operator = user C.string_of_big_variant "operator"

  let a_k1 = user C.string_of_number "k1"

  let a_k2 = user C.string_of_number "k2"

  let a_k3 = user C.string_of_number "k3"

  let a_k4 = user C.string_of_number "k4"

  let a_order = user C.string_of_number_optional_number "order"

  let a_kernelMatrix = user C.string_of_numbers "kernelMatrix"

  let a_divisor = user C.string_of_number "divisor"

  let a_bias = user C.string_of_number "bias"

  let a_kernelUnitLength =
    user C.string_of_number_optional_number "kernelUnitLength"

  let a_targetX = user string_of_int "targetX"

  let a_targetY = user string_of_int "targetY"

  let a_edgeMode = user C.string_of_big_variant "targetY"

  let a_preserveAlpha = user Bool.to_string "preserveAlpha"

  let a_surfaceScale = user C.string_of_number "surfaceScale"

  let a_diffuseConstant = user C.string_of_number "diffuseConstant"

  let a_scale = user C.string_of_number "scale"

  let a_xChannelSelector = user C.string_of_big_variant "xChannelSelector"

  let a_yChannelSelector = user C.string_of_big_variant "yChannelSelector"

  let a_stdDeviation = user C.string_of_number_optional_number "stdDeviation"

  let a_feMorphology_operator = user C.string_of_big_variant "operator"

  let a_radius = user C.string_of_number_optional_number "radius"

  let a_baseFrenquency =
    user C.string_of_number_optional_number "baseFrequency"

  let a_numOctaves = user string_of_int "numOctaves"

  let a_seed = user C.string_of_number "seed"

  let a_stitchTiles = user C.string_of_big_variant "stitchTiles"

  let a_feTurbulence_type = user C.string_of_big_variant "type"

  let a_target = string "xlink:target"

  let a_attributeName = string "attributeName"

  let a_attributeType = user C.string_of_big_variant "attributeType"

  let a_begin' = string "begin"

  let a_dur = string "dur"

  let a_min = string "min"

  let a_max = string "max"

  let a_restart = user C.string_of_big_variant "restart"

  let a_repeatCount = string "repeatCount"

  let a_repeatDur = string "repeatDur"

  let a_fill = user C.string_of_paint "fill"

  let a_animation_fill = user C.string_of_big_variant "fill"

  let a_fill_rule = user C.string_of_fill_rule "fill-rule"

  let a_calcMode = user C.string_of_big_variant "calcMode"

  let a_animation_values = comma_sep "values"

  let a_keyTimes = comma_sep "keyTimes"

  let a_keySplines = comma_sep "keySplines"

  let a_from = string "from"

  let a_to' = string "to"

  let a_by = string "by"

  let a_additive = user C.string_of_big_variant "additive"

  let a_accumulate = user C.string_of_big_variant "accumulate"

  let a_keyPoints = user C.string_of_numbers_semicolon "keyPoints"

  let a_path = string "path"

  let a_animateTransform_type = user C.string_of_big_variant "type"

  let a_horiz_origin_x = user C.string_of_number "horiz-origin-x"

  let a_horiz_origin_y = user C.string_of_number "horiz-origin-y"

  let a_horiz_adv_x = user C.string_of_number "horiz-adv-x"

  let a_vert_origin_x = user C.string_of_number "vert-origin-x"

  let a_vert_origin_y = user C.string_of_number "vert-origin-y"

  let a_vert_adv_y = user C.string_of_number "vert-adv-y"

  let a_unicode = string "unicode"

  let a_glyph_name = string "glyphname"

  let a_orientation = user C.string_of_big_variant "orientation"

  let a_arabic_form = user C.string_of_big_variant "arabic-form"

  let a_lang = string "lang"

  let a_u1 = string "u1"

  let a_u2 = string "u2"

  let a_g1 = string "g1"

  let a_g2 = string "g2"

  let a_k = string "k"

  let a_font_family = string "font-family"

  let a_font_style = string "font-style"

  let a_font_variant = string "font-variant"

  let a_font_weight = string "font-weight"

  let a_font_stretch = string "font-stretch"

  let a_font_size = string "font-size"

  let a_unicode_range = string "unicode-range"

  let a_units_per_em = string "units-per-em"

  let a_stemv = user C.string_of_number "stemv"

  let a_stemh = user C.string_of_number "stemh"

  let a_slope = user C.string_of_number "slope"

  let a_cap_height = user C.string_of_number "cap-height"

  let a_x_height = user C.string_of_number "x-height"

  let a_accent_height = user C.string_of_number "accent-height"

  let a_ascent = user C.string_of_number "ascent"

  let a_widths = string "widths"

  let a_bbox = string "bbox"

  let a_ideographic = user C.string_of_number "ideographic"

  let a_alphabetic = user C.string_of_number "alphabetic"

  let a_mathematical = user C.string_of_number "mathematical"

  let a_hanging = user C.string_of_number "hanging"

  let a_videographic = user C.string_of_number "v-ideographic"

  let a_v_alphabetic = user C.string_of_number "v-alphabetic"

  let a_v_mathematical = user C.string_of_number "v-mathematical"

  let a_v_hanging = user C.string_of_number "v-hanging"

  let a_underline_position = user C.string_of_number "underline-position"

  let a_underline_thickness = user C.string_of_number "underline-thickness"

  let a_strikethrough_position =
    user C.string_of_number "strikethrough-position"

  let a_strikethrough_thickness =
    user C.string_of_number "strikethrough-thickness"

  let a_overline_position = user C.string_of_number "overline-position"

  let a_overline_thickness = user C.string_of_number "overline-thickness"

  let a_string = string "string"

  let a_name = X.At.string "name"

  let a_alignment_baseline =
    user C.string_of_alignment_baseline "alignment-baseline"

  let a_dominant_baseline =
    user C.string_of_dominant_baseline "dominant-baseline"

  (** Javascript events *)

  let a_onabort : (Model.Ev.base, _) cons_handler = handler "onabort"

  let a_onactivate : (Model.Ev.base, _) cons_handler = handler "onactivate"

  let a_onbegin : (Model.Ev.base, _) cons_handler = handler "onbegin"

  let a_onend : (Model.Ev.base, _) cons_handler = handler "onend"

  let a_onerror : (Model.Ev.base, _) cons_handler = handler "onerror"

  let a_onfocusin : (Model.Ev.base, _) cons_handler = handler "onfocusin"

  let a_onfocusout : (Model.Ev.base, _) cons_handler = handler "onfocusout"

  let a_onload : (Model.Ev.base, _) cons_handler = handler "onload"

  let a_onrepeat : (Model.Ev.base, _) cons_handler = handler "onrepeat"

  let a_onresize : (Model.Ev.base, _) cons_handler = handler "onresize"

  let a_onscroll : (Model.Ev.base, _) cons_handler = handler "onscroll"

  let a_onunload : (Model.Ev.base, _) cons_handler = handler "onunload"

  let a_onzoom : (Model.Ev.base, _) cons_handler = handler "onzoom"

  (** Javascript mouse events *)

  let a_onclick : (Model.Ev.mouse, _) cons_handler = handler "onclick"

  let a_onmousedown : (Model.Ev.mouse, _) cons_handler =
    handler "onmousedown"

  let a_onmouseup : (Model.Ev.mouse, _) cons_handler = handler "onmouseup"

  let a_onmouseover : (Model.Ev.mouse, _) cons_handler =
    handler "onmouseover"

  let a_onmouseout : (Model.Ev.mouse, _) cons_handler = handler "onmouseout"

  let a_onmousemove : (Model.Ev.mouse, _) cons_handler =
    handler "onmousemove"

  (** Javascript touch events *)
  let a_ontouchstart : (Model.Ev.touch, _) cons_handler =
    handler "ontouchstart"

  let a_ontouchend : (Model.Ev.touch, _) cons_handler = handler "ontouchend"

  let a_ontouchmove : (Model.Ev.touch, _) cons_handler =
    handler "ontouchmove"

  let a_ontouchcancel : (Model.Ev.touch, _) cons_handler =
    handler "ontouchcancel"

  let a_stop_color = X.At.string "stop-color"

  let a_stop_opacity = user C.string_of_number "stop-opacity"

  let a_stroke = user C.string_of_paint "stroke"

  let a_stroke_width = user C.string_of_length "stroke-width"

  let a_stroke_linecap = user C.string_of_big_variant "stroke-linecap"

  let a_stroke_linejoin = user C.string_of_big_variant "stroke-linejoin"

  let a_stroke_miterlimit = user C.string_of_number "stroke-miterlimit"

  let a_stroke_dasharray =
    user C.string_of_strokedasharray "stroke-dasharray"

  let a_stroke_dashoffset = user C.string_of_length "stroke-dashoffset"

  let a_stroke_opacity = user C.string_of_number "stroke-opacity"

  module Co = struct
    module A = X.At
    open At
    include X.Co

    let svg ?(a = []) children =
      let attribs =
        (A.string "xmlns" @:= "http://www.w3.org/2000/svg")
        :: (A.string "xmlns:xlink" @:= "http://www.w3.org/1999/xlink")
        :: a
      in
      star ~a:attribs "svg" children

    (* also generated *)
    let g = star "g"

    let defs = star "defs"

    let desc = unary "desc"

    let title = unary "title"

    let symbol = star "symbol"

    let use = star "use"

    let image = star "image"

    let switch = star "switch"

    let style = unary "style"

    let path = star "path"

    let rect = star "rect"

    let circle = star "circle"

    let ellipse = star "ellipse"

    let line = star "line"

    let polyline = star "polyline"

    let polygon = star "polygon"

    let text = star "text"

    let tspan = star "tspan"

    let tref = star "tref"

    let textPath = star "textPath"

    let marker = star "marker"

    let linearGradient = star "linearGradient"

    let radialGradient = star "radialGradient"

    let stop = star "stop"

    let pattern = star "pattern"

    let clipPath = star "clipPath"

    let filter = star "filter"

    let feDistantLight = star "feDistantLight"

    let fePointLight = star "fePointLight"

    let feSpotLight = star "feSpotLight"

    let feBlend = star "feBlend"

    let feColorMatrix = star "feColorMatrix"

    let feComponentTransfer = star "feComponentTransfer"

    let feFuncA = star "feFuncA"

    let feFuncG = star "feFuncG"

    let feFuncB = star "feFuncB"

    let feFuncR = star "feFuncR"

    let feComposite = star "feComposite"

    let feConvolveMatrix = star "feConvolveMatrix"

    let feDiffuseLighting = star "feDiffuseLighting"

    let feDisplacementMap = star "feDisplacementMap"

    let feFlood = star "feFlood"

    let feGaussianBlur = star "feGaussianBlur"

    let feImage = star "feImage"

    let feMerge = star "feMerge"

    let feMorphology = star "feMorphology"

    let feOffset = star "feOffset"

    let feSpecularLighting = star "feSpecularLighting"

    let feTile = star "feTile"

    let feTurbulence = star "feTurbulence"

    let cursor = star "cursor"

    let a = star "a"

    let view = star "view"

    let script = unary "script"

    let animation = star "animate"

    let set = star "set"

    let animateMotion = star "animateMotion"

    let mpath = star "mpath"

    let animateColor = star "animateColor"

    let animateTransform = star "animateTransform"

    let metadata = star "metadata"

    let foreignObject = star "foreignObject"
  end
end
