module Make (M : Model.Sig) = struct
  module At = struct
    type 'a t = M.At.t

    type ('a, 'b) cons_handler = 'a M.Ev.handler -> 'b t

    type ('a, 'b) cons = 'a option M.WrapValue.t -> 'b t

    let ( @:?!= ) f x = f x

    let ( @:= ) c x = c @:?!= M.WrapValue.pure (Some x)

    let ( @:?= ) c x = c @:?!= M.WrapValue.pure x

    let ( @:!= ) c x = c @:?!= M.WrapValue.map (fun x -> Some x) x

    let handler s handler = M.At.handler s handler

    let user f name v = M.At.string name (M.WrapValue.map (Option.map f) v)

    let int x = user string_of_int x

    let bool x = user Bool.to_string x

    let uri x = user M.Uri.to_string x

    let float x = user string_of_float x

    let space_sep x = user (String.concat " ") x

    let comma_sep x = user (String.concat " ") x

    let string x = user (fun s -> s) x
  end

  module Co = struct
    type elem = M.Co.elem

    type t = M.Co.t

    type 'a nullary = ?a:'a At.t list -> unit -> t

    type 'a unary = ?a:'a At.t list -> t -> t

    type 'a star = ?a:'a At.t list -> t list -> t

    let txt s = M.Co.txt (M.WrapValue.pure s)

    let txtw = M.Co.txt

    let fmt s = Printf.kprintf txt s

    let list = M.Co.list

    let seq = M.Co.seq

    let node tag a elt = M.Co.elem (M.Co.node tag a elt)

    let raw_node tag a elt = M.Co.node tag a elt

    let empty = M.Co.empty

    let elem = M.Co.elem

    let unary tag ?(a = []) elt = node tag a elt

    let star tag ?a elts = unary tag ?a (list elts)

    let nullary tag ?a () = unary tag ?a empty

    let option = function None -> empty | Some x -> x

    let reactive = M.Co.reactive

    let reactive_list = M.Co.reactive_list

    let draw ~f x = reactive (M.WrapValue.map (fun value -> f value) x)

    let concat ~sep f l =
      let rec aux = function
        | [] -> empty
        | [x] -> x
        | t :: q -> list [t; sep (); aux q]
      in
      aux (List.map f l)
  end
end
