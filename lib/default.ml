module IdWrapValue = struct
  type 'a t = 'a

  let pure x = x

  let map f x = f x
end

module WrapList = struct
  type 'a t = 'a list

  let empty = []

  let singleton x = [x]

  let cons x l = x :: l

  let append x y = x @ y
end

module UriString = struct
  type t = string

  let of_string s = s

  let to_string s = s
end
