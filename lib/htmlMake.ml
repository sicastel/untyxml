module C = struct
  let string_of_sandbox_token = function
    | `Allow_forms -> "allow-forms"
    | `Allow_pointer_lock -> "allow-pointer-lock"
    | `Allow_popups -> "allow-popups"
    | `Allow_top_navigation -> "allow-top-navigation"
    | `Allow_same_origin -> "allow-same-origin"
    | `Allow_script -> "allow-script"

  let string_of_linktype = function
    | `Alternate -> "alternate"
    | `Archives -> "archives"
    | `Author -> "author"
    | `Bookmark -> "bookmark"
    | `Canonical -> "canonical"
    | `External -> "external"
    | `First -> "first"
    | `Help -> "help"
    | `Icon -> "icon"
    | `Index -> "index"
    | `Last -> "last"
    | `License -> "license"
    | `Next -> "next"
    | `Nofollow -> "nofollow"
    | `Noreferrer -> "noreferrer"
    | `Noopener -> "noopener"
    | `Pingback -> "pingback"
    | `Prefetch -> "prefetch"
    | `Prev -> "prev"
    | `Search -> "search"
    | `Stylesheet -> "stylesheet"
    | `Sidebar -> "sidebar"
    | `Tag -> "tag"
    | `Up -> "up"
    | `Other s -> s

  let string_of_mediadesc_token = function
    | `All -> "all"
    | `Aural -> "aural"
    | `Braille -> "braille"
    | `Embossed -> "embossed"
    | `Handheld -> "handheld"
    | `Print -> "print"
    | `Projection -> "projection"
    | `Screen -> "screen"
    | `Speech -> "speech"
    | `Tty -> "tty"
    | `Tv -> "tv"
    | `Raw_mediadesc s -> s

  let string_of_referrerpolicy = function
    | `Empty -> ""
    | `No_referrer -> "no-referrer"
    | `No_referrer_when_downgrade -> "no-referrer-when-downgrade"
    | `Origin -> "origin"
    | `Origin_when_cross_origin -> "origin-when-cross-origin"
    | `Same_origin -> "same-origin"
    | `Strict_origin -> "strict-origin"
    | `Strict_origin_when_cross_origin -> "strict-origin-when-cross-origin"
    | `Unsafe_url -> "unsafe-url"

  let string_of_big_variant = function
    | `Anonymous -> "anonymous"
    | `Async -> "async"
    | `Autofocus -> "autofocus"
    | `Autoplay -> "autoplay"
    | `Checked -> "checked"
    | `Defer -> "defer"
    | `Disabled -> "disabled"
    | `Muted -> "muted"
    | `ReadOnly -> "readonly"
    | `Rect -> "rect"
    | `Selected -> "selected"
    | `Use_credentials -> "use-credentials"
    | `W3_org_1999_xhtml -> "http://www.w3.org/1999/xhtml"
    | `All -> "all"
    | `Preserve -> "preserve"
    | `Default -> "default"
    | `Controls -> "controls"
    | `Ltr -> "ltr"
    | `Rtl -> "rtl"
    | `Get -> "GET"
    | `Post -> "POST"
    | `Formnovalidate -> "formnovalidate"
    | `Hidden -> "hidden"
    | `Ismap -> "ismap"
    | `Loop -> "loop"
    | `Novalidate -> "novalidate"
    | `Open -> "open"
    | `None -> "none"
    | `Metadata -> "metadata"
    | `Audio -> "audio"
    | `Pubdate -> "pubdate"
    | `Required -> "required"
    | `Reversed -> "reserved"
    | `Scoped -> "scoped"
    | `Seamless -> "seamless"
    | `Any -> "any"
    | `Soft -> "soft"
    | `Hard -> "hard"
    | `Context -> "context"
    | `Toolbar -> "toolbar"
    | `Command -> "command"
    | `Checkbox -> "checkbox"
    | `Radio -> "radio"
    | `Multiple -> "multiple"
    | `Left -> "left"
    | `Right -> "right"
    | `Justify -> "justify"
    | `Char -> "char"
    | `Row -> "row"
    | `Col -> "col"
    | `Rowgroup -> "rowgroup"
    | `Colgroup -> "colgroup"
    | `Groups -> "groups"
    | `Rows -> "rows"
    | `Cols -> "cols"
    | `Zero -> "0"
    | `One -> "1"
    | `Yes -> "yes"
    | `No -> "no"
    | `Auto -> "auto"
    | `Circle -> "circle"
    | `Poly -> "poly"
    | `Alternate -> "alternate"
    | `Archives -> "archives"
    | `Author -> "author"
    | `Bookmark -> "bookmark"
    | `External -> "external"
    | `First -> "first"
    | `Help -> "help"
    | `Icon -> "icon"
    | `Index -> "index"
    | `Last -> "last"
    | `License -> "license"
    | `Next -> "next"
    | `Nofollow -> "nofollow"
    | `Noreferrer -> "noreferrer"
    | `Pingback -> "pingback"
    | `Prefetch -> "prefetch"
    | `Prev -> "prev"
    | `Search -> "search"
    | `Stylesheet -> "stylesheet"
    | `Sidebar -> "sidebar"
    | `Tag -> "tag"
    | `Up -> "up"
    | `Verbatim -> "verbatim"
    | `Latin -> "latin"
    | `Latin_name -> "latin-name"
    | `Latin_prose -> "latin-prose"
    | `Full_width_latin -> "full-width-latin"
    | `Kana -> "kana"
    | `Katakana -> "katakana"
    | `Numeric -> "numeric"
    | `Tel -> "tel"
    | `Email -> "email"
    | `Url -> "url"
    | `Text -> "text"
    | `Decimal -> "decimal"
    | `Other s -> s

  let string_of_input_type = function
    | `Button -> "button"
    | `Checkbox -> "checkbox"
    | `Color -> "color"
    | `Date -> "date"
    | `Datetime -> "datetime"
    | `Datetime_local -> "datetime-local"
    | `Email -> "email"
    | `File -> "file"
    | `Hidden -> "hidden"
    | `Image -> "image"
    | `Month -> "month"
    | `Number -> "number"
    | `Password -> "password"
    | `Radio -> "radio"
    | `Range -> "range"
    | `Readonly -> "readonly"
    | `Reset -> "reset"
    | `Search -> "search"
    | `Submit -> "submit"
    | `Tel -> "tel"
    | `Text -> "text"
    | `Time -> "time"
    | `Url -> "url"
    | `Week -> "week"

  let string_of_script_type = function
    | `Javascript -> "application/javascript"
    | `Module -> "module"
    | `Mime s -> s

  let string_of_number_or_datetime = function
    | `Number n -> string_of_int n
    | `Datetime t -> t

  let string_of_character = String.make 1

  let string_of_number = string_of_int

  let unoption_string = function Some x -> x | None -> ""

  let string_of_step = function Some x -> string_of_float x | None -> "any"

  let string_of_sizes = function
    | Some l ->
        String.concat " "
          (List.map (fun (x, y) -> Printf.sprintf "%dx%d" x y) l)
    | None -> "any"

  let string_of_sandbox l =
    String.concat " " (List.map string_of_sandbox_token l)

  let string_of_numbers l = String.concat "," (List.map string_of_number l)

  let string_of_mediadesc l =
    String.concat ", " (List.map string_of_mediadesc_token l)

  let string_of_linktypes l =
    String.concat " " (List.map string_of_linktype l)

  let string_of_srcset (type a) (f : a -> string)
      (l : [< a Html.Terminology.image_candidate] list) =
    let f = function
      | `Url url -> f url
      | `Url_width (url, v) ->
          Printf.sprintf "%s %sw" (f url) (string_of_number v)
      | `Url_pixel (url, v) ->
          Printf.sprintf "%s %sx" (f url) (string_of_float v)
    in
    String.concat ", " (List.map f l)

  let string_of_autocomplete (l : Html.Terminology.autocomplete_option) =
    match l with
    | `On | `Tokens [] -> "on"
    | `Off -> "off"
    | `Tokens strs -> String.concat " " strs
end

module Make (Model : Model.Sig) = struct
  module Model = Model
  module Svg = SvgMake.Make (Model)
  module X = ModelType.Make (Model)
  module At = X.At
  open At

  let a_linktypes name = user C.string_of_linktypes name

  let a_mediadesc name = user C.string_of_mediadesc name

  let a_srcset name = user (C.string_of_srcset Model.Uri.to_string) name

  let a_constant a () = At.string a @:= a

  let a_class = At.space_sep "class"

  let a_id = At.string "id"

  let a_user_data name = At.string ("data-" ^ name)

  let a_title = At.string "title"

  (* I18N: *)
  let a_xml_lang = At.string "xml:lang"

  let a_lang = At.string "lang"

  (* Style: *)
  let a_style = At.string "style"

  let a_property = At.string "property"

  (* Events: *)
  let a_onabort : (Model.Ev.base, [> `OnAbort]) cons_handler =
    At.handler "onabort"

  let a_onafterprint : (Model.Ev.base, _) cons_handler =
    At.handler "onafterprint"

  let a_onbeforeprint : (Model.Ev.base, _) cons_handler =
    At.handler "onbeforeprint"

  let a_onbeforeunload : (Model.Ev.base, _) cons_handler =
    At.handler "onbeforeunload"

  let a_onblur : (Model.Ev.base, _) cons_handler = At.handler "onblur"

  let a_oncanplay : (Model.Ev.base, _) cons_handler = At.handler "oncanplay"

  let a_oncanplaythrough : (Model.Ev.base, _) cons_handler =
    At.handler "oncanplaythrough"

  let a_onchange : (Model.Ev.base, _) cons_handler = At.handler "onchange"

  let a_onclose : (Model.Ev.base, _) cons_handler = At.handler "onclose"

  let a_ondurationchange : (Model.Ev.base, _) cons_handler =
    At.handler "ondurationchange"

  let a_onemptied : (Model.Ev.base, _) cons_handler = At.handler "onemptied"

  let a_onended : (Model.Ev.base, _) cons_handler = At.handler "onended"

  let a_onerror : (Model.Ev.base, _) cons_handler = At.handler "onerror"

  let a_onfocus : (Model.Ev.base, _) cons_handler = At.handler "onfocus"

  let a_onformchange : (Model.Ev.base, _) cons_handler =
    At.handler "onformchange"

  let a_onforminput : (Model.Ev.base, _) cons_handler =
    At.handler "onforminput"

  let a_onhashchange : (Model.Ev.base, _) cons_handler =
    At.handler "onhashchange"

  let a_oninput : (Model.Ev.base, _) cons_handler = At.handler "oninput"

  let a_oninvalid : (Model.Ev.base, _) cons_handler = At.handler "oninvalid"

  let a_onoffline : (Model.Ev.base, _) cons_handler = At.handler "onoffline"

  let a_ononline : (Model.Ev.base, _) cons_handler = At.handler "ononline"

  let a_onpause : (Model.Ev.base, _) cons_handler = At.handler "onpause"

  let a_onplay : (Model.Ev.base, _) cons_handler = At.handler "onplay"

  let a_onplaying : (Model.Ev.base, _) cons_handler = At.handler "onplaying"

  let a_onpagehide : (Model.Ev.base, _) cons_handler =
    At.handler "onpagehide"

  let a_onpageshow : (Model.Ev.base, _) cons_handler =
    At.handler "onpageshow"

  let a_onpopstate : (Model.Ev.base, _) cons_handler =
    At.handler "onpopstate"

  let a_onprogress : (Model.Ev.base, _) cons_handler =
    At.handler "onprogress"

  let a_onratechange : (Model.Ev.base, _) cons_handler =
    At.handler "onratechange"

  let a_onreadystatechange : (Model.Ev.base, _) cons_handler =
    At.handler "onreadystatechange"

  let a_onredo : (Model.Ev.base, _) cons_handler = At.handler "onredo"

  let a_onresize : (Model.Ev.base, _) cons_handler = At.handler "onresize"

  let a_onscroll : (Model.Ev.base, _) cons_handler = At.handler "onscroll"

  let a_onseeked : (Model.Ev.base, _) cons_handler = At.handler "onseeked"

  let a_onseeking : (Model.Ev.base, _) cons_handler = At.handler "onseeking"

  let a_onselect : (Model.Ev.base, _) cons_handler = At.handler "onselect"

  let a_onshow : (Model.Ev.base, _) cons_handler = At.handler "onshow"

  let a_onstalled : (Model.Ev.base, _) cons_handler = At.handler "onstalled"

  let a_onstorage : (Model.Ev.base, _) cons_handler = At.handler "onstorage"

  let a_onsubmit : (Model.Ev.base, _) cons_handler = At.handler "onsubmit"

  let a_onsuspend : (Model.Ev.base, _) cons_handler = At.handler "onsuspend"

  let a_ontimeupdate : (Model.Ev.base, _) cons_handler =
    At.handler "ontimeupdate"

  let a_onundo : (Model.Ev.base, _) cons_handler = At.handler "onundo"

  let a_onunload : (Model.Ev.base, _) cons_handler = At.handler "onunload"

  let a_onvolumechange : (Model.Ev.base, _) cons_handler =
    At.handler "onvolumechange"

  let a_onwaiting : (Model.Ev.base, _) cons_handler = At.handler "onwaiting"

  let a_onload : (Model.Ev.base, _) cons_handler = At.handler "onload"

  let a_onloadeddata : (Model.Ev.base, _) cons_handler =
    At.handler "onloadeddata"

  let a_onloadedmetadata : (Model.Ev.base, _) cons_handler =
    At.handler "onloadedmetadata"

  let a_onloadstart : (Model.Ev.base, _) cons_handler =
    At.handler "onloadstart"

  let a_onmessage : (Model.Ev.base, _) cons_handler = At.handler "onmessage"

  let a_onmousewheel : (Model.Ev.base, _) cons_handler =
    At.handler "onmousewheel"

  (** Javascript mouse events *)
  let a_onclick : (Model.Ev.mouse, _) cons_handler = At.handler "onclick"

  let a_oncontextmenu : (Model.Ev.mouse, _) cons_handler =
    At.handler "oncontextmenu"

  let a_ondblclick : (Model.Ev.mouse, _) cons_handler =
    At.handler "ondblclick"

  let a_ondrag : (Model.Ev.mouse, _) cons_handler = At.handler "ondrag"

  let a_ondragend : (Model.Ev.mouse, _) cons_handler = At.handler "ondragend"

  let a_ondragenter : (Model.Ev.mouse, _) cons_handler =
    At.handler "ondragenter"

  let a_ondragleave : (Model.Ev.mouse, _) cons_handler =
    At.handler "ondragleave"

  let a_ondragover : (Model.Ev.mouse, _) cons_handler =
    At.handler "ondragover"

  let a_ondragstart : (Model.Ev.mouse, _) cons_handler =
    At.handler "ondragstart"

  let a_ondrop : (Model.Ev.mouse, _) cons_handler = At.handler "ondrop"

  let a_onmousedown : (Model.Ev.mouse, _) cons_handler =
    At.handler "onmousedown"

  let a_onmouseup : (Model.Ev.mouse, _) cons_handler = At.handler "onmouseup"

  let a_onmouseover : (Model.Ev.mouse, _) cons_handler =
    At.handler "onmouseover"

  let a_onmousemove : (Model.Ev.mouse, _) cons_handler =
    At.handler "onmousemove"

  let a_onmouseout : (Model.Ev.mouse, _) cons_handler =
    At.handler "onmouseout"

  (** Javascript touch events *)
  let a_ontouchstart : (Model.Ev.touch, _) cons_handler =
    At.handler "ontouchstart"

  let a_ontouchend : (Model.Ev.touch, _) cons_handler =
    At.handler "ontouchend"

  let a_ontouchmove : (Model.Ev.touch, _) cons_handler =
    At.handler "ontouchmove"

  let a_ontouchcancel : (Model.Ev.touch, _) cons_handler =
    At.handler "ontouchcancel"

  (** Javascript keyboard events *)
  let a_onkeypress : (Model.Ev.keyboard, _) cons_handler =
    At.handler "onkeypress"

  let a_onkeydown : (Model.Ev.keyboard, _) cons_handler =
    At.handler "onkeydown"

  let a_onkeyup : (Model.Ev.keyboard, _) cons_handler = At.handler "onkeyup"

  (* Other Attributes *)
  let a_version = At.string "version"

  let a_xmlns = user C.string_of_big_variant "xmlns"

  let a_manifest = At.uri "manifest"

  let a_cite = At.uri "cite"

  let a_xml_space = user C.string_of_big_variant "xml:space"

  let a_accesskey = user C.string_of_character "accesskey"

  let a_charset = At.string "charset"

  let a_accept_charset = At.space_sep "accept-charset"

  let a_accept = At.comma_sep "accept"

  let a_href = At.uri "href"

  let a_hreflang = At.string "hreflang"

  let a_download = user C.unoption_string "download"

  let a_rel = a_linktypes "rel"

  let a_tabindex = At.int "tabindex"

  let a_mime_type = At.string "type"

  let a_alt = At.string "alt"

  let a_height = At.int "height"

  let a_src = At.uri "src"

  let a_width = At.int "width"

  let a_label_for = At.string "for"

  let a_output_for = At.space_sep "for"

  let a_selected () = a_constant "selected" ()

  let a_text_value = At.string "value"

  let a_int_value = At.int "value"

  let a_value = At.string "value"

  let a_float_value = At.float "value"

  let a_action = At.uri "action"

  let a_method' = user C.string_of_big_variant "method"

  let a_enctype = At.string "enctype"

  let a_checked () = a_constant "checked" ()

  let a_disabled () = a_constant "disabled" ()

  let a_readonly () = a_constant "readonly" ()

  let a_maxlength = At.int "maxlength"

  let a_minlength = At.int "minlength"

  let a_name = At.string "name"

  let a_allowfullscreen () = a_constant "allowfullscreen" ()

  let a_allowpaymentrequest () = a_constant "allowpaymentrequest" ()

  let a_referrerpolicy = user C.string_of_referrerpolicy "referrerpolicy"

  let a_autocomplete = user C.string_of_autocomplete "autocomplete"

  let a_async () = a_constant "async" ()

  let a_autofocus () = a_constant "autofocus" ()

  let a_autoplay () = a_constant "autoplay" ()

  let a_muted () = a_constant "muted" ()

  let a_crossorigin = user C.string_of_big_variant "crossorigin"

  let a_integrity = At.string "integrity"

  let a_mediagroup = At.string "mediagroup"

  let a_challenge = At.string "challenge"

  let a_contenteditable = bool "contenteditable"

  let a_contextmenu = At.string "contextmenu"

  let a_controls () = a_constant "controls" ()

  let a_dir = user C.string_of_big_variant "dir"

  let a_draggable = bool "draggable"

  let a_form = At.string "form"

  let a_formaction = At.uri "formaction"

  let a_formenctype = At.string "formenctype"

  let a_formnovalidate () = a_constant "formnovalidate" ()

  let a_formtarget = At.string "formtarget"

  let a_hidden () = a_constant "hidden" ()

  let a_high = At.float "high"

  let a_icon = At.uri "icon"

  let a_ismap () = a_constant "ismap" ()

  let a_keytype = At.string "keytype"

  let a_list = At.string "list"

  let a_loop () = a_constant "loop" ()

  let a_low = At.float "low"

  let a_max = At.float "max"

  let a_input_max = user C.string_of_number_or_datetime "max"

  let a_min = At.float "min"

  let a_input_min = user C.string_of_number_or_datetime "min"

  let a_inputmode = user C.string_of_big_variant "inputmode"

  let a_novalidate () = a_constant "novalidate" ()

  let a_open' () = a_constant "open" ()

  let a_optimum = At.float "optimum"

  let a_pattern = At.string "pattern"

  let a_placeholder = At.string "placeholder"

  let a_poster = At.uri "poster"

  let a_preload = user C.string_of_big_variant "preload"

  let a_pubdate () = a_constant "pubdate" ()

  let a_radiogroup = At.string "radiogroup"

  let a_required () = a_constant "required" ()

  let a_reversed () = a_constant "reserved" ()

  let a_sandbox = user C.string_of_sandbox "sandbox"

  let a_spellcheck = bool "spellcheck"

  let a_scoped () = a_constant "scoped" ()

  let a_seamless () = a_constant "seamless" ()

  let a_sizes = user C.string_of_sizes "sizes"

  let a_span = At.int "span"

  (*let a_srcdoc*)
  let a_srcset = a_srcset "srcset"

  let a_img_sizes = At.comma_sep "sizes"

  let a_start = At.int "start"

  let a_step = user C.string_of_step "step"

  let a_translate = user C.string_of_big_variant "translate"

  let a_wrap = user C.string_of_big_variant "wrap"

  let a_size = At.int "size"

  let a_input_type = user C.string_of_input_type "type"

  let a_menu_type = user C.string_of_big_variant "type"

  let a_command_type = user C.string_of_big_variant "type"

  let a_button_type = user C.string_of_input_type "type"

  let a_script_type = user C.string_of_script_type "type"

  let a_multiple () = a_constant "multiple" ()

  let a_cols = At.int "cols"

  let a_rows = At.int "rows"

  let a_colspan = At.int "colspan"

  let a_headers = At.space_sep "headers"

  let a_rowspan = At.int "rowspan"

  let a_data = At.uri "data"

  let a_scrolling = user C.string_of_big_variant "scrolling"

  let a_target = At.string "target"

  let a_content = At.string "content"

  let a_http_equiv = At.string "http-equiv"

  let a_media = a_mediadesc "media"

  let a_datetime = At.string "datetime"

  let a_shape = user C.string_of_big_variant "shape"

  let a_coords = user C.string_of_numbers "coords"

  let a_usemap = At.string "usemap"

  let a_defer () = a_constant "defer" ()

  let a_label = At.string "label"

  let a_role = At.space_sep "role"

  module Co = struct
    module A = X.At
    open At
    include X.Co

    let star_cl s ?cl ?id ?(a = []) body =
      let a = (a_id @:?= id) :: (a_class @:?= cl) :: a in
      star s ~a body

    let option_cons opt elts =
      match opt with None -> elts | Some x -> x :: elts

    let draw ~f x = reactive @@ Model.WrapValue.map (fun x -> f x) x

    let body = star "body"

    let head = star "head"

    let title = unary "title"

    let footer = star_cl "footer"

    let html ?(a = []) head body =
      let content = [head; body] in
      node "html" a (list content)

    let header = star_cl "header"

    let section = star_cl "section"

    let nav = star_cl "nav"

    let h1 = star_cl "h1"

    let h2 = star_cl "h2"

    let h3 = star_cl "h3"

    let h4 = star_cl "h4"

    let h5 = star_cl "h5"

    let h6 = star_cl "h6"

    let hgroup = star_cl "hgroup"

    let address = star_cl "address"

    let blockquote = star_cl "blockquote"

    let dialog = star_cl "dialog"

    let div = star_cl "div"

    let p = star_cl "p"

    let pre = star_cl "pre"

    let abbr = star_cl "abbr"

    let br = nullary "br"

    let cite = star_cl "cite"

    let code = star_cl "code"

    let dfn = star_cl "dfn"

    let em = star_cl "em"

    let kbd = star_cl "kbd"

    let q = star_cl "q"

    let samp = star_cl "samp"

    let span = star_cl "span"

    let strong = star_cl "strong"

    let time = star_cl "time"

    let var = star_cl "var"

    let a = star_cl "a"

    let dl = star_cl "dl"

    let ol = star_cl "ol"

    let ul = star_cl "ul"

    let dd = star_cl "dd"

    let dt = star_cl "dt"

    let li = star_cl "li"

    let hr = nullary "hr"

    let b = star_cl "b"

    let i = star_cl "i"

    let u = star_cl "u"

    let small = star_cl "small"

    let sub = star_cl "sub"

    let sup = star_cl "sup"

    let mark = star_cl "mark"

    let rp = star_cl "rp"

    let rt = star_cl "rt"

    let ruby = star_cl "ruby"

    let wbr = nullary "wbr"

    let bdo ~dir ?(a = []) elts =
      node "bdo" ((a_dir @:!= dir) :: a) (list elts)

    let area ~alt ?(a = []) () =
      node "area" ((a_alt @:!= alt) :: a) Model.Co.empty

    let map = star_cl "map"

    let del = star_cl "del"

    let ins = star_cl "ins"

    let script = unary "script"

    let noscript = star_cl "noscript"

    let template = star_cl "template"

    let article = star_cl "article"

    let aside = star_cl "aside"

    let main = star_cl "main"

    let video_audio name ?src ?srcs ?(a = []) elts =
      let a =
        match src with None -> a | Some uri -> (a_src @:!= uri) :: a
      in
      match srcs with
      | None -> node name a (list elts)
      | Some srcs -> node name a (list (srcs @ elts))

    let audio = video_audio "audio"

    let video = video_audio "video"

    let canvas = star_cl "canvas"

    let command ~label ?(a = []) () =
      node "command" ((a_label @:!= label) :: a) Model.Co.empty

    let menu ?children ?(a = []) () =
      let children =
        match children with
        | None -> Model.Co.empty
        | Some (`Lis l) | Some (`Flows l) -> l
      in
      node "menu" a children

    let embed = nullary "embed"

    let source = nullary "source"

    let meter = star_cl "meter"

    let output_elt = star_cl "output"

    let form = star_cl "form"

    let svg ?(a = []) children = Svg.Co.svg ~a children

    let input = nullary "input"

    let keygen = nullary "keygen"

    let label = star_cl "label"

    let option = unary "option"

    let select = star_cl "select"

    let textarea = unary "textarea"

    let button = star_cl "button"

    let datalist ?children ?(a = []) () =
      let children =
        match children with None -> [] | Some (`Options x | `Phras x) -> x
      in
      node "datalist" a (list children)

    let progress = star_cl "progress"

    let legend = star_cl "legend"

    let details = star_cl "details"

    let summary = star_cl "summary"

    let fieldset ?legend ?(a = []) elts =
      node "fieldset" a (list (option_cons legend elts))

    let optgroup ~label ?(a = []) elts =
      node "optgroup" ((a_label @:!= label) :: a) (list elts)

    let figcaption = star_cl "figcaption"

    let figure ?figcaption ?(a = []) elts =
      let content =
        match figcaption with
        | None -> elts
        | Some (`Top c) -> c :: elts
        | Some (`Bottom c) -> elts @ [c]
      in
      node "figure" a (list content)

    let caption = star_cl "caption"

    let tablex ?caption ?columns ?thead ?(tfoot = []) ?(a = []) elts =
      let content = option_cons thead (tfoot @ elts) in
      let content = option_cons columns content in
      let content = option_cons caption content in
      node "table" a (list content)

    let table = tablex

    let td = star_cl "td"

    let th = star_cl "th"

    let tr = star_cl "tr"

    let colgroup = star_cl "colgroup"

    let col = nullary "col"

    let thead = star_cl "thead"

    let tbody = star_cl "tbody"

    let tfoot = star_cl "tfoot"

    let iframe = star_cl "iframe"

    let object_ ?params ?(a = []) elts =
      let elts = match params with None -> elts | Some e -> e @ elts in
      node "object" a (list elts)

    let param = nullary "param"

    let img ~src ~alt ?(a = []) () =
      let a = (a_src @:!= src) :: (a_alt @:!= alt) :: a in
      node "img" a Model.Co.empty

    let picture ~img ?(a = []) elts =
      let tail_node = [img] in
      let content = elts @ tail_node in
      node "picture" a (list content)

    let meta = nullary "meta"

    let style ?(a = []) elts = node "style" a (list elts)

    let link ~rel ~href ?(a = []) () =
      node "link" ((a_rel @:= rel) :: (a_href @:!= href) :: a) empty

    let base = nullary "base"
  end
end
