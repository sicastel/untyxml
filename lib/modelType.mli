module Make (M : Model.Sig) : sig
  module At : sig
    type +'a t

    type (-'a, +'b) cons

    type ('a, +'b) cons_handler = 'a M.Ev.handler -> 'b t

    val ( @:= ) : ('a, 'b) cons -> 'a -> 'b t

    val ( @:?= ) : ('a, 'b) cons -> 'a option -> 'b t

    val ( @:!= ) : ('a, 'b) cons -> 'a M.WrapValue.t -> 'b t

    val ( @:?!= ) : ('a, 'b) cons -> 'a option M.WrapValue.t -> 'b t

    val handler : string -> ('a, 'b) cons_handler

    val user : ('a -> string) -> string -> ('a, 'b) cons

    val bool : string -> (bool, 'a) cons

    val uri : string -> (M.Uri.t, 'a) cons

    val string : string -> (string, 'a) cons

    val int : string -> (int, 'a) cons

    val float : string -> (float, 'a) cons

    val space_sep : string -> (string list, 'a) cons

    val comma_sep : string -> (string list, 'a) cons
  end

  module Co : sig
    type elem = M.Co.elem

    type t = M.Co.t

    type 'a nullary = ?a:'a At.t list -> unit -> t

    type 'a unary = ?a:'a At.t list -> t -> t

    type 'a star = ?a:'a At.t list -> t list -> t

    val nullary : string -> 'a nullary

    val star : string -> 'a star

    val unary : string -> 'a unary

    val txt : string -> t

    val fmt : ('a, unit, string, t) format4 -> 'a

    val txtw : string M.WrapValue.t -> t

    val list : t list -> t

    val seq : t M.WrapSeq.t -> t

    val reactive : t M.WrapValue.t -> t

    val draw : f:('a -> t) -> 'a M.WrapValue.t -> t

    val reactive_list : t list M.WrapValue.t -> t

    val node : string -> 'a At.t list -> t -> t

    val raw_node : string -> 'a At.t list -> t -> elem

    val elem : elem -> t

    val empty : t

    val option : t option -> t

    val concat : sep:(unit -> t) -> ('a -> t) -> 'a list -> t
  end
end
